﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
namespace ProyectoOscarMarco
{
    public class Codes : TabPage
    {
        private RichTextBox richTextBox1 = null;
        private RichTextBox richTextBox2 = null;
        private RichTextBox richTextBox3 = null;
        private Button cerrar = null;
        private Label output = null;
        private String path = null;
        private Form1 Form1 = null;
        public Codes(String Nombre, String Codigo,String p,Form1 form)
        {
            richTextBox1 = new RichTextBox();
            richTextBox1.Location = new System.Drawing.Point(3, 3);
            richTextBox1.Size = new System.Drawing.Size(40, 653);
            richTextBox1.ReadOnly = true;
            richTextBox2 = new RichTextBox();
            richTextBox2.Location = new System.Drawing.Point(49, 3);
            richTextBox2.Size = new System.Drawing.Size(930, 653);
            richTextBox2.Text = Codigo;
            richTextBox2.TextChanged += new EventHandler(cambio_Texto);
            richTextBox1.MouseDown += new MouseEventHandler(mouse_Down);
            richTextBox2.FontChanged += new EventHandler(font_Change);
            richTextBox2.VScroll += new EventHandler(VScroll);
            richTextBox2.SelectionChanged += new EventHandler(SelectionChanged);
            richTextBox1.ScrollBars = RichTextBoxScrollBars.None;
            richTextBox3 = new RichTextBox();
            richTextBox3.Location = new System.Drawing.Point(6, 691);
            richTextBox3.Size = new System.Drawing.Size(973, 96);
            richTextBox3.ReadOnly = true;
            cerrar = new Button();
            cerrar.Location = new System.Drawing.Point(985, 6);
            cerrar.Size = new System.Drawing.Size(130, 23);
            cerrar.Text = "Cerrar";
            cerrar.Click += new EventHandler(cerrar_Evento);
            output = new Label();
            output.Location = new System.Drawing.Point(6, 665);
            output.Size = new System.Drawing.Size(97, 23);
            output.Text = "Output:";
            this.SuspendLayout();
            this.Controls.Add(output);
            this.Controls.Add(richTextBox1);
            this.Controls.Add(richTextBox2);
            this.Controls.Add(richTextBox3);
            this.Controls.Add(cerrar);
            this.Name = Nombre;
            this.Location = new System.Drawing.Point(4, 22);
            this.Size = new System.Drawing.Size(985, 793);
            this.Text = Nombre;
            this.UseVisualStyleBackColor = true;
            richTextBox2.Select();
            AddLineNumbers();
            path = p;
            Form1 = form;
            
        }

        bool verificarCerrados(String path, List<String> lista)
        {
            foreach (var elemento in lista)
            {
                if (elemento == path)
                {
                    return false;
                }
            }

            return true;
        }

        void cerrar_Evento(object o, EventArgs e)
        {
            if (path != "" && verificarCerrados(path,Form1.Cerrados1))
            {
                var agrega = Form1.Cerrados1;
                agrega.Add(path);
                Form1.Cerrados1 = agrega;
                ItemMenu nuevo = new ItemMenu(path,Form1);
                var items = Form1.DropDownItem;
                items.Add(nuevo);
            }
            this.Dispose();
        }

        void cambio_Texto(object o, EventArgs e)
        {
            if (richTextBox1.Text == "")
            {
                AddLineNumbers();
            }
        }

        void mouse_Down(object o, EventArgs e)
        {
            richTextBox2.Select();
            richTextBox1.DeselectAll();
        }

        void font_Change(object o, EventArgs e)
        {
            richTextBox1.Font = richTextBox2.Font;
            richTextBox2.Select();
            AddLineNumbers();
        }

        void VScroll(object o, EventArgs e)
        {
            richTextBox1.Text = "";
            AddLineNumbers();
            richTextBox1.Invalidate();
        }

        void SelectionChanged(object o, EventArgs e)
        {
            Point pt = richTextBox2.GetPositionFromCharIndex(richTextBox2.SelectionStart);
            if (pt.X == 1)
            {
                AddLineNumbers();
            }
        }

        public int getWidth()
        {
            int w = 25;
            // get total lines of richTextBox1
            int line = richTextBox1.Lines.Length;

            if (line <= 99)
            {
                w = 20 + (int)richTextBox1.Font.Size;
            }
            else if (line <= 999)
            {
                w = 30 + (int)richTextBox1.Font.Size;
            }
            else
            {
                w = 50 + (int)richTextBox1.Font.Size;
            }

            return w;
        }
        public void AddLineNumbers()
        {
            // create & set Point pt to (0,0)
            Point pt = new Point(0, 0);
            // get First Index & First Line from richTextBox1
            int First_Index = richTextBox2.GetCharIndexFromPosition(pt);
            int First_Line = richTextBox2.GetLineFromCharIndex(First_Index);
            // set X & Y coordinates of Point pt to ClientRectangle Width & Height respectively
            pt.X = ClientRectangle.Width;
            pt.Y = ClientRectangle.Height;
            // get Last Index & Last Line from richTextBox1
            int Last_Index = richTextBox2.GetCharIndexFromPosition(pt);
            int Last_Line = richTextBox2.GetLineFromCharIndex(Last_Index);
            // set Center alignment to LineNumberTextBox
            richTextBox1.SelectionAlignment = HorizontalAlignment.Center;
            // set LineNumberTextBox text to null & width to getWidth() function value
            richTextBox1.Text = "";
            richTextBox1.Width = getWidth();
            // now add each line number to LineNumberTextBox upto last line
            for (int i = First_Line; i <= Last_Line + 2; i++)
            {
                richTextBox1.Text += i + 1 + "\n";
            }
        }

        public String getCodigo()
        {
            return richTextBox2.Text;
        }
    }
}