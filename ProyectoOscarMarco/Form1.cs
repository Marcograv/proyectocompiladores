﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;

namespace ProyectoOscarMarco
{
    public partial class Form1 : Form
    {
        static List<String> Cerrados = new List<String>();

        public static List<string> Cerrados1 { get => Cerrados; set => Cerrados = value; }
        public ArrayList erroreslineas = null;
        public Form1()
        {
            InitializeComponent();
            Codes inicial = new Codes("Panel Principal", "", "", this);
            this.tabControl1.Controls.Add(inicial);
            erroreslineas = new ArrayList();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.tabControl1.TabPages.Count > 0)
            {
                RichTextBox richTextBox =(RichTextBox) this.tabControl1.SelectedTab.Controls[2];
                RichTextBox richTextBox2 =(RichTextBox) this.tabControl1.SelectedTab.Controls[1];
                var codigo = this.tabControl1.SelectedTab.Controls[2].Text;
                erroreslineas = new ArrayList();
                var lines = richTextBox2.Lines;
                var output = Program.GetErroresMinics(codigo,this);
                foreach (int index in erroreslineas)
                {
                    if (richTextBox2.Lines.Length > index)
                    {
                        richTextBox2.Select(richTextBox2.GetFirstCharIndexFromLine(index - 1), richTextBox2.Lines[index].Length);
                        richTextBox2.SelectionBackColor = Color.Red;
                        /*richtext.SelectionStart = l;
                        richtext.SelectionLength = 0;
                        richtext.SelectionBackColor = Color.Red;*/
                        richTextBox2.DeselectAll();
                    }
                }
                this.tabControl1.SelectedTab.Controls[3].Text = "";
                this.tabControl1.SelectedTab.Controls[3].Text =
                    output + "\n EL ARCHIVO PARA LA PRUEBA MINICS esta en:\n " +
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PruebaMiniCS.txt");


            }
        }

        private void guardarArchivoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var codigo = this.tabControl1.SelectedTab.Controls[2].Text;
                var nombre = this.tabControl1.SelectedTab.Name;
                StreamWriter sw = new StreamWriter(nombre);
                sw.Write(this.tabControl1.SelectedTab.Controls[2].Text);
                sw.Close();
            }
            catch {

            }
        }

        private void guardarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = "Archivo de Texto|*.txt";
            if (sf.ShowDialog() == DialogResult.OK) {
                StreamWriter sw = new StreamWriter(sf.FileName);
                var codigo = this.tabControl1.SelectedTab.Controls[2].Text;
                sw.Write(codigo);
                sw.Close();

            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void abrirArchivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //ABRIR ARCHIVO
                string codigo = System.IO.File.ReadAllText(openFileDialog1.FileName);
                var path = openFileDialog1.FileName;
                string filename = Path.GetFileName(path);
                Codes nuevo = new Codes(filename, codigo, path, this);
                this.tabControl1.Controls.Add(nuevo);
                var size = this.tabControl1.TabPages.Count;
                this.tabControl1.SelectTab(size - 1);
            }
        }

        public TabControl TabControl1
        {
            get => tabControl1;
            set => tabControl1 = value;
        }
        public ToolStripItemCollection DropDownItem
        {
            get => Ultimos.DropDownItems;
        }
        public void selectlastab() {
            this.tabControl1.SelectTab(this.tabControl1.TabPages.Count - 1);
        }
        public ArrayList Lista {
            get => this.erroreslineas;
            set => this.erroreslineas = value;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RichTextBox richTextBox =(RichTextBox) this.tabControl1.SelectedTab.Controls[2];
            RichTextBox richTextBox2 =(RichTextBox) this.tabControl1.SelectedTab.Controls[1];
            var codigo = this.tabControl1.SelectedTab.Controls[2].Text;
            erroreslineas = new ArrayList();
            var lines = richTextBox2.Lines;
            var output = Program.GetErroresReflect(codigo,this);
            foreach (int index in erroreslineas)
            {
                if (richTextBox2.Lines.Length > index)
                {
                    richTextBox2.Select(richTextBox2.GetFirstCharIndexFromLine(index - 1), richTextBox2.Lines[index].Length);
                    richTextBox2.SelectionBackColor = Color.Red;
                    /*richtext.SelectionStart = l;
                    richtext.SelectionLength = 0;
                    richtext.SelectionBackColor = Color.Red;*/
                    richTextBox2.DeselectAll();
                }
            }
            this.tabControl1.SelectedTab.Controls[3].Text = "";
            this.tabControl1.SelectedTab.Controls[3].Text = output+"\nEL ARCHIVO GENERADO DE REFLECTIONS SE ENCUENTRA EN LA SIGUIENTE UBICACION: \n"+Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PruebaReflectios.exe");

        }
    }
}
