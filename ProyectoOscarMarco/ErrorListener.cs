﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using Antlr4.Runtime;

namespace ProyectoOscarMarco
{
    public class ErrorListener : BaseErrorListener
    {
        public ArrayList errores = null;
        public ArrayList listalineas = null;

        public ErrorListener()
        {
            errores = new ArrayList();
            listalineas = new ArrayList();
        }

        public override void SyntaxError(TextWriter output, IRecognizer recognizer, IToken offendingSymbol, int charPositionInLine, int i,
            string msg, RecognitionException recognitionException)
        {
            if (recognizer is Parser1)
            {
                var error = "PARSER ERROR - line " + charPositionInLine + ":" + i + " " + msg;
                Console.WriteLine(charPositionInLine);
                listalineas.Add(charPositionInLine);
                errores.Add(error);
            }
            else if (recognizer is LexerP)
            {
                var error = "SCANNER ERROR - line " + i + ":" + charPositionInLine + " " + msg;
                errores.Add(error);
            }

        }
        public bool HasErrors()
        {

            return errores.Count > 0;
        }

        public override string ToString()
        {
            if (HasErrors() == false)
            {
                
                return "No hay errores de Parser!!";
            }

            var Errores = "";
            foreach (var error in errores)
            {
                Errores += error + "\n";
            }
            
            return Errores;
        }
    }
}