lexer grammar LexerP;

//PALABRAS RESERVADAS
BREAK : 'break';
CLASS : 'class';
CONST : 'const';
ELSE  : 'else';
IF    : 'if';
NEW   : 'new';
READ  : 'read';
RETURN: 'return';
VOID  : 'void';
WHILE : 'while';
FOR   : 'for';
WRITE : 'write';
VAR   : 'var';
BOOL  : 'bool';
INT   : 'int';
FLOAT : 'float';
CHAR  : 'char';
SWITCH: 'switch';
CASE:   'case';
DEFAULT: 'default';
STRINGV: 'string';
//CHR:    'chr';
//NULL:   'null';
//ORD:    'ord';
//LEN:    'len';

//PATRONES
BOOLEAN  : TRUE | FALSE;
IDENT : LETTER(LETTER | DIGIT |'_')*;
NUMBER : [1-9][0-9]* | '0';
FLOATNUMBER : DIGIT '.' DIGIT+;
CHARCONST: '\'' PRINTABLECHAR  '\'';
STRING: '"' ( ESC | ~[\\"] )* '"';
TRUE: 'true';
FALSE: 'false';
//CARACTERES
fragment LETTER: [A-Za-z\u0080-\uFFFF_];
fragment DIGIT : '0'..'9';
fragment PRINTABLECHAR: LETTER|('\\n'|'\\r')|'!'|'"'|'#'|'$'|'%'|'&'|'\''|'('|')'|'*'|'+'|','|'-'|'.'|'/'|':'|';'|'<'|'='|'>'|'?'|'@'|' ';
fragment ESC : '\\"' | '\\\\' ;
//OPERADORES y SIMBOLOS
SUM:  '+';
RES:  '-';
MULT: '*';
DIV:  '/';
DIVMOD: '%';
IGUAL: '==';
DIF: '!=';
MENOR: '>';
MENORIGUAL: '>=';
MAYOR: '<';
MAYORIGUAL: '<=';
AND: '&&';
OR: '||';
ASIGNA: '=';
PLUS: '++';
RESTA: '--';
PYCOMA: ';';
COMA: ',';
PUNTO: '.';
PIZQ:'(';
PDER:')';
CORCHETEIZQ: '[';
CORCHETEDER: ']';
BRAKETIZQ: '{';
BRAKETDER: '}';
COMILLA :   '"';
DOSPUNTOS: ':';

COMMENT:   '/*' .*? '*/' -> skip;
LINE_COMMENT:   '//' ~[\r\n]* -> skip;

WS  :   [ \t\n\r]+ -> skip ;
