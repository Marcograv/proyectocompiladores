﻿using System.Windows.Forms;
using System;
using System.Drawing;
using System.IO;

namespace ProyectoOscarMarco
{
    public class ItemMenu: ToolStripMenuItem
    {
        private String path = null;
        private Form1 form = null;
        public ItemMenu(string p,Form1 form1)
        {
            form = form1;
            path = p;
            this.SetBounds(new Rectangle(new Point(100,100),new System.Drawing.Size(1000,130) ));
            this.Text = path;
            Console.WriteLine(this.Text);
            this.Name = path;
            
            this.Click += new EventHandler(AbrirPath);
        }

        void AbrirPath(object o, EventArgs e)
        {
            string codigo = System.IO.File.ReadAllText(path);
            Codes inicial = new Codes(Path.GetFileName(path),codigo,path,form);
            TabControl tab = form.TabControl1;
            tab.Controls.Add(inicial);
            
            form.TabControl1 = tab;
            form.selectlastab();
        }
    }
    
}