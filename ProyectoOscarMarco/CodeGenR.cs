﻿using System;
using System.Collections;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;

namespace ProyectoOscarMarco
{
    public class CodeGenR: Parser1BaseVisitor<object>
    {
        //REFLECTION!
        private TypeBuilder actglobal;
        private TypeBuilder act;
        private ILGenerator pmactual;
        private AssemblyBuilder myAsmBldr;
        private ModuleBuilder moduleBuilder;
        private MethodBuilder metact;
        private ArrayList clases;//CLASES
        private ArrayList localesParams;//PARA VARIABLES LOCALES (PARAMETROS)
        private ArrayList localesmet;//PARA VARIABLES LOCALES
        private ArrayList globales;//PARA VARIABLES GLOBALES
        private ArrayList metodos;//PARA METODOS
        private ArrayList contructores;//PARA CONTRUCTORES DE CLASES!
        private bool typelist;
        private bool retdesig;
        private ArrayList breaks;
        private ArrayList classFields;
        private bool desgcont;
        private ArrayList listf_porclase;
        public CodeGenR()
        {
            actglobal = null;
            act = null;
            pmactual = null;
            myAsmBldr = null;
            metact = null;
            clases= new ArrayList();
            globales=new ArrayList();
            localesParams= new ArrayList();
            localesmet = new ArrayList();
            metodos = new ArrayList();
            contructores = new ArrayList();
            typelist = false;
            retdesig = false;
            breaks= new ArrayList();
            desgcont = false;
            listf_porclase=new ArrayList();
        }

        public void Codigo(IParseTree ctx)
        {
            Type myType = (Type)Visit(ctx);
        }
        public static TypeBuilder temp(ModuleBuilder act, string nombre)
        {
            return (act).DefineType(nombre,TypeAttributes.Public);
        }
        public override object VisitProgramAST(Parser1.ProgramASTContext context)
        {
            Type pointType = null;
            AppDomain currentDom = Thread.GetDomain();
            string asmFileName = "PruebaReflections.exe";
            //assembly
            AssemblyName myAsmName = new AssemblyName();
            myAsmName.Name = "DynamicAssembly";
            myAsmBldr = currentDom.DefineDynamicAssembly(
                myAsmName,
                AssemblyBuilderAccess.RunAndSave);
            
            //module
            ModuleBuilder myModuleBldr = myAsmBldr.DefineDynamicModule(asmFileName,
                asmFileName,true);
            moduleBuilder = myModuleBldr;
            
            //type
            actglobal = temp(myModuleBldr,(string)Visit(context.classdecl()[0]));
            for (int i = 1; i < context.classdecl().Length; i++)
            {
                Visit(context.classdecl()[i]);
            }
            
            //CREACION DE VARS Y CONST GLOBALES Y CLASES INTERNAS!
            foreach (var vardeclContext in context.vardecl())
            {
                Parser1.VardeclASTContext v = (Parser1.VardeclASTContext)Visit(vardeclContext);
                string rtipo = (string)Visit(v.type());
                if (rtipo.Contains("LIST"))
                {
                    typelist = true;
                    if (rtipo.Equals("CHAR LIST"))
                    {
                        Parser1.ChartypeASTContext c =(Parser1.ChartypeASTContext) Visit(v.type());
                        ArrayList arr = null;
                        if (c.NUMBER()!=null)
                        {
                            arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                        }
                        else
                        {
                            arr = new ArrayList();
                        }
                        if (v.IDENT().Length>1)
                        {
                            foreach (var node in v.IDENT())
                            {
                                FieldBuilder field = actglobal.DefineField(node.GetText(),arr.GetType(),
                                    FieldAttributes.Private | FieldAttributes.Static);
                                globales.Add(field);
                            }
                        }
                        else
                        {
                            FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), arr.GetType(),
                                FieldAttributes.Private | FieldAttributes.Static);
                            globales.Add(field);
                        }
                    }else if (rtipo.Equals("INT LIST"))
                    {
                        Parser1.InttypeASTContext c =(Parser1.InttypeASTContext) Visit(v.type());
                        ArrayList arr = null;
                        if (c.NUMBER()!=null)
                        {
                            arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                        }
                        else
                        {
                            arr = new ArrayList();
                        }
                        if (v.IDENT().Length>1)
                        {
                            foreach (var node in v.IDENT())
                            {
                                FieldBuilder field = actglobal.DefineField(node.GetText(),arr.GetType(),
                                    FieldAttributes.Private | FieldAttributes.Static);
                                globales.Add(field);
                            }
                        }
                        else
                        {
                            FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), arr.GetType(),
                                FieldAttributes.Private | FieldAttributes.Static);
                            globales.Add(field);
                        }
                    }
                    else
                    {
                        Parser1.IdenttypeASTContext c =(Parser1.IdenttypeASTContext) Visit(v.type());
                        ArrayList arr = null;
                        if (c.NUMBER()!=null)
                        {
                            arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                        }
                        else
                        {
                            arr = new ArrayList();
                        }
                        if (v.IDENT().Length>1)
                        {
                            foreach (var node in v.IDENT())
                            {
                                FieldBuilder field = actglobal.DefineField(node.GetText(),arr.GetType(),
                                    FieldAttributes.Private | FieldAttributes.Static);
                                globales.Add(field);
                            }
                        }
                        else
                        {
                            FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), arr.GetType(),
                                FieldAttributes.Private | FieldAttributes.Static);
                            globales.Add(field);
                        }   
                    }

                    typelist = false;
                }
                else if (rtipo=="CHAR")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                           FieldBuilder field = actglobal.DefineField(node.GetText(), typeof(char),
                                FieldAttributes.Private | FieldAttributes.Static);
                           globales.Add(field);
                        }
                    }
                    else
                    {
                        FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), typeof(char),
                            FieldAttributes.Private | FieldAttributes.Static);
                        globales.Add(field);
                    }
                }else if (rtipo=="BOOL")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder field = actglobal.DefineField(node.GetText(), typeof(bool),
                                FieldAttributes.Private | FieldAttributes.Static);
                            globales.Add(field);
                        }
                    }
                    else
                    {
                        FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), typeof(bool),
                            FieldAttributes.Private | FieldAttributes.Static);
                        globales.Add(field);
                    }
                }else if (rtipo=="STRING")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder field = actglobal.DefineField(node.GetText(), typeof(string),
                                FieldAttributes.Private | FieldAttributes.Static);
                            globales.Add(field);
                        }
                    }
                    else
                    {
                        FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), typeof(string),
                            FieldAttributes.Private | FieldAttributes.Static);
                        globales.Add(field);
                    }
                }else if (rtipo=="FLOAT")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder field = actglobal.DefineField(node.GetText(), typeof(double),
                                FieldAttributes.Private | FieldAttributes.Static);
                            globales.Add(field);
                        }
                    }
                    else
                    {
                        FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), typeof(double),
                            FieldAttributes.Private | FieldAttributes.Static);
                        globales.Add(field);
                    }
                }else if (rtipo=="INT")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder field = actglobal.DefineField(node.GetText(), typeof(int),
                                FieldAttributes.Private | FieldAttributes.Static);
                            globales.Add(field);
                        }
                    }
                    else
                    {
                        FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), typeof(int),
                            FieldAttributes.Private | FieldAttributes.Static);
                        globales.Add(field);
                    }
                }
                else
                {
                    Type tipoclass = null;
                    foreach (var clase in clases)
                    {
                        if (rtipo==((TypeBuilder)clase).Name)
                        {
                            tipoclass = ((TypeBuilder) clase);
                        }
                    }
                    if (v.IDENT().Length>1)
                    {
                        
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder field = actglobal.DefineField(node.GetText(), tipoclass,
                                FieldAttributes.Private | FieldAttributes.Static);
                            globales.Add(field);
                        }
                    }
                    else
                    {
                        FieldBuilder field = actglobal.DefineField(v.IDENT()[0].GetText(), tipoclass,
                            FieldAttributes.Private | FieldAttributes.Static);
                        globales.Add(field);
                    }
                }
            }
            foreach (var constdeclContext in context.constdecl())
            {
                Parser1.ConstASTContext c= (Parser1.ConstASTContext)Visit(constdeclContext);
                FieldBuilder field;
                string rtipo = (string)Visit(c.type());
                if (rtipo=="CHAR")
                {
                    field=actglobal.DefineField(c.IDENT().GetText(), typeof(char),
                        FieldAttributes.Private | FieldAttributes.Literal);
                    string g = c.CHARCONST().GetText();
                    char[] cl = g.ToCharArray();
                    char ch = cl[1];
                    field.SetConstant(ch);
                    
                    
                }else if (rtipo=="STRING")
                {
                    field=actglobal.DefineField(c.IDENT().GetText(), typeof(string),
                        FieldAttributes.Private | FieldAttributes.Literal);
                    field.SetConstant(c.STRING().GetText());
                    
                    
                }else if (rtipo=="BOOL")
                {
                    field=actglobal.DefineField(c.IDENT().GetText(), typeof(bool),
                        FieldAttributes.Private | FieldAttributes.Literal);
                    if (c.BOOLEAN().GetText()=="true")
                    {
                        field.SetConstant(true);
                    }
                    else
                    {
                        
                        field.SetConstant(false);
                    }
                    
                }else
                {
                    field=actglobal.DefineField(c.IDENT().GetText(), typeof(int),
                        FieldAttributes.Private | FieldAttributes.Literal);
                    field.SetConstant(int.Parse(c.NUMBER().GetText()));
                }
                globales.Add(field);
            }
            Type objType = Type.GetType("System.Object");
            ConstructorInfo objCtor = objType.GetConstructor(new Type[0]);
            ConstructorBuilder pointCtor = actglobal.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard,
                null);
            ILGenerator ctorIL = pointCtor.GetILGenerator();
            ctorIL.Emit(OpCodes.Ldarg_0); 
            ctorIL.Emit(OpCodes.Ret);
            foreach (var met in context.methodecl())
            {
                MethodBuilder mt = (MethodBuilder)Visit(met);
                metodos.Add(mt);
            }
            //cierre
            actglobal.CreateType();
            myAsmBldr.Save(asmFileName);
            return actglobal;
        }

        public override object VisitConstAST(Parser1.ConstASTContext context)
        {
            if (act!=null)
            {
                string rtipo = (string)Visit(context.type());
                if (rtipo=="CHAR")
                {
                    var field=act.DefineField(context.IDENT().GetText(), typeof(char),
                        FieldAttributes.Private | FieldAttributes.Static | FieldAttributes.Literal);
                    field.SetConstant(char.Parse(context.CHARCONST().GetText()));
                }else if (rtipo=="BOOL")
                {
                    var field=act.DefineField(context.IDENT().GetText(), typeof(bool),
                        FieldAttributes.Private | FieldAttributes.Static | FieldAttributes.Literal);
                    if (context.BOOLEAN().GetText()=="true")
                    {
                        field.SetConstant(true);
                    }
                    else
                    {
                        
                        field.SetConstant(false);
                    }
                }else
                {
                    var field=act.DefineField(context.IDENT().GetText(), typeof(int),
                        FieldAttributes.Private | FieldAttributes.Static | FieldAttributes.Literal);
                    field.SetConstant(int.Parse(context.NUMBER().GetText()));
                }

                return null;
            }
            return context;
        }

        public override object VisitVardeclAST(Parser1.VardeclASTContext context)
        {
            return context;
        }

        public override object VisitClassdeclAST(Parser1.ClassdeclASTContext context)
        {
            if (actglobal==null)
            {
                return context.IDENT().GetText();
            }
            ArrayList listfield_class= new ArrayList();
            listfield_class.Add(context.IDENT().GetText());
            TypeBuilder tb = actglobal.DefineNestedType(context.IDENT().GetText(), TypeAttributes.NestedPublic, typeof(Object)); //DEFINO CLASE DENTRO DE CLASE
            foreach (var vardeclContext in context.vardecl())
            {
                Parser1.VardeclASTContext v = (Parser1.VardeclASTContext)Visit(vardeclContext);
                string rtipo = (string)Visit(v.type());
                if (rtipo=="CHAR")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder f =tb.DefineField(node.GetText(), typeof(char),FieldAttributes.Public);
                            listfield_class.Add(f);
                        }
                    }
                    else
                    {
                        FieldBuilder f= tb.DefineField(v.IDENT()[0].GetText(), typeof(char),
                            FieldAttributes.Public);
                        listfield_class.Add(f);
                    }
                }else if (rtipo=="BOOL")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder f= tb.DefineField(node.GetText(), typeof(bool),
                                FieldAttributes.Public);
                            listfield_class.Add(f);
                        }
                    }
                    else
                    {
                        FieldBuilder f= tb.DefineField(v.IDENT()[0].GetText(), typeof(bool),
                            FieldAttributes.Public);
                        listfield_class.Add(f);
                    }
                }else if (rtipo=="STRING")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder f= tb.DefineField(node.GetText(), typeof(string),
                                FieldAttributes.Public);
                            listfield_class.Add(f);
                        }
                    }
                    else
                    {
                        FieldBuilder f= tb.DefineField(v.IDENT()[0].GetText(), typeof(string),
                            FieldAttributes.Public);
                        listfield_class.Add(f);
                    }
                }else if (rtipo=="FLOAT")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder f= tb.DefineField(node.GetText(), typeof(double),
                                FieldAttributes.Public);
                            listfield_class.Add(f);
                        }
                    }
                    else
                    {
                        FieldBuilder f=  tb.DefineField(v.IDENT()[0].GetText(), typeof(double),
                            FieldAttributes.Public);
                        listfield_class.Add(f);
                    }
                }else if (rtipo=="INT")
                {
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder f= tb.DefineField(node.GetText(), typeof(int),
                                FieldAttributes.Public);
                            listfield_class.Add(f);
                        }
                    }
                    else
                    {
                        FieldBuilder f= tb.DefineField(v.IDENT()[0].GetText(), typeof(int),
                            FieldAttributes.Public);
                        listfield_class.Add(f);
                    } 
                }
                else
                {
                    Type tipo = actglobal.GetDeclaredNestedType(rtipo);
                    if (v.IDENT().Length>1)
                    {
                        foreach (var node in v.IDENT())
                        {
                            FieldBuilder f=  tb.DefineField(node.GetText(), tipo,
                                FieldAttributes.Public);
                            listfield_class.Add(f);
                        }
                    }
                    else
                    {
                        FieldBuilder f=tb.DefineField(v.IDENT()[0].GetText(),tipo,
                            FieldAttributes.Public);
                        listfield_class.Add(f);
                    }
                }
            }

            listf_porclase.Add(listfield_class);
            Type objType = Type.GetType("System.Object");
            ConstructorInfo objCtor = objType.GetConstructor(new Type[]{typeof(System.Object)});
            ConstructorBuilder pointCtor = tb.DefineDefaultConstructor(
                MethodAttributes.Public);
            ArrayList listc = new ArrayList();
            listc.Add(context.IDENT().GetText());
            listc.Add(pointCtor);
            contructores.Add(listc);
            tb.CreateType();
            clases.Add(tb);
            return context;
        }

        public override object VisitMethodeclAST(Parser1.MethodeclASTContext context)
        {

            string rtipo;
            MethodBuilder pointMainBldr;
            if (context.type()!=null)
            {
                rtipo = (string)Visit(context.type());
                if (rtipo.Contains("LIST"))
                {
                    typelist = true;
                    if (rtipo.Contains("CHAR"))
                    {
                        Type[] listatiposparams=null;
                        if (context.formpars()!=null)
                        {
                            listatiposparams =(Type[])Visit(context.formpars());
                        }
                        pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                            MethodAttributes.Public |
                            MethodAttributes.Static,
                            typeof(ArrayList),
                            listatiposparams);
                        metact = pointMainBldr;
                        if (context.formpars()!=null)
                        {
                            Visit(context.formpars());
                        }
                    }else if (rtipo.Contains("INT"))
                    {
                        Type[] listatiposparams=null;
                        if (context.formpars()!=null)
                        {
                            listatiposparams =(Type[])Visit(context.formpars());
                        }
                        pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                            MethodAttributes.Public |
                            MethodAttributes.Static,
                            typeof(ArrayList),
                            listatiposparams);
                        metact = pointMainBldr;
                        if (context.formpars()!=null)
                        {
                            Visit(context.formpars());
                        }
                    }
                    else
                    {
                        Type[] listatiposparams=null;
                        if (context.formpars()!=null)
                        {
                            listatiposparams =(Type[])Visit(context.formpars());
                        }
                        pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                            MethodAttributes.Public |
                            MethodAttributes.Static,
                            typeof(ArrayList),
                            listatiposparams);
                        metact = pointMainBldr;
                        if (context.formpars()!=null)
                        {
                            Visit(context.formpars());
                        }
                    }

                    typelist = false;
                }
                if (rtipo=="CHAR")
                {
                    Type[] listatiposparams=null;
                    if (context.formpars()!=null)
                    {
                        listatiposparams =(Type[])Visit(context.formpars());
                    }
                    pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                        MethodAttributes.Public |
                        MethodAttributes.Static,
                        typeof(char),
                        listatiposparams);
                    metact = pointMainBldr;
                    if (context.formpars()!=null)
                    {
                        Visit(context.formpars());
                    }
                }else if (rtipo=="BOOL")
                {
                    Type[] listatiposparams=null;
                    if (context.formpars()!=null)
                    {
                        listatiposparams =(Type[])Visit(context.formpars());
                    }
                    pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                        MethodAttributes.Public |
                        MethodAttributes.Static,
                        typeof(bool),
                        listatiposparams);
                    metact = pointMainBldr;
                    if (context.formpars()!=null)
                    {
                        Visit(context.formpars());
                    }
                }else if (rtipo=="STRING")
                {
                    Type[] listatiposparams=null;
                    if (context.formpars()!=null)
                    {
                        listatiposparams =(Type[])Visit(context.formpars());
                    }
                    pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                        MethodAttributes.Public |
                        MethodAttributes.Static,
                        typeof(string),
                        listatiposparams);
                    metact = pointMainBldr;
                    if (context.formpars()!=null)
                    {
                        Visit(context.formpars());
                    }
                }else if (rtipo=="FLOAT")
                {
                    Type[] listatiposparams=null;
                    if (context.formpars()!=null)
                    {
                        listatiposparams =(Type[])Visit(context.formpars());
                    }
                    pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                        MethodAttributes.Public |
                        MethodAttributes.Static,
                        typeof(double),
                        listatiposparams);
                    metact = pointMainBldr;
                    if (context.formpars()!=null)
                    {
                        Visit(context.formpars());
                    }
                }else if (rtipo=="INT")
                {
                    Type[] listatiposparams=null;
                    if (context.formpars()!=null)
                    {
                        listatiposparams =(Type[])Visit(context.formpars());
                    }

                    pointMainBldr = actglobal.DefineMethod(context.IDENT().GetText(),
                        MethodAttributes.Public |
                        MethodAttributes.Static,
                        typeof(int),
                        listatiposparams);
                    metact = pointMainBldr;
                    if (context.formpars()!=null)
                    {
                        Visit(context.formpars());
                    }
                }
                else
                {
                    Type tipo = null;
                    foreach (var clase in clases)
                    {
                        if (rtipo== ((TypeBuilder)clase).Name)
                        {
                            tipo = ((TypeBuilder) clase);
                        }
                    }
                    Type[] listatiposparams=null;
                    if (context.formpars()!=null)
                    {
                        listatiposparams =(Type[])Visit(context.formpars());
                    }
                    pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                        MethodAttributes.Public |
                        MethodAttributes.Static,
                        tipo,
                        listatiposparams);
                    metact = pointMainBldr;
                    if (context.formpars()!=null)
                    {
                        Visit(context.formpars());
                    }
                }
            }
            else
            {
                Type[] listatiposparams=null;
                if (context.formpars()!=null)
                {
                    listatiposparams =(Type[])Visit(context.formpars());
                }
                pointMainBldr=actglobal.DefineMethod(context.IDENT().GetText(),
                    MethodAttributes.Public |
                    MethodAttributes.Static,
                    typeof(void),
                    listatiposparams);
                metact = pointMainBldr;
                if (context.formpars()!=null)
                {
                    Visit(context.formpars());
                }
                
            }
            
            pmactual = pointMainBldr.GetILGenerator();
            foreach (var vardeclContext in context.vardecl())
            {
               Parser1.VardeclASTContext v= (Parser1.VardeclASTContext)Visit(vardeclContext);
               Addvars(v,pointMainBldr);
            }
            
            Visit(context.block());
            var tipedecl = metact.ReturnType;
            if (tipedecl== typeof(void))
            {
                pmactual.Emit(OpCodes.Nop);
            }
            pmactual.Emit(OpCodes.Ret);
            myAsmBldr.SetEntryPoint(pointMainBldr);
            pmactual = null;
            metact = null;
            /*foreach (ArrayList local in localesmet)
            {
                LocalBuilder lb = (LocalBuilder)(local[0]);
                lb.SetLocalSymInfo((string)local[1]);
            }*/
            localesmet = new ArrayList();
            localesParams= new ArrayList();
            return pointMainBldr;
        }

        public override object VisitFormparsAST(Parser1.FormparsASTContext context)
        {
            if (metact==null)
            {
                Type[] lista= new Type[context.type().Length];
                int i = 0;
            foreach (var typeContext in context.type())
            {
                lista[i]=typeContext.GetType();
                if (Visit(typeContext).GetType()==typeof(string))
                {
                    if (((string)Visit(typeContext))=="INT")
                    {
                        lista[i]=typeof(int);
                        
                    }
                    else if (((string)Visit(typeContext))=="FLOAT")
                    {
                        lista[i]=typeof(double);
                    }
                    else if (((string)Visit(typeContext))=="STRING")
                    {
                        lista[i]=typeof(string);
                    }
                    else if (((string)Visit(typeContext))=="CHAR")
                    {
                        lista[i]=typeof(char);
                    }
                    else if (((string)Visit(typeContext))=="BOOL")
                    {
                        lista[i]=typeof(bool);
                    }
                    else
                    {
                        string tp = ((string) Visit(typeContext));
                        Type tipoclass = null;
                        foreach (var clase in clases)
                        {
                            if (tp==((TypeBuilder)clase).Name)
                            {
                                tipoclass = ((TypeBuilder) clase);
                            }
                        }
                        lista[i]=tipoclass;
                    }
                }
                
                i++;
            }

            return lista;
            }
            else
            {
                int k = 1;
                foreach (var node in context.IDENT())
                {
                    ParameterBuilder param = metact.DefineParameter(k, ParameterAttributes.Lcid, node.GetText());
                    k++;
                    localesParams.Add(param);
                }

                return null;
            }
        }

        public override object VisitChartypeAST(Parser1.ChartypeASTContext context)
        {
            if (typelist)
            {
                return context;
            }
            if (context.CORCHETEDER()!=null)
            {
                return "CHAR LIST";
            }
            return "CHAR";
        }

        public override object VisitInttypeAST(Parser1.InttypeASTContext context)
        {
            if (typelist)
            {
                return context;
            }
            if (context.CORCHETEDER()!=null)
            {
                return "INT LIST";
            }
            return "INT";
        }

        public override object VisitFloattypeAST(Parser1.FloattypeASTContext context)
        {
            return "FLOAT";
        }

        public override object VisitBooltypeAST(Parser1.BooltypeASTContext context)
        {
            return "BOOL";
        }

        public override object VisitIdenttypeAST(Parser1.IdenttypeASTContext context)
        {
            if (typelist)
            {
                return context;
            }
            if (context.CORCHETEDER()!=null)
            {
                return context.IDENT()[0].GetText()+" LIST";
            }
            return context.IDENT()[0].GetText();
        }

        public override object VisitStringtypeAST(Parser1.StringtypeASTContext context)
        {
            return "STRING";
        }

        public override object VisitDesignatorSTMAST(Parser1.DesignatorSTMASTContext context)
        {
            if (context.ASIGNA()!=null)
            {
                if (context.expr()!=null)
                {
                    var exp = Visit(context.expr());
                    desgcont = true;
                    if (Visit(context.designator()).GetType()== typeof(Parser1.DesignatorASTContext))
                    {
                        desgcont = true;
                        Parser1.DesignatorASTContext desg = (Parser1.DesignatorASTContext) Visit(context.designator());
                        desgcont = false;
                        if (desg.IDENT().Length>=2)
                        {
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                                pmactual.Emit(OpCodes.Ldsfld,is_global);
                            }
                            else
                            {
                                
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param!=-1)
                                {
                                    if (localesParams.Count==1)
                                    {
                                        
                                        pmactual.Emit(OpCodes.Ldarg_S);
                                    }
                                    else
                                    {
                                        pmactual.Emit(OpCodes.Ldarg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Ldloc,local);
                                }
                            }
                        }
                    }
                    desgcont = false;
                    if (exp==null)
                    {
                        
                        FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                        if (is_global!=null)
                        {
                            pmactual.Emit(OpCodes.Stsfld,is_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(context.designator()));
                            if (param!=-1)
                            {
                                if (localesParams.Count==1)
                                {
                                    pmactual.Emit(OpCodes.Starg_S);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Starg_S, param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                pmactual.Emit(OpCodes.Stloc,local);
                            }
                        }

                        return null;
                    }
                    else if (exp.GetType()==typeof(Parser1.NewFACASTContext))
                    {
                        string nombreclase = (string) Visit(((Parser1.NewFACASTContext)exp).type());
                        ConstructorBuilder cB = buscarC(nombreclase);
                        if (cB==null)
                        {
                            if (nombreclase.Contains("LIST"))
                            {
                                typelist = true;
                                var tp = Visit(((Parser1.NewFACASTContext) exp).type());
                                if (tp is Parser1.ChartypeASTContext)
                                {
                                    if (((Parser1.ChartypeASTContext)tp).NUMBER()!=null)
                                    {
                                        ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                        pmactual.Emit(OpCodes.Ldc_I4_S,int.Parse(((Parser1.ChartypeASTContext)tp).NUMBER().GetText()));
                                        pmactual.Emit(OpCodes.Newobj,L[1]);
                                        typelist = false;
                                    }
                                    else
                                    {
                                        ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                        pmactual.Emit(OpCodes.Newobj,L[0]);
                                        typelist = false;
                                    }
                                }
                                if (tp is Parser1.IdenttypeASTContext)
                                {
                                    if (((Parser1.IdenttypeASTContext) tp).NUMBER() != null)
                                    {
                                        ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                        pmactual.Emit(OpCodes.Ldc_I4_S,int.Parse(((Parser1.IdenttypeASTContext)tp).NUMBER().GetText()));
                                        pmactual.Emit(OpCodes.Newobj,L[1]);
                                        typelist = false;
                                    }
                                    else
                                    {
                                        ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                        pmactual.Emit(OpCodes.Newobj,L[0]);
                                        typelist = false;
                                    }
                                }
                                else
                                {
                                    if (((Parser1.InttypeASTContext) tp).NUMBER() != null)
                                    {
                                        ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                        pmactual.Emit(OpCodes.Ldc_I4_S,int.Parse(((Parser1.InttypeASTContext)tp).NUMBER().GetText()));
                                        pmactual.Emit(OpCodes.Newobj,L[1]);
                                        typelist = false;
                                    }
                                    else
                                    {
                                        ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                        pmactual.Emit(OpCodes.Newobj,L[0]);
                                        typelist = false;
                                    }
                                }
                            }
                        }
                        if (cB!=null)
                        {
                            pmactual.Emit(OpCodes.Newobj,cB);
                        }
                        FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                        if (is_global!=null)
                        {
                            pmactual.Emit(OpCodes.Stsfld,is_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(context.designator()));
                            if (param!=-1)
                            {
                                if (localesParams.Count==1)
                                {
                                    pmactual.Emit(OpCodes.Starg_S);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Starg_S, param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                pmactual.Emit(OpCodes.Stloc,local);
                            }
                        }
                    }
                    else if (exp!=null)
                    {
                        desgcont = true;
                        Parser1.DesignatorASTContext desg = (Parser1.DesignatorASTContext) Visit(context.designator());
                        desgcont = false;
                        if (desg.IDENT().Length>=2)
                        {
                            if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                            {
                                FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                                if (is_global!=null)
                                {
                                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                    var nc = is_global.DeclaringType.Name;
                                    foreach (ArrayList cls in listf_porclase)
                                    {
                                        if (cls[0].ToString() == nc)
                                        {
                                            for (int i = 1; i < cls.Count; i++)
                                            {
                                                FieldBuilder fls = (FieldBuilder) cls[i];
                                                if (fls.Name==desg.IDENT()[1].GetText())
                                                {
                                                    pmactual.Emit(OpCodes.Stfld,fls);
                                                }
                                            }
                                        }
                                    }
                                    pmactual.Emit(OpCodes.Stsfld,is_global);
                                }
                                else
                                {
                                    int param = buscarparam((string) Visit(context.designator()));
                                    if (param!=-1)
                                    {
                                        if (localesParams.Count==1)
                                        {
                                            pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                            pmactual.Emit(OpCodes.Starg_S);
                                        }
                                        else
                                        {
                                            pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                            pmactual.Emit(OpCodes.Starg_S, param);
                                        }
                                    }
                                    else
                                    {
                                        LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                        var nc = local.LocalType.Name;
                                        foreach (ArrayList cls in listf_porclase)
                                        {
                                            if (cls[0].ToString() == nc)
                                            {
                                                for (int i = 1; i < cls.Count; i++)
                                                {
                                                    FieldBuilder fls = (FieldBuilder) cls[i];
                                                    if (fls.Name==desg.IDENT()[1].GetText())
                                                    {
                                                        pmactual.Emit(OpCodes.Stfld,fls);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }if (exp.GetType()==typeof(Parser1.DesignatorFACASTContext))
                            {
                                FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                                if (is_global!=null)
                                {
                                    FieldBuilder ig =
                                        buscarglobal(
                                            (string) Visit(((Parser1.DesignatorFACASTContext) exp).designator()));
                                    if (ig!=null)
                                    {
                                        pmactual.Emit(OpCodes.Ldfld,ig);
                                        var nc = is_global.DeclaringType.Name;
                                        foreach (ArrayList cls in listf_porclase)
                                        {
                                            
                                                for (int i = 1; i < cls.Count; i++)
                                                {
                                                    FieldBuilder fls = (FieldBuilder) cls[i];
                                                    if (fls.Name == desg.IDENT()[1].GetText())
                                                    {
                                                        pmactual.Emit(OpCodes.Stfld, fls);
                                                    }
                                                }
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    int param = buscarparam((string) Visit(context.designator()));
                                    if (param!=-1)
                                    {
                                        if (localesParams.Count==1)
                                        {
                                            
                                            pmactual.Emit(OpCodes.Starg_S);
                                        }
                                        else
                                        {
                                            pmactual.Emit(OpCodes.Starg_S, param);
                                        }
                                    }
                                    else
                                    {
                                        LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext) exp).designator()));
                                        pmactual.Emit(OpCodes.Ldloc,local);
                                        var nc = local.LocalType.Name;
                                        foreach (ArrayList cls in listf_porclase)
                                        {
                                            for (int i = 1; i < cls.Count; i++)
                                            {
                                                    FieldBuilder fls = (FieldBuilder) cls[i];
                                                    if (fls.Name==desg.IDENT()[1].GetText())
                                                    {
                                                        pmactual.Emit(OpCodes.Stfld,fls);
                                                    }
                                            }
                                        }
                                    }
                                }
                            }if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                            {
                                FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                                if (is_global!=null)
                                {
                                   
                                    var nc = is_global.DeclaringType.Name;
                                    foreach (ArrayList cls in listf_porclase)
                                    {
                                        if (cls[0].ToString() == nc)
                                        {
                                            for (int i = 1; i < cls.Count; i++)
                                            {
                                                FieldBuilder fls = (FieldBuilder) cls[i];
                                                if (fls.Name==desg.IDENT()[1].GetText())
                                                {
                                                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                                    pmactual.Emit(OpCodes.Stfld,fls);
                                                    
                                                }
                                            }
                                        }
                                    }
                                    pmactual.Emit(OpCodes.Stsfld,is_global);
                                }
                                else
                                {
                                    int param = buscarparam((string) Visit(context.designator()));
                                    if (param!=-1)
                                    {
                                        if (localesParams.Count==1)
                                        {
                                            pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                            pmactual.Emit(OpCodes.Starg_S);
                                        }
                                        else
                                        {
                                            pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                            pmactual.Emit(OpCodes.Starg_S, param);
                                        }
                                    }
                                    else
                                    {
                                        LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                        var nc = local.LocalType.Name;
                                        foreach (ArrayList cls in listf_porclase)
                                        {
                                            if (cls[0].ToString() == nc)
                                            {
                                                for (int i = 1; i < cls.Count; i++)
                                                {
                                                    FieldBuilder fls = (FieldBuilder) cls[i];
                                                    if (fls.Name == desg.IDENT()[1].GetText())
                                                    {
                                                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                                        pmactual.Emit(OpCodes.Stfld,fls);
                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }if (exp.GetType()==typeof(Parser1.StringFACASTContext))
                            {
                                FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                                if (is_global!=null)
                                {
                                    
                                    var nc = is_global.DeclaringType.Name;
                                    foreach (ArrayList cls in listf_porclase)
                                    {
                                        if (cls[0].ToString() == nc)
                                        {
                                            for (int i = 1; i < cls.Count; i++)
                                            {
                                                FieldBuilder fls = (FieldBuilder) cls[i];
                                                if (fls.Name==desg.IDENT()[1].GetText())
                                                {
                                                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).STRING().GetText());
                                                    pmactual.Emit(OpCodes.Stfld,fls);
                                                }
                                            }
                                        }
                                    }
                                    pmactual.Emit(OpCodes.Stsfld,is_global);
                                }
                                else
                                {
                                    int param = buscarparam((string) Visit(context.designator()));
                                    if (param!=-1)
                                    {
                                        if (localesParams.Count==1)
                                        {
                                            pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).STRING().GetText());
                                            pmactual.Emit(OpCodes.Starg_S);
                                        }
                                        else
                                        {
                                            pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).STRING().GetText());
                                            pmactual.Emit(OpCodes.Starg_S, param);
                                        }
                                    }
                                    else
                                    {
                                        LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).STRING().GetText());
                                        var nc = local.LocalType.Name;
                                        foreach (ArrayList cls in listf_porclase)
                                        {
                                            if (cls[0].ToString() == nc)
                                            {
                                                for (int i = 1; i < cls.Count; i++)
                                                {
                                                    FieldBuilder fls = (FieldBuilder) cls[i];
                                                    if (fls.Name==desg.IDENT()[1].GetText())
                                                    {
                                                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).STRING().GetText());
                                                        pmactual.Emit(OpCodes.Stfld,fls);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }if (exp.GetType()==typeof(Parser1.CharFACASTContext))
                            {
                                FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                                if (is_global!=null)
                                {
                                    
                                    var nc = is_global.DeclaringType.Name;
                                    foreach (ArrayList cls in listf_porclase)
                                    {
                                        if (cls[0].ToString() == nc)
                                        {
                                            for (int i = 1; i < cls.Count; i++)
                                            {
                                                FieldBuilder fls = (FieldBuilder) cls[i];
                                                if (fls.Name==desg.IDENT()[1].GetText())
                                                {
                                                    string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                                                    char[] c = g.ToCharArray();
                                                    char ch = c[1];
                                                    pmactual.Emit(OpCodes.Ldc_I4_S,ch);
                                                    pmactual.Emit(OpCodes.Stfld,fls);
                                                }
                                            }
                                        }
                                    }
                                    pmactual.Emit(OpCodes.Stsfld,is_global);
                                }
                                else
                                {
                                    int param = buscarparam((string) Visit(context.designator()));
                                    if (param!=-1)
                                    {
                                        if (localesParams.Count==1)
                                        {
                                            string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                                            char[] c = g.ToCharArray();
                                            char ch = c[1];
                                            pmactual.Emit(OpCodes.Ldc_I4_S,ch);
                                            pmactual.Emit(OpCodes.Starg_S);
                                        }
                                        else
                                        {
                                            string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                                            char[] c = g.ToCharArray();
                                            char ch = c[1];
                                            pmactual.Emit(OpCodes.Ldc_I4_S,ch);
                                            pmactual.Emit(OpCodes.Starg_S, param);
                                        }
                                    }
                                    else
                                    {
                                        LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                        var nc = local.LocalType.Name;
                                        foreach (ArrayList cls in listf_porclase)
                                        {
                                            if (cls[0].ToString() == nc)
                                            {
                                                for (int i = 1; i < cls.Count; i++)
                                                {
                                                    FieldBuilder fls = (FieldBuilder) cls[i];
                                                    if (fls.Name==desg.IDENT()[1].GetText())
                                                    {
                                                        string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                                                        char[] c = g.ToCharArray();
                                                        char ch = c[1];
                                                        pmactual.Emit(OpCodes.Ldc_I4_S,ch);
                                                        pmactual.Emit(OpCodes.Stfld,fls);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
    
                            return null;
                        }

                        if (desg.CORCHETEIZQ().Length != 0) 
                        {
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                                pmactual.Emit(OpCodes.Ldsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param!=-1)
                                {
                                    if (localesParams.Count==1)
                                    {
                                        pmactual.Emit(OpCodes.Ldarg);
                                    }
                                    else
                                    {
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Ldloc,local);
                                }
                            }
                            var indexs = (Visit(desg.expr()[0]));
                            if (indexs is Parser1.NumFACASTContext)
                            {
                                
                                int index = int.Parse(((Parser1.NumFACASTContext)indexs).NUMBER().GetText());
                                pmactual.Emit(OpCodes.Ldc_I4_S,index);
                                //valor 
                                if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                                {
                                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                }else if (exp.GetType() == typeof(Parser1.CharFACASTContext))
                                {
                                    string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                                    char[] c = g.ToCharArray();
                                    char ch = c[1];
                                    pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                                }
                                pmactual.Emit(OpCodes.Box,typeof(int));
                                MethodInfo insert = typeof(ArrayList).GetMethod(
                                    "Insert",
                                    new Type[] {typeof(Int32), typeof(Int32)});
                                pmactual.EmitCall(OpCodes.Callvirt,insert,null);
                            }
                            else
                            {
                                FieldBuilder is_global2 = buscarglobal((string) indexs);
                                if (is_global2!=null)
                                {
                                    pmactual.Emit(OpCodes.Ldsfld,is_global2);
                                    if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                                    {
                                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                    }else if (exp.GetType() == typeof(Parser1.CharFACASTContext))
                                    {
                                        string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                                        char[] c = g.ToCharArray();
                                        char ch = c[1];
                                        pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                                    }
                                   
                                    pmactual.Emit(OpCodes.Box,typeof(char));
                                    MethodInfo insert = typeof(ArrayList).GetMethod(
                                        "Insert",
                                        new Type[] {typeof(Int32), typeof(char)});
                                    pmactual.EmitCall(OpCodes.Callvirt,insert,null);
                                }
                                else
                                {
                                    int param = buscarparam((string) indexs);
                                    if (param!=-1)
                                    {
                                        if (localesParams.Count==1)
                                        {
                                            pmactual.Emit(OpCodes.Ldarg);
                                            if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                                            {
                                                pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                            }else if (exp.GetType() == typeof(Parser1.CharFACASTContext))
                                            {
                                                string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                                                char[] c = g.ToCharArray();
                                                char ch = c[1];
                                                pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                                            }
                                            
                                            //pmactual.Emit(OpCodes.Box);
                                            MethodInfo insert = typeof(ArrayList).GetMethod(
                                                "Insert",
                                                new Type[] {typeof(Int32), typeof(System.Object)});
                                            pmactual.EmitCall(OpCodes.Callvirt,insert,null);
                                        }
                                        else
                                        {
                                            pmactual.Emit(OpCodes.Starg_S, param);
                                            if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                                            {
                                                pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                            }else if (exp.GetType() == typeof(Parser1.CharFACASTContext))
                                            {
                                                string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                                                char[] c = g.ToCharArray();
                                                char ch = c[1];
                                                pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                                            }
                                            
                                            //pmactual.Emit(OpCodes.Box);
                                            MethodInfo insert = typeof(ArrayList).GetMethod(
                                                "Insert",
                                                new Type[] {typeof(Int32), typeof(System.Object)});
                                            pmactual.EmitCall(OpCodes.Callvirt,insert,null);
                                        }
                                    }
                                    else
                                    {
                                        LocalBuilder local = buscarlocal((string) indexs);
                                        pmactual.Emit(OpCodes.Ldloc,local);
                                        MethodInfo insert = typeof(ArrayList).GetMethod(
                                            "Insert",
                                            new Type[] {typeof(Int32), typeof(System.Object)});
                                        pmactual.EmitCall(OpCodes.Callvirt,insert,null);
                                    }
                                }
                            }
                            return null;
                        }
                        else if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                        {
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                                pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                pmactual.Emit(OpCodes.Stsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param!=-1)
                                {
                                    if (localesParams.Count==1)
                                    {
                                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                        pmactual.Emit(OpCodes.Starg_S);
                                    }
                                    else
                                    {
                                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp).NUMBER().GetText()));
                                    pmactual.Emit(OpCodes.Stloc,local);
                                }
                            }
                        }else if (exp.GetType()==typeof(Parser1.CharFACASTContext))
                        {
                            string g = ((Parser1.CharFACASTContext) exp).CHARCONST().GetText();
                            char[] c = g.ToCharArray();
                            char ch = c[1];
                            
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                                pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                                pmactual.Emit(OpCodes.Stsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param!=-1)
                                {
                                    if (localesParams.Count==1)
                                    {
                                        pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                                        pmactual.Emit(OpCodes.Starg_S);
                                    }
                                    else
                                    {
                                        pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                                    pmactual.Emit(OpCodes.Stloc,local);
                                }
                            }
                        }else if (exp.GetType()==typeof(Parser1.BooleanFACASTContext))
                        {
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                                pmactual.Emit(OpCodes.Ldstr,((Parser1.BooleanFACASTContext)exp).BOOLEAN().GetText());
                                pmactual.Emit(OpCodes.Stsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param!=-1)
                                {
                                    if (localesParams.Count==1)
                                    {
                                        pmactual.Emit(OpCodes.Ldc_I4,((Parser1.BooleanFACASTContext)exp).BOOLEAN().GetText());
                                        pmactual.Emit(OpCodes.Starg_S);
                                    }
                                    else
                                    {
                                        pmactual.Emit(OpCodes.Ldc_I4,((Parser1.BooleanFACASTContext)exp).BOOLEAN().GetText());
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Ldc_I4,((Parser1.BooleanFACASTContext)exp).BOOLEAN().GetText());
                                    pmactual.Emit(OpCodes.Stloc,local);
                                }
                            }
                        }else if (exp.GetType()==typeof(Parser1.StringFACASTContext))
                        {
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                                pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).GetText());
                                pmactual.Emit(OpCodes.Stsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param!=-1)
                                {
                                    if (localesParams.Count==1)
                                    {
                                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).GetText());
                                        pmactual.Emit(OpCodes.Starg_S);
                                    }
                                    else
                                    {
                                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).GetText());
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp).GetText());
                                    pmactual.Emit(OpCodes.Stloc,local);
                                }
                            }
                        }else if (exp.GetType()==typeof(Parser1.FloatFACASTContext))
                        {
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                                pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp).FLOATNUMBER().GetText()));
                                pmactual.Emit(OpCodes.Stsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param!=-1)
                                {
                                    if (localesParams.Count==1)
                                    {
                                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp).FLOATNUMBER().GetText()));
                                        pmactual.Emit(OpCodes.Starg_S);
                                    }
                                    else
                                    {
                                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp).FLOATNUMBER().GetText()));
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp).FLOATNUMBER().GetText()));
                                    pmactual.Emit(OpCodes.Stloc,local);
                                }
                            }
                        }
                    }
                }

                if (Visit(context.expr())!=null)
                {
                    if (Visit(context.expr()).GetType() == typeof(Parser1.DesignatorFACASTContext))
                    {
                        Parser1.DesignatorFACASTContext ds = (Parser1.DesignatorFACASTContext) Visit(context.expr());
                        if (((string) Visit(ds.designator())).Equals("len"))
                        {
                            Visit(ds.actpars()[0]);
                            MethodInfo count = typeof(ArrayList).GetMethod(
                                "get_Count",
                                new Type[] { });
                            pmactual.EmitCall(OpCodes.Callvirt,count,null);
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                                
                                pmactual.Emit(OpCodes.Stsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param != -1)
                                {
                                    if (localesParams.Count == 1)
                                    {
                                   
                                        pmactual.Emit(OpCodes.Starg_S);
                                    }
                                    else
                                    {
                                   
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Stloc, local);
                                    local.SetLocalSymInfo((string) Visit(context.designator()));
                                
                                }
                            }
                            return null;
                        }
                        if (ds.actpars() != null)
                        {
                            Visit(ds.actpars()[0]);
                            pmactual.Emit(OpCodes.Call,buscarMet((string) Visit(ds.designator())));
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            if (is_global!=null)
                            {
                            
                                pmactual.Emit(OpCodes.Stsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param != -1)
                                {
                                    if (localesParams.Count == 1)
                                    {
                                   
                                        pmactual.Emit(OpCodes.Starg_S);
                                    }
                                    else
                                    {
                                   
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Stloc, local);
                                    local.SetLocalSymInfo((string) Visit(context.designator()));
                                
                                }
                            }
                        }
                        else
                        {
                            FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                            MethodBuilder met = buscarMet((string) Visit(ds.designator()));
                            if (is_global!=null)
                            {
                                pmactual.Emit(OpCodes.Call,met);
                                pmactual.Emit(OpCodes.Stsfld,is_global);
                            }
                            else
                            {
                                int param = buscarparam((string) Visit(context.designator()));
                                if (param != -1)
                                {
                                    if (localesParams.Count == 1)
                                    {
                                        pmactual.Emit(OpCodes.Call,met);
                                        pmactual.Emit(OpCodes.Starg_S);
                                    }
                                    else
                                    {
                                        pmactual.Emit(OpCodes.Call,met);
                                        pmactual.Emit(OpCodes.Starg_S, param);
                                    }
                                }
                                else
                                {
                                    LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                                    pmactual.Emit(OpCodes.Call,met);
                                    pmactual.Emit(OpCodes.Stloc, local);
                                    local.SetLocalSymInfo((string) Visit(context.designator()));
                                
                                }
                            }
                        }
                    }
                }
            }
            else if (context.PLUS() != null)
            {
                FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                if (is_global!=null)
                {
                    pmactual.Emit(OpCodes.Ldsfld,is_global);
                    pmactual.Emit(OpCodes.Ldc_I4_1);
                    pmactual.Emit(OpCodes.Add);
                    pmactual.Emit(OpCodes.Stsfld,is_global);
                }
                else
                {
                    int param = buscarparam((string) Visit(context.designator()));
                    if (param != -1)
                    {
                        if (localesParams.Count == 1)
                        {
                            pmactual.Emit(OpCodes.Ldarg_0);
                            pmactual.Emit(OpCodes.Ldc_I4_1);
                            pmactual.Emit(OpCodes.Add);
                            pmactual.Emit(OpCodes.Starg_S);
                        }
                        else
                        {
                            pmactual.Emit(OpCodes.Ldarg_S,param);
                            pmactual.Emit(OpCodes.Ldc_I4_1);
                            pmactual.Emit(OpCodes.Add);
                            pmactual.Emit(OpCodes.Starg_S, param);

                        }
                    }
                    else
                    {
                        LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                        pmactual.Emit(OpCodes.Ldloc,local);
                        pmactual.Emit(OpCodes.Ldc_I4_1);
                        pmactual.Emit(OpCodes.Add);
                        pmactual.Emit(OpCodes.Stloc, local);
                        local.SetLocalSymInfo((string) Visit(context.designator()));
                    }
                }
            }
            else if (context.RESTA()!=null)
            {
                FieldBuilder is_global = buscarglobal((string) Visit(context.designator()));
                if (is_global!=null)
                {
                    pmactual.Emit(OpCodes.Ldsfld,is_global);
                    pmactual.Emit(OpCodes.Ldc_I4_1);
                    pmactual.Emit(OpCodes.Sub);
                    pmactual.Emit(OpCodes.Stsfld,is_global);

                }
                else
                {
                    int param = buscarparam((string) Visit(context.designator()));
                    if (param != -1)
                    {
                        if (localesParams.Count == 1)
                        {
                            pmactual.Emit(OpCodes.Ldarg_0);
                            pmactual.Emit(OpCodes.Ldc_I4_1);
                            pmactual.Emit(OpCodes.Sub);
                            pmactual.Emit(OpCodes.Starg_S);
                        }
                        else
                        {
                            pmactual.Emit(OpCodes.Ldarg_S,param);
                            pmactual.Emit(OpCodes.Ldc_I4_1);
                            pmactual.Emit(OpCodes.Sub);
                            pmactual.Emit(OpCodes.Starg_S, param);
                        }
                    }
                    else
                    {
                        LocalBuilder local = buscarlocal((string) Visit(context.designator()));
                        pmactual.Emit(OpCodes.Ldloc,local);
                        pmactual.Emit(OpCodes.Ldc_I4_1);
                        pmactual.Emit(OpCodes.Sub);
                        pmactual.Emit(OpCodes.Stloc, local);
                        local.SetLocalSymInfo((string) Visit(context.designator()));
                    }

                }
                    
            }
            else if (context.PDER() != null)
            {
                
                if (context.actpars() != null)
                {
                    Visit(context.actpars());
                    pmactual.Emit(OpCodes.Call,buscarMet(((string) Visit(context.designator()))));
                }
                else
                {
                    pmactual.Emit(OpCodes.Call,buscarMet(((string) Visit(context.designator()))));
                }
            }

            return null;
        }

        public override object VisitIfSTMAST(Parser1.IfSTMASTContext context)
        {
            if (context.ELSE()==null)
            {
                
                Label iff = pmactual.DefineLabel();
                Visit(context.condition());
                pmactual.Emit(OpCodes.Brfalse,iff);
                Visit(context.statement()[0]);
                pmactual.MarkLabel(iff);
            }
            else
            {
                Visit(context.condition());
                Label iff = pmactual.DefineLabel();
                Label vElse = pmactual.DefineLabel();
                pmactual.Emit(OpCodes.Brfalse,vElse);
                Visit(context.statement()[0]);
                pmactual.Emit(OpCodes.Br,iff);
                pmactual.MarkLabel(vElse);
                Visit(context.statement()[1]);
                pmactual.MarkLabel(iff);
            }

            return null;
        }
        
        
        public override object VisitForSTMAST(Parser1.ForSTMASTContext context)
        {
            Label fallado = pmactual.DefineLabel();
            Label condition = pmactual.DefineLabel(); 
            breaks.Add(fallado);
            pmactual.MarkLabel(condition);

            Visit(context.condition());
            pmactual.Emit(OpCodes.Brfalse,fallado);

            Visit(context.statement()[1]);//NO SE VISITA EXPR (OCURREN ERRORES!!)

            Visit(context.statement()[0]);
            pmactual.Emit(OpCodes.Br,condition);
            pmactual.MarkLabel(fallado);
            return null;
        }

        public override object VisitWhileSTMAST(Parser1.WhileSTMASTContext context)
        {
            Label InicioWhile = pmactual.DefineLabel();
            Label startcondition = pmactual.DefineLabel();
            Label finwhile = pmactual.DefineLabel();
            breaks.Add(finwhile);
            pmactual.Emit(OpCodes.Br_S,startcondition);
            pmactual.MarkLabel(InicioWhile);
            Visit(context.statement());
            pmactual.MarkLabel(startcondition);
            Visit(context.condition());
            pmactual.Emit(OpCodes.Brtrue_S,InicioWhile);
            pmactual.MarkLabel(finwhile);
            return null;
        }

        public override object VisitBreakSTMAST(Parser1.BreakSTMASTContext context)
        {
            pmactual.Emit(OpCodes.Br,(Label)breaks[breaks.Count-1]);
            return null;
        }

        public override object VisitReturnSTMAST(Parser1.ReturnSTMASTContext context)
        {
            var v =Visit(context.expr());
            if (v.GetType()== typeof(Parser1.NumFACASTContext))
            {
                pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)v).NUMBER().GetText()));
            }else if (v is Parser1.DesignatorFACASTContext)
            {
                string nom = (string)Visit(((Parser1.DesignatorFACASTContext) v).designator());
                FieldBuilder is_global = buscarglobal(nom);
                if (is_global!=null)
                {
                    pmactual.Emit(OpCodes.Ldsfld,is_global);
                }
                else
                {
                    int param = buscarparam(nom);
                    if (param != -1)
                    {
                        if (localesParams.Count == 1)
                        {
                            pmactual.Emit(OpCodes.Ldarg_0);
                        }
                        else
                        {
                            pmactual.Emit(OpCodes.Ldarg_S,param);
                        }
                    }
                    else
                    {
                        LocalBuilder local = buscarlocal(nom);
                        pmactual.Emit(OpCodes.Ldloc,local);
                    }
                }
               
            }else if (v is Parser1.StringFACASTContext)
            {
                pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)v).STRING().GetText());
            }else if (v is Parser1.FloatFACASTContext)
            {
                pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)v).FLOATNUMBER().GetText()));
            }else if (v is Parser1.CharFACASTContext)
            {
                string g = (((Parser1.CharFACASTContext) v).CHARCONST().GetText());
                char[] c = g.ToCharArray();
                char ch = c[1];
                pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
            }else if (v is Parser1.BooleanFACASTContext)
            {
                if (((Parser1.BooleanFACASTContext)v).BOOLEAN().GetText()=="true")
                {
                    pmactual.Emit(OpCodes.Ldc_I4_1);
                }
                else
                {
                    pmactual.Emit(OpCodes.Ldc_I4_0);
                }

            }
            return null;
        }

        public override object VisitReadSTMAST(Parser1.ReadSTMASTContext context)
        {
            //HACER PARSE char,float!!
            string var = (string) Visit(context.designator());
            FieldBuilder fb = buscarglobal(var);
            MethodInfo readline=null;
            if (fb!=null)
            {
                readline = typeof(Console).GetMethod(
                    "ReadLine");
                pmactual.EmitCall(OpCodes.Call, readline,null);
                if (fb.FieldType ==typeof(int))
                {
                    MethodInfo parse = typeof(Int32).GetMethod(
                        "Parse",new Type[]{typeof(string)});
                    pmactual.EmitCall(OpCodes.Call,parse,null);
                }else if (fb.FieldType ==typeof(char))
                {
                    MethodInfo parse = typeof(char).GetMethod(
                        "Parse",new Type[]{typeof(string)});
                    pmactual.EmitCall(OpCodes.Call,parse,null);
                }else if (fb.FieldType ==typeof(float))
                {
                    MethodInfo parse = typeof(float).GetMethod(
                        "Parse",new Type[]{typeof(string)});
                    pmactual.EmitCall(OpCodes.Call,parse,null);
                }
                pmactual.Emit(OpCodes.Stsfld,fb);
            }
            else
            {
                int param = buscarparam(var);
                if (param!=-1)
                {
                    
                    if (metact.GetParameters().Length==1)
                    {
                        readline = typeof(Console).GetMethod(
                            "ReadLine");
                        pmactual.EmitCall(OpCodes.Call, readline,null);
                        if (metact.GetParameters()[0].ParameterType == typeof(int))
                        {
                            MethodInfo parse = typeof(Int32).GetMethod(
                                "Parse",new Type[]{typeof(string)});
                            pmactual.EmitCall(OpCodes.Call,parse,null);
                        }else if (fb.FieldType ==typeof(char))
                        {
                            MethodInfo parse = typeof(char).GetMethod(
                                "Parse",new Type[]{typeof(string)});
                            pmactual.EmitCall(OpCodes.Call,parse,null);
                        }else if (fb.FieldType ==typeof(float))
                        {
                            MethodInfo parse = typeof(float).GetMethod(
                                "Parse",new Type[]{typeof(string)});
                            pmactual.EmitCall(OpCodes.Call,parse,null);
                        }
                        pmactual.Emit(OpCodes.Starg_S);
                    }
                    else
                    {
                        readline = typeof(Console).GetMethod(
                            "ReadLine",new Type[]{typeof(string)});
                        pmactual.EmitCall(OpCodes.Call, readline,null);
                        if (metact.GetParameters()[param].ParameterType == typeof(int))
                        {
                            MethodInfo parse = typeof(Int32).GetMethod(
                                "Parse",new Type[]{typeof(string)});
                            pmactual.EmitCall(OpCodes.Call,parse,null);
                        }else if (fb.FieldType ==typeof(char))
                        {
                            MethodInfo parse = typeof(char).GetMethod(
                                "Parse",new Type[]{typeof(string)});
                            pmactual.EmitCall(OpCodes.Call,parse,null);
                        }else if (fb.FieldType ==typeof(float))
                        {
                            MethodInfo parse = typeof(float).GetMethod(
                                "Parse",new Type[]{typeof(string)});
                            pmactual.EmitCall(OpCodes.Call,parse,null);
                        }
                        pmactual.Emit(OpCodes.Starg_S,param);
                    }
                }
                else
                {
                    LocalBuilder lb = buscarlocal(var);
                    readline = typeof(Console).GetMethod(
                        "ReadLine");
                   
                    pmactual.EmitCall(OpCodes.Call, readline,null);
                    if (lb.LocalType == typeof(int))
                    {
                        MethodInfo parse = typeof(Int32).GetMethod(
                            "Parse",new Type[]{typeof(string)});
                        pmactual.EmitCall(OpCodes.Call,parse,null);
                    }else if (lb.LocalType ==typeof(char))
                    {
                        MethodInfo parse = typeof(char).GetMethod(
                            "Parse",new Type[]{typeof(string)});
                        pmactual.EmitCall(OpCodes.Call,parse,null);
                    }else if (lb.LocalType ==typeof(float))
                    {
                        MethodInfo parse = typeof(float).GetMethod(
                            "Parse",new Type[]{typeof(string)});
                        pmactual.EmitCall(OpCodes.Call,parse,null);
                    }
                    pmactual.Emit(OpCodes.Stloc,lb);
                }

            }

            return null;
        }

        public override object VisitWriteSTMAST(Parser1.WriteSTMASTContext context)
        {
            dynamic tipo = null;
            bool tiplist = false;
            var rtipo = Visit(context.expr());
            if (rtipo.GetType()==typeof(Parser1.CharFACASTContext))
            {
                tipo = ' ';
            }else if (rtipo.GetType()==typeof(Parser1.BooleanFACASTContext))
            {
                tipo = true;
            }else if (rtipo.GetType()==typeof(Parser1.StringFACASTContext))
            {
                tipo = "HOLA!";
            }else if (rtipo.GetType()==typeof(Parser1.FloatFACASTContext))
            {
                tipo = 1.2;
            }else if (rtipo.GetType()==typeof(Parser1.NumFACASTContext))
            {
                tipo = 1;
            }
            else
            {
                //IDENTS!
                desgcont = true;
                Parser1.DesignatorASTContext desg =
                    (Parser1.DesignatorASTContext) Visit(((Parser1.DesignatorFACASTContext) rtipo).designator());
                desgcont = false;
                if (desg.CORCHETEIZQ().Length != 0)
                {
                    tiplist = true;
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) rtipo).designator());
                    FieldBuilder is_globallist = buscarglobal(nom);
                    if (is_globallist != null)
                    {
                        tipo = tipowrit(is_globallist);
                        pmactual.Emit(OpCodes.Ldsfld, is_globallist);
                    }
                    else
                    {
                        int paramlist = buscarparam(nom);
                        if (paramlist != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                tipo = tipowrit(metact.GetParameters()[0]);
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                tipo = tipowrit(metact.GetParameters()[paramlist]);
                                pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                            tipo = tipowrit(local);
                        }
                    }

                    
                    var nameexp=Visit(desg.expr()[0]);
                    if (nameexp is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)nameexp).NUMBER().GetText()));
                    }
                    else
                    {
                        Parser1.DesignatorFACASTContext d = (Parser1.DesignatorFACASTContext) nameexp;
                        FieldBuilder is_expglob = buscarglobal((string)Visit(d.designator()));
                        if (is_globallist != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld, is_expglob);
                        }
                        else
                        {
                            int paramlist = buscarparam((string)Visit(d.designator()));
                            if (paramlist != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    tipo = tipowrit(metact.GetParameters()[paramlist]);
                                    pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string)Visit(d.designator()));
                                pmactual.Emit(OpCodes.Ldloc, local);
                            }
                        }
                    }
                    MethodInfo getItem = typeof(ArrayList).GetMethod(
                        "get_Item",
                        new Type[] {typeof(int)});
                    pmactual.EmitCall(OpCodes.Callvirt, getItem, null);
                }
                else if (desg.IDENT().Length == 2)
                {
                    FieldBuilder is_global = buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) rtipo).designator()));
                    if (is_global!=null)
                    {
                        
                        var nc = is_global.DeclaringType.Name;
                        pmactual.Emit(OpCodes.Ldsfld,is_global);
                        foreach (ArrayList cls in listf_porclase)
                        {
                            if (cls[0].ToString() == nc)
                            {
                                for (int i = 1; i < cls.Count; i++)
                                {
                                    FieldBuilder fls = (FieldBuilder) cls[i];
                                    if (fls.Name==desg.IDENT()[1].GetText())
                                    {
                                        pmactual.Emit(OpCodes.Ldfld,fls);
                                        tipo = tipowrit(fls);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                        int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext) rtipo).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {

                                    pmactual.Emit(OpCodes.Starg_S);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Starg_S, param);
                                }
                            }
                            else
                            {
                                LocalBuilder local =
                                    buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext) rtipo).designator()));

                                var nc = local.LocalType.Name;
                                pmactual.Emit(OpCodes.Ldloc,local);
                                foreach (ArrayList cls in listf_porclase)
                                {
                                    if (cls[0].ToString() == nc)
                                    {
                                        for (int i = 1; i < cls.Count; i++)
                                        {
                                            FieldBuilder fls = (FieldBuilder) cls[i];
                                            if (fls.Name == desg.IDENT()[1].GetText())
                                            {
                                                pmactual.Emit(OpCodes.Ldfld, fls);
                                                tipo = tipowrit(fls);
                                                break;
                                            }
                                        }

                                        break;
                                    }
                                }
                            }
                    }
                    
                }
                else
                {
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) rtipo).designator());
                    FieldBuilder is_global = buscarglobal(nom);
                    if (is_global != null)
                    {
                        tipo = tipowrit(is_global);
                        pmactual.Emit(OpCodes.Ldsfld, is_global);
                    }
                    else
                    {
                        int param = buscarparam(nom);
                        if (param != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                tipo = tipowrit(metact.GetParameters()[0]);
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                tipo = tipowrit(metact.GetParameters()[param]);
                                pmactual.Emit(OpCodes.Ldarg_S, param);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                            tipo = tipowrit(local);
                        }
                    }
                }
            }

            MethodInfo writeMI1 = null;
            if (tiplist)
            {
                writeMI1=typeof(Console).GetMethod(
                    "WriteLine",
                    new Type[] { typeof(Object) });
            }
            else
            {
                writeMI1= typeof(Console).GetMethod(
                    "WriteLine",
                    new Type[] { tipo.GetType() });
            }

             
            if (tipo is int)
            {
                if (rtipo is Parser1.DesignatorFACASTContext)
                {
                    
                }
                else
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)rtipo).NUMBER().GetText()));
                }

                
            }else if (tipo is double)
            {
                if (rtipo is Parser1.DesignatorFACASTContext)
                {
                    
                }
                else
                {
                    pmactual.Emit(OpCodes.Ldc_R8,((Parser1.FloatFACASTContext)rtipo).FLOATNUMBER().GetText());
                }
            }else if (tipo is string)
            {
                if (rtipo is Parser1.DesignatorFACASTContext)
                {
                    
                }
                else
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)rtipo).STRING().GetText());
                }
            }else if (tipo is char)
            {
                if (rtipo is Parser1.DesignatorFACASTContext)
                {
                    
                }
                else
                {
                    string g = ((Parser1.CharFACASTContext) rtipo).CHARCONST().GetText();
                    char[] c = g.ToCharArray();
                    char ch = c[1];
                    pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                }
            }else if (tipo is bool)
            {
                if (rtipo is Parser1.DesignatorFACASTContext)
                {
                    
                }
                else
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.BooleanFACASTContext)rtipo).BOOLEAN().GetText());
                }
            }
            pmactual.EmitCall(OpCodes.Call,writeMI1,null);
            return null;
        }

        public override object VisitBlockSTMAST(Parser1.BlockSTMASTContext context)
        {
            return Visit(context.block());
        }

        public override object VisitPycomaSTMAST(Parser1.PycomaSTMASTContext context)
        {
            return base.VisitPycomaSTMAST(context);
        }

        public override object VisitSwitchcaseSTAST(Parser1.SwitchcaseSTASTContext context)
        {
            Visit(context.switchcase());
            return null;
        }

        public override object VisitBlockAST(Parser1.BlockASTContext context)
        {
            foreach (var vardeclContext in context.vardecl())
            {
                Parser1.VardeclASTContext v = (Parser1.VardeclASTContext)Visit(vardeclContext);
                Addvars(v,metact);
            }
            foreach (var statementContext in context.statement())
            {
                Visit(statementContext);
            }
            return null;
        }

        public override object VisitActparsAST(Parser1.ActparsASTContext context)
        {
            foreach (var exprContext in context.expr())
            {
                var e = Visit(exprContext);
                if (e is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)e).NUMBER().GetText()));
                }else if (e is Parser1.CharFACASTContext)
                {
                    string g = ((Parser1.CharFACASTContext) e).CHARCONST().GetText();
                    char[] c = g.ToCharArray();
                    char ch = c[1];
                    pmactual.Emit(OpCodes.Ldc_I4,(int)ch);
                }else if (e is Parser1.BooleanFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,(((Parser1.BooleanFACASTContext)e).BOOLEAN().GetText()));
                }else if (e is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,(((Parser1.StringFACASTContext)e).STRING().GetText()));
                }else if (e is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)e).FLOATNUMBER().GetText()));
                }else if (e is Parser1.NewFACASTContext)
                {
                    typelist = true;
                    Parser1.NewFACASTContext nw = (Parser1.NewFACASTContext) e;
                    if (nw.type() != null)
                    {
                        var visittype = Visit(nw.type());
                        if (visittype is Parser1.ChartypeASTContext)
                        {
                            if (((Parser1.ChartypeASTContext)visittype).NUMBER()!=null)
                            {
                                ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                pmactual.Emit(OpCodes.Ldc_I4_S,int.Parse(((Parser1.ChartypeASTContext)visittype).NUMBER().GetText()));
                                pmactual.Emit(OpCodes.Newobj,L[1]);
                            }
                            else
                            {
                                ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                pmactual.Emit(OpCodes.Newobj,L[0]);
                            }
                        }
                        else if (visittype is Parser1.InttypeASTContext)
                        {
                            if (((Parser1.InttypeASTContext)visittype).NUMBER()!=null)
                            {
                                ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                pmactual.Emit(OpCodes.Ldc_I4_S,int.Parse(((Parser1.InttypeASTContext)visittype).NUMBER().GetText()));
                                pmactual.Emit(OpCodes.Newobj,L[1]);
                            }
                            else
                            {
                                ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                pmactual.Emit(OpCodes.Newobj,L[0]);
                            }

                        }
                        else
                        {
                            if (((Parser1.IdenttypeASTContext)visittype).NUMBER()!=null)
                            {
                                ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                pmactual.Emit(OpCodes.Ldc_I4_S,int.Parse(((Parser1.IdenttypeASTContext)visittype).NUMBER().GetText()));
                                pmactual.Emit(OpCodes.Newobj,L[1]);
                            }
                            else
                            {
                                ConstructorInfo[] L = new ArrayList().GetType().GetConstructors();
                                pmactual.Emit(OpCodes.Newobj,L[0]);
                            }
                        }
                    }
                    else
                    {
                        string nombreclase = (string) Visit(((Parser1.NewFACASTContext)e).type());
                        ConstructorBuilder cB = buscarC(nombreclase);
                        pmactual.Emit(OpCodes.Newobj,cB);
                    }

                    typelist = false;

                }else if (e is Parser1.DesignatorFACASTContext)
                {
                    string name = (string)Visit(((Parser1.DesignatorFACASTContext)e).designator());
                    FieldBuilder is_global = buscarglobal(name);
                    if (is_global != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld, is_global);
                    }
                    else
                    {
                        int param = buscarparam(name);
                        if (param != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S, param);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(name);
                            pmactual.Emit(OpCodes.Ldloc, local);
                        }
                    }
                }

            }
            return context;
        }

        public override object VisitConditiontermAST(Parser1.ConditiontermASTContext context)
        {
            Parser1.RelopContext rp =null;
            if (context.condterm().Length>1)
            {

                for (int i = 0; i < context.condterm().Length; i++)
                {
                    rp = (Parser1.RelopContext) Visit(context.condterm()[i]);
                    if (rp.IGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Ceq);
                    }
                    else if (rp.DIF() != null)
                    {
                        pmactual.Emit(OpCodes.Ceq);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }
                    else if (rp.MENORIGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Clt);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }
                    else if (rp.MAYORIGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Cgt);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }
                    else if (rp.MENOR() != null)
                    {
                        pmactual.Emit(OpCodes.Cgt);
                    }
                    else
                    {
                        pmactual.Emit(OpCodes.Clt);

                    }

                    if ((i + 1) % 2 == 0)
                    {
                        pmactual.Emit(OpCodes.Or);
                    }

                    /*if (rp.IGUAL()!=null&&context.condterm().Length -1 == i)
                    {
                        pmactual.Emit(OpCodes.Ceq,ifor);
                    }else if (rp.IGUAL()!=null)
                    {
                        pmactual.Emit(OpCodes.Beq);
                    }else if (rp.DIF()!=null&&context.condterm().Length -1 == i)
                    {
                        pmactual.Emit(OpCodes.Bne_Un,ifor);
                    }else if (rp.DIF() != null)
                    {
                        pmactual.Emit(OpCodes.Ceq);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }else if (rp.MENORIGUAL()!=null&&context.condterm().Length -1 == i)
                    {
                        pmactual.Emit(OpCodes.Bge,ifor);
                    }else if (rp.MENORIGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Clt);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }else if (rp.MAYORIGUAL()!=null&&context.condterm().Length -1 == i)
                    {
                        pmactual.Emit(OpCodes.Ble,ifor);
                    }else if (rp.MAYORIGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Cgt);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }else if (rp.MAYOR()!=null)
                    {
                        pmactual.Emit(OpCodes.Blt,ifor);
                    }else if (rp.MAYOR()!=null&&context.condterm().Length -1 == i)
                    {
                        pmactual.Emit(OpCodes.Ble);
                    }
                    else
                    {
                        if (context.condterm().Length-1 == i)
                        {
                            pmactual.Emit(OpCodes.Clt);
                        }
                        else
                        {
                            pmactual.Emit(OpCodes.Bgt,ifor);
                        }
                    }*/
                }
            }
            else
            {
                rp = (Parser1.RelopContext)Visit(context.condterm()[0]);
                if (rp.IGUAL()!=null)
                {
                    pmactual.Emit(OpCodes.Ceq);
                }else if (rp.DIF()!=null)
                {
                    pmactual.Emit(OpCodes.Ceq);
                    pmactual.Emit(OpCodes.Ldc_I4_0);
                    pmactual.Emit(OpCodes.Ceq);
                }else if (rp.MENORIGUAL()!=null)
                {
                    pmactual.Emit(OpCodes.Clt);
                    pmactual.Emit(OpCodes.Ldc_I4_0);
                    pmactual.Emit(OpCodes.Ceq);
                }else if (rp.MAYORIGUAL()!=null)
                {
                    pmactual.Emit(OpCodes.Cgt);
                    pmactual.Emit(OpCodes.Ldc_I4_0);
                    pmactual.Emit(OpCodes.Ceq);
                }else if (rp.MENOR()!=null)
                {
                    pmactual.Emit(OpCodes.Cgt);
                }
                else
                {
                    pmactual.Emit(OpCodes.Clt);
                    
                }
            }
            return rp;
        }

        public override object VisitCondtermAST(Parser1.CondtermASTContext context)
        {
            Parser1.RelopContext rp =null;
            if (context.condfact().Length>1)
            {
                for (int i = 0; i < context.condfact().Length; i++)
                {
                    rp = (Parser1.RelopContext)Visit(context.condfact()[i]);
                    if (rp.IGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Ceq);
                    }
                    else if (rp.DIF() != null)
                    {
                        pmactual.Emit(OpCodes.Ceq);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }
                    else if (rp.MENORIGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Clt);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }
                    else if (rp.MAYORIGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Cgt);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }
                    else if (rp.MENOR() != null)
                    {
                        pmactual.Emit(OpCodes.Cgt);
                    }
                    else
                    {
                        pmactual.Emit(OpCodes.Clt);

                    }

                    if ((i + 1) % 2 == 0)
                    {
                        pmactual.Emit(OpCodes.And);
                    }

                    /*if (rp.IGUAL()!=null&&context.condfact().Length-1 == i)
                    {
                        pmactual.Emit(OpCodes.Ceq);
                    }else if (rp.IGUAL()!=null)
                    {
                        pmactual.Emit(OpCodes.Beq);
                    }else if (rp.DIF()!=null&&context.condfact().Length-1 == i)
                    {
                        pmactual.Emit(OpCodes.Bne_Un);
                    }else if (rp.DIF() != null)
                    {
                        pmactual.Emit(OpCodes.Ceq);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }else if (rp.MENORIGUAL()!=null&&context.condfact().Length-1 == i)
                    {
                        pmactual.Emit(OpCodes.Bge);
                    }else if (rp.MENORIGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Clt);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }else if (rp.MAYORIGUAL()!=null&&context.condfact().Length-1 == i)
                    {
                        pmactual.Emit(OpCodes.Ble);
                    }else if (rp.MAYORIGUAL() != null)
                    {
                        pmactual.Emit(OpCodes.Cgt);
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                        pmactual.Emit(OpCodes.Ceq);
                    }else if (rp.MAYOR()!=null)
                    {
                        pmactual.Emit(OpCodes.Blt);
                    }else if (rp.MAYOR()!=null&&context.condfact().Length-1 == i)
                    {
                        pmactual.Emit(OpCodes.Ble);
                    }
                    else
                    {
                        if (context.condfact().Length-1 == i)
                        {
                            pmactual.Emit(OpCodes.Clt);
                        }
                        else
                        {
                            pmactual.Emit(OpCodes.Bgt);
                        }
                    }*/
                }
            }
            else
            {
                rp = (Parser1.RelopContext)Visit(context.condfact()[0]);
            }

            return rp;
    }

        public override object VisitCondfactAST(Parser1.CondfactASTContext context)
        {
            var exp1 = Visit(context.expr()[0]);
            var exp2 = Visit(context.expr()[1]);
            if (exp1 is Parser1.DesignatorFACASTContext)
            {
                desgcont = true;
                Parser1.DesignatorASTContext desg = (Parser1.DesignatorASTContext)Visit(((Parser1.DesignatorFACASTContext) exp1).designator());
                desgcont = false;
                if (desg.CORCHETEIZQ().Length != 0)
                {
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) exp1).designator());
                    FieldBuilder is_globallist = buscarglobal(nom);
                    if (is_globallist != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld, is_globallist);
                    }
                    else
                    {
                        int paramlist = buscarparam(nom);
                        if (paramlist != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                        }
                    }

                    
                    var nameexp=Visit(desg.expr()[0]);
                    if (nameexp is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)nameexp).NUMBER().GetText()));
                    }
                    else
                    {
                        Parser1.DesignatorFACASTContext d = (Parser1.DesignatorFACASTContext) nameexp;
                        FieldBuilder is_expglob = buscarglobal((string)Visit(d.designator()));
                        if (is_globallist != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld, is_expglob);
                        }
                        else
                        {
                            int paramlist = buscarparam((string)Visit(d.designator()));
                            if (paramlist != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string)Visit(d.designator()));
                                pmactual.Emit(OpCodes.Ldloc, local);
                            }
                        }
                    }
                    MethodInfo getItem = typeof(ArrayList).GetMethod(
                        "get_Item",
                        new Type[] {typeof(int)});
                    pmactual.EmitCall(OpCodes.Callvirt, getItem, null);
                }
                else
                {
                    FieldBuilder is_global =
                        buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) exp1).designator()));
                    if (is_global != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld,is_global);
                    }
                    else
                    {
                        int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)exp1).designator()));
                        if (param != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S,param);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)exp1).designator()));
                            pmactual.Emit(OpCodes.Ldloc,local);
                        }
                    }
                    
                }

                if (exp2 is Parser1.DesignatorFACASTContext)
                { 
                    desgcont = true;
                    Parser1.DesignatorASTContext desg2 = (Parser1.DesignatorASTContext)Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    desgcont = false;
                    if (desg2.CORCHETEIZQ().Length != 0)
                {
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    FieldBuilder is_globallist = buscarglobal(nom);
                    if (is_globallist != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld, is_globallist);
                    }
                    else
                    {
                        int paramlist = buscarparam(nom);
                        if (paramlist != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                        }
                    }

                    
                    var nameexp=Visit(desg2.expr()[0]);
                    if (nameexp is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)nameexp).NUMBER().GetText()));
                    }
                    else
                    {
                        Parser1.DesignatorFACASTContext d = (Parser1.DesignatorFACASTContext) nameexp;
                        FieldBuilder is_expglob = buscarglobal((string)Visit(d.designator()));
                        if (is_globallist != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld, is_expglob);
                        }
                        else
                        {
                            int paramlist = buscarparam((string)Visit(d.designator()));
                            if (paramlist != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string)Visit(d.designator()));
                                pmactual.Emit(OpCodes.Ldloc, local);
                            }
                        }
                    }
                    MethodInfo getItem = typeof(ArrayList).GetMethod(
                        "get_Item",
                        new Type[] {typeof(int)});
                    pmactual.EmitCall(OpCodes.Callvirt, getItem, null);
                    }
                    else
                    {
                        FieldBuilder is2_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator()));
                        if (is2_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,is2_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }

                    
                    
                }else if (exp2 is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp2).NUMBER().GetText()));
                    
                }else if (exp2 is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp2).STRING().GetText());
                }else if (exp2 is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp2).FLOATNUMBER().GetText()));
                }else if (exp2 is Parser1.BooleanFACASTContext)
                {
                    string valor = ((Parser1.BooleanFACASTContext) exp2).BOOLEAN().GetText();
                    if (valor.Equals("true"))
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_1);
                    }
                    else
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                    }

                }if (exp2 is Parser1.CharFACASTContext)
                {
                    string g = ((Parser1.CharFACASTContext)exp2).CHARCONST().GetText();
                    char[] cl = g.ToCharArray();
                    char ch = cl[1];
                    pmactual.Emit(OpCodes.Ldc_I4,(int)ch);
                }
            }else if (exp1 is Parser1.NumFACASTContext)
            {
                pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp1).NUMBER().GetText()));
                if (exp2 is Parser1.DesignatorFACASTContext)
                {
                    desgcont = true;
                    Parser1.DesignatorASTContext desg2 = (Parser1.DesignatorASTContext)Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    desgcont = false;
                    if (desg2.CORCHETEIZQ().Length != 0)
                {
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    FieldBuilder is_globallist = buscarglobal(nom);
                    if (is_globallist != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld, is_globallist);
                    }
                    else
                    {
                        int paramlist = buscarparam(nom);
                        if (paramlist != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                        }
                    }

                    
                    var nameexp=Visit(desg2.expr()[0]);
                    if (nameexp is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)nameexp).NUMBER().GetText()));
                    }
                    else
                    {
                        Parser1.DesignatorFACASTContext d = (Parser1.DesignatorFACASTContext) nameexp;
                        FieldBuilder is_expglob = buscarglobal((string)Visit(d.designator()));
                        if (is_globallist != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld, is_expglob);
                        }
                        else
                        {
                            int paramlist = buscarparam((string)Visit(d.designator()));
                            if (paramlist != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string)Visit(d.designator()));
                                pmactual.Emit(OpCodes.Ldloc, local);
                            }
                        }
                    }
                    MethodInfo getItem = typeof(ArrayList).GetMethod(
                        "get_Item",
                        new Type[] {typeof(int)});
                    pmactual.EmitCall(OpCodes.Callvirt, getItem, null);
                    }
                    else
                    {
                        
                        FieldBuilder is2_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator()));
                        if (is2_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,is2_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }

                    
                }else if (exp2 is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp2).NUMBER().GetText()));
                }else if (exp2 is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp2).STRING().GetText());
                }else if (exp2 is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp2).FLOATNUMBER().GetText()));

                }else if (exp2 is Parser1.BooleanFACASTContext)
                {
                    string valor = ((Parser1.BooleanFACASTContext) exp2).BOOLEAN().GetText();
                    if (valor.Equals("true"))
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_1);
                    }
                    else
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                    }

                }if (exp2 is Parser1.CharFACASTContext)
                {
                    string g2 = ((Parser1.CharFACASTContext)exp2).CHARCONST().GetText();
                    char[] cl2 = g2.ToCharArray();
                    char ch2 = cl2[1];
                    pmactual.Emit(OpCodes.Ldc_I4,(int)ch2);
                }
            }else if (exp1 is Parser1.StringFACASTContext)
            {
                pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp1).STRING().GetText());
                if (exp2 is Parser1.DesignatorFACASTContext)
                {
                    desgcont = true;
                    Parser1.DesignatorASTContext desg2 = (Parser1.DesignatorASTContext)Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    desgcont = false;
                    if (desg2.CORCHETEIZQ().Length != 0)
                    {
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    FieldBuilder is_globallist = buscarglobal(nom);
                    if (is_globallist != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld, is_globallist);
                    }
                    else
                    {
                        int paramlist = buscarparam(nom);
                        if (paramlist != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                        }
                    }

                    
                    var nameexp=Visit(desg2.expr()[0]);
                    if (nameexp is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)nameexp).NUMBER().GetText()));
                    }
                    else
                    {
                        Parser1.DesignatorFACASTContext d = (Parser1.DesignatorFACASTContext) nameexp;
                        FieldBuilder is_expglob = buscarglobal((string)Visit(d.designator()));
                        if (is_globallist != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld, is_expglob);
                        }
                        else
                        {
                            int paramlist = buscarparam((string)Visit(d.designator()));
                            if (paramlist != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string)Visit(d.designator()));
                                pmactual.Emit(OpCodes.Ldloc, local);
                            }
                        }
                    }
                    MethodInfo getItem = typeof(ArrayList).GetMethod(
                        "get_Item",
                        new Type[] {typeof(int)});
                    pmactual.EmitCall(OpCodes.Callvirt, getItem, null);
                    }
                    else
                    {
                        FieldBuilder is2_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator()));
                        if (is2_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,is2_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }
                    
                }else if (exp2 is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp2).NUMBER().GetText()));
                }else if (exp2 is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp2).STRING().GetText());
                }else if (exp2 is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp2).FLOATNUMBER().GetText()));
                }else if (exp2 is Parser1.BooleanFACASTContext)
                {
                    string valor = ((Parser1.BooleanFACASTContext) exp2).BOOLEAN().GetText();
                    if (valor.Equals("true"))
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_1);
                    }
                    else
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                    }

                }if (exp2 is Parser1.CharFACASTContext)
                {
                    string g = ((Parser1.CharFACASTContext)exp2).CHARCONST().GetText();
                    char[] cl = g.ToCharArray();
                    char ch = cl[1];
                    pmactual.Emit(OpCodes.Ldc_I4,(int)ch);
                }
            }else if (exp1 is Parser1.FloatFACASTContext)
            {
                pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp1).FLOATNUMBER().GetText()));
                if (exp2 is Parser1.DesignatorFACASTContext)
                {
                    desgcont = true;
                    Parser1.DesignatorASTContext desg2 = (Parser1.DesignatorASTContext)Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    desgcont = false;
                    if (desg2.CORCHETEIZQ().Length != 0)
                {
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    FieldBuilder is_globallist = buscarglobal(nom);
                    if (is_globallist != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld, is_globallist);
                    }
                    else
                    {
                        int paramlist = buscarparam(nom);
                        if (paramlist != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                        }
                    }

                    
                    var nameexp=Visit(desg2.expr()[0]);
                    if (nameexp is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)nameexp).NUMBER().GetText()));
                    }
                    else
                    {
                        Parser1.DesignatorFACASTContext d = (Parser1.DesignatorFACASTContext) nameexp;
                        FieldBuilder is_expglob = buscarglobal((string)Visit(d.designator()));
                        if (is_globallist != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld, is_expglob);
                        }
                        else
                        {
                            int paramlist = buscarparam((string)Visit(d.designator()));
                            if (paramlist != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string)Visit(d.designator()));
                                pmactual.Emit(OpCodes.Ldloc, local);
                            }
                        }
                    }
                    MethodInfo getItem = typeof(ArrayList).GetMethod(
                        "get_Item",
                        new Type[] {typeof(int)});
                    pmactual.EmitCall(OpCodes.Callvirt, getItem, null);
                    }
                    else
                    {
                        FieldBuilder is2_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator()));
                        if (is2_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,is2_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }

                    
                }else if (exp2 is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp2).NUMBER().GetText()));
                }else if (exp2 is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp2).STRING().GetText());
                }else if (exp2 is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp2).FLOATNUMBER().GetText()));
                }else if (exp2 is Parser1.BooleanFACASTContext)
                {
                    string valor = ((Parser1.BooleanFACASTContext) exp2).BOOLEAN().GetText();
                    if (valor.Equals("true"))
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_1);
                    }
                    else
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                    }

                }if (exp2 is Parser1.CharFACASTContext)
                {
                    string g = ((Parser1.CharFACASTContext)exp2).CHARCONST().GetText();
                    char[] cl = g.ToCharArray();
                    char ch = cl[1];
                    pmactual.Emit(OpCodes.Ldc_I4,(int)ch);
                }
            }else if (exp1 is Parser1.BooleanFACASTContext)
            { 
                string valor = ((Parser1.BooleanFACASTContext) exp1).BOOLEAN().GetText();
                if (valor.Equals("true"))
                {
                    pmactual.Emit(OpCodes.Ldc_I4_1);
                }
                else
                {
                    pmactual.Emit(OpCodes.Ldc_I4_0);
                }

                if (exp2 is Parser1.DesignatorFACASTContext)
                {
                    desgcont = true;
                    Parser1.DesignatorASTContext desg2 = (Parser1.DesignatorASTContext)Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    desgcont = false;
                    if (desg2.CORCHETEIZQ().Length != 0)
                {
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    FieldBuilder is_globallist = buscarglobal(nom);
                    if (is_globallist != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld, is_globallist);
                    }
                    else
                    {
                        int paramlist = buscarparam(nom);
                        if (paramlist != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                        }
                    }

                    
                    var nameexp=Visit(desg2.expr()[0]);
                    if (nameexp is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)nameexp).NUMBER().GetText()));
                    }
                    else
                    {
                        Parser1.DesignatorFACASTContext d = (Parser1.DesignatorFACASTContext) nameexp;
                        FieldBuilder is_expglob = buscarglobal((string)Visit(d.designator()));
                        if (is_globallist != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld, is_expglob);
                        }
                        else
                        {
                            int paramlist = buscarparam((string)Visit(d.designator()));
                            if (paramlist != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string)Visit(d.designator()));
                                pmactual.Emit(OpCodes.Ldloc, local);
                            }
                        }
                    }
                    MethodInfo getItem = typeof(ArrayList).GetMethod(
                        "get_Item",
                        new Type[] {typeof(int)});
                    pmactual.EmitCall(OpCodes.Callvirt, getItem, null);
                    }
                    else
                    {
                        FieldBuilder is2_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator()));
                        if (is2_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,is2_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }

                    
                }else if (exp2 is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp2).NUMBER().GetText()));
                }else if (exp2 is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp2).STRING().GetText());
                }else if (exp2 is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp2).FLOATNUMBER().GetText()));
                }else if (exp2 is Parser1.BooleanFACASTContext)
                {
                    string valor2 = ((Parser1.BooleanFACASTContext) exp2).BOOLEAN().GetText();
                    if (valor2.Equals("true"))
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_1);
                    }
                    else
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                    }

                }if (exp2 is Parser1.CharFACASTContext)
                {
                    string g = ((Parser1.CharFACASTContext)exp2).CHARCONST().GetText();
                    char[] cl = g.ToCharArray();
                    char ch = cl[1];
                    pmactual.Emit(OpCodes.Ldc_I4,(int)ch);
                }
            }else if (exp1 is Parser1.CharFACASTContext)
            {
                string g = ((Parser1.CharFACASTContext)exp1).CHARCONST().GetText();
                char[] cl = g.ToCharArray();
                char ch = cl[1];
                pmactual.Emit(OpCodes.Ldc_I4,(int)ch);
                if (exp2 is Parser1.DesignatorFACASTContext)
                {
                    desgcont = true;
                    Parser1.DesignatorASTContext desg2 = (Parser1.DesignatorASTContext)Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    desgcont = false;
                    if (desg2.CORCHETEIZQ().Length != 0)
                {
                    string nom = (string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator());
                    FieldBuilder is_globallist = buscarglobal(nom);
                    if (is_globallist != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld, is_globallist);
                    }
                    else
                    {
                        int paramlist = buscarparam(nom);
                        if (paramlist != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal(nom);
                            pmactual.Emit(OpCodes.Ldloc, local);
                        }
                    }

                    
                    var nameexp=Visit(desg2.expr()[0]);
                    if (nameexp is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)nameexp).NUMBER().GetText()));
                    }
                    else
                    {
                        Parser1.DesignatorFACASTContext d = (Parser1.DesignatorFACASTContext) nameexp;
                        FieldBuilder is_expglob = buscarglobal((string)Visit(d.designator()));
                        if (is_globallist != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld, is_expglob);
                        }
                        else
                        {
                            int paramlist = buscarparam((string)Visit(d.designator()));
                            if (paramlist != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S, paramlist);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string)Visit(d.designator()));
                                pmactual.Emit(OpCodes.Ldloc, local);
                            }
                        }
                    }
                    MethodInfo getItem = typeof(ArrayList).GetMethod(
                        "get_Item",
                        new Type[] {typeof(int)});
                    pmactual.EmitCall(OpCodes.Callvirt, getItem, null);
                    }
                    else
                    {
                        FieldBuilder is2_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) exp2).designator()));
                        if (is2_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,is2_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)exp2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }

                    
                    
                }else if (exp2 is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)exp2).NUMBER().GetText()));
                }else if (exp2 is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)exp2).STRING().GetText());
                }else if (exp2 is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)exp2).FLOATNUMBER().GetText()));
                }else if (exp2 is Parser1.BooleanFACASTContext)
                {
                    string valor = ((Parser1.BooleanFACASTContext) exp2).BOOLEAN().GetText();
                    if (valor.Equals("true"))
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_1);
                    }
                    else
                    {
                        pmactual.Emit(OpCodes.Ldc_I4_0);
                    }

                }if (exp2 is Parser1.CharFACASTContext)
                {
                    string g2 = ((Parser1.CharFACASTContext)exp2).CHARCONST().GetText();
                    char[] cl2 = g2.ToCharArray();
                    char ch2 = cl2[1];
                    pmactual.Emit(OpCodes.Ldc_I4,(int)ch2);
                }
            }
            return Visit(context.relop());
        }

        public override object VisitExprAST(Parser1.ExprASTContext context)
        {
            if (context.addop().Length>=1)
            {
                var t1 = Visit(context.term()[0]);
                var t2 = Visit(context.term()[1]);
                if (t1 is Parser1.DesignatorFACASTContext)
                {
                    FieldBuilder is_global =
                        buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t1).designator()));
                    if (is_global != null)
                    {
                        pmactual.Emit(OpCodes.Ldsfld,is_global);
                    }
                    else
                    {
                        int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t1).designator()));
                        if (param != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S,param);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t1).designator()));
                            pmactual.Emit(OpCodes.Ldloc,local);
                        }
                    }
                    if (t2 is Parser1.DesignatorFACASTContext)
                    {
                        FieldBuilder ist2_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t2).designator()));
                        if (ist2_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,ist2_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }   
                    }else if (t2 is Parser1.StringFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t2).STRING().GetText());
                    }else if (t2 is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t2).NUMBER().GetText()));
                    }else if (t2 is Parser1.FloatFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t2).FLOATNUMBER().GetText()));
                    }
                }else if (t1 is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t1).NUMBER().GetText()));
                    if (t2 is Parser1.DesignatorFACASTContext)
                    {
                        FieldBuilder is_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t2).designator()));
                        if (is_global != null)
                        {
                            pmactual.Emit(OpCodes.Stsfld,is_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }else if (t2 is Parser1.StringFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t2).STRING().GetText());
                    }else if (t2 is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t2).NUMBER().GetText()));
                    }else if (t2 is Parser1.FloatFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t2).FLOATNUMBER().GetText()));
                    }
                }else if (t1 is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t1).STRING().GetText());
                    if (t2 is Parser1.DesignatorFACASTContext)
                    {
                        FieldBuilder is_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t2).designator()));
                        if (is_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,is_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }else if (t2 is Parser1.StringFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t2).STRING().GetText());
                    }else if (t2 is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t2).NUMBER().GetText()));
                    }else if (t2 is Parser1.FloatFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t2).FLOATNUMBER().GetText()));
                    }
                }else if (t1 is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t1).FLOATNUMBER().GetText()));
                    if (t2 is Parser1.DesignatorFACASTContext)
                    {
                        FieldBuilder is_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t2).designator()));
                        if (is_global != null)
                        {
                            pmactual.Emit(OpCodes.Ldsfld,is_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }else if (t2 is Parser1.StringFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t2).STRING().GetText());
                    }else if (t2 is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t2).NUMBER().GetText()));
                    }else if (t2 is Parser1.FloatFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t2).FLOATNUMBER().GetText()));
                    }
                }

                if (((Parser1.AddopContext)Visit(context.addop()[0])).RES()!=null)
                {
                    pmactual.Emit(OpCodes.Sub);
                }
                else
                {
                   pmactual.Emit(OpCodes.Add);
                }

                return null;
            }
            return Visit(context.term()[0]);
        }

        public override object VisitTermAST(Parser1.TermASTContext context)
        {
            if (context.mulop().Length>=1)
            {
                var t1 = Visit(context.factor()[0]);
                var t2 = Visit(context.factor()[1]);
                if (t1 is Parser1.DesignatorFACASTContext)
                {
                    FieldBuilder is_global =
                        buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t1).designator()));
                    if (is_global != null)
                    {
                        pmactual.Emit(OpCodes.Stsfld,is_global);
                    }
                    else
                    {
                        int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t1).designator()));
                        if (param != -1)
                        {
                            if (localesParams.Count == 1)
                            {
                                pmactual.Emit(OpCodes.Ldarg_0);
                            }
                            else
                            {
                                pmactual.Emit(OpCodes.Ldarg_S,param);
                            }
                        }
                        else
                        {
                            LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t1).designator()));
                            pmactual.Emit(OpCodes.Ldloc,local);
                        }
                    }
                    if (t2 is Parser1.DesignatorFACASTContext)
                    {
                        FieldBuilder ist2_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t2).designator()));
                        if (ist2_global != null)
                        {
                            pmactual.Emit(OpCodes.Stsfld,ist2_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }   
                    }else if (t2 is Parser1.StringFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t2).STRING().GetText());
                    }else if (t2 is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t2).NUMBER().GetText()));
                    }else if (t2 is Parser1.FloatFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t2).FLOATNUMBER().GetText()));
                    }
                }else if (t1 is Parser1.NumFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t1).NUMBER().GetText()));
                    if (t2 is Parser1.DesignatorFACASTContext)
                    {
                        FieldBuilder is_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t2).designator()));
                        if (is_global != null)
                        {
                            pmactual.Emit(OpCodes.Stsfld,is_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }else if (t2 is Parser1.StringFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t2).STRING().GetText());
                    }else if (t2 is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t2).NUMBER().GetText()));
                    }else if (t2 is Parser1.FloatFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t2).FLOATNUMBER().GetText()));
                    }
                }else if (t1 is Parser1.StringFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t1).STRING().GetText());
                    if (t2 is Parser1.DesignatorFACASTContext)
                    {
                        FieldBuilder is_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t2).designator()));
                        if (is_global != null)
                        {
                            pmactual.Emit(OpCodes.Stsfld,is_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }else if (t2 is Parser1.StringFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t2).STRING().GetText());
                    }else if (t2 is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t2).NUMBER().GetText()));
                    }else if (t2 is Parser1.FloatFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t2).FLOATNUMBER().GetText()));
                    }
                }else if (t1 is Parser1.FloatFACASTContext)
                {
                    pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t1).FLOATNUMBER().GetText()));
                    if (t2 is Parser1.DesignatorFACASTContext)
                    {
                        FieldBuilder is_global =
                            buscarglobal((string) Visit(((Parser1.DesignatorFACASTContext) t2).designator()));
                        if (is_global != null)
                        {
                            pmactual.Emit(OpCodes.Stsfld,is_global);
                        }
                        else
                        {
                            int param = buscarparam((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            if (param != -1)
                            {
                                if (localesParams.Count == 1)
                                {
                                    pmactual.Emit(OpCodes.Ldarg_0);
                                }
                                else
                                {
                                    pmactual.Emit(OpCodes.Ldarg_S,param);
                                }
                            }
                            else
                            {
                                LocalBuilder local = buscarlocal((string) Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                                pmactual.Emit(OpCodes.Ldloc,local);
                            }
                        }
                    }else if (t2 is Parser1.StringFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldstr,((Parser1.StringFACASTContext)t2).STRING().GetText());
                    }else if (t2 is Parser1.NumFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_I4,int.Parse(((Parser1.NumFACASTContext)t2).NUMBER().GetText()));
                    }else if (t2 is Parser1.FloatFACASTContext)
                    {
                        pmactual.Emit(OpCodes.Ldc_R8,float.Parse(((Parser1.FloatFACASTContext)t2).FLOATNUMBER().GetText()));
                    }
                }

                if (((Parser1.MulopContext)Visit(context.mulop()[0])).DIV()!=null)
                {
                    pmactual.Emit(OpCodes.Rem);
                }else if (((Parser1.MulopContext)Visit(context.mulop()[0])).DIVMOD()!=null)
                {
                    pmactual.Emit(OpCodes.Rem_Un);
                }else
                {
                   pmactual.Emit(OpCodes.Mul);
                }

                return null;
            }
            return Visit(context.factor()[0]);
        }

        public override object VisitDesignatorFACAST(Parser1.DesignatorFACASTContext context)
        {
            return context;
        }

        public override object VisitNumFACAST(Parser1.NumFACASTContext context)
        {
            return context;
        }

        public override object VisitCharFACAST(Parser1.CharFACASTContext context)
        {
            return context;
        }

        public override object VisitBooleanFACAST(Parser1.BooleanFACASTContext context)
        {
            return context;
        }

        public override object VisitNewFACAST(Parser1.NewFACASTContext context)
        {
            return context;
        }

        public override object VisitStringFACAST(Parser1.StringFACASTContext context)
        {
            return context;
        }

        public override object VisitFloatFACAST(Parser1.FloatFACASTContext context)
        {
            return context;
        }

        public override object VisitExprFACAST(Parser1.ExprFACASTContext context)
        {
            return context;
        }

        public override object VisitDesignatorAST(Parser1.DesignatorASTContext context)
        {
            if (desgcont)
                return context;
            return context.IDENT()[0].GetText();
        }

        public override object VisitRelop(Parser1.RelopContext context)
        {
            return context;
        }

        public override object VisitAddop(Parser1.AddopContext context)
        {
            return context;
        }

        public override object VisitMulop(Parser1.MulopContext context)
        {
            return context;
        }

        public override object VisitSwitchAST(Parser1.SwitchASTContext context)
        {
            Label final = pmactual.DefineLabel();
            int indexvalor = 0;
            foreach (var caso in context.CASE())
            {
                Label else_IF=pmactual.DefineLabel();
                FieldBuilder is_global =
                    buscarglobal(context.IDENT().GetText());
                if (is_global != null)
                {
                    pmactual.Emit(OpCodes.Ldsfld,is_global);
                }
                else
                {
                    int param = buscarparam(context.IDENT().GetText());
                    if (param != -1)
                    {
                        if (localesParams.Count == 1)
                        {
                            pmactual.Emit(OpCodes.Ldarg_0);
                        }
                        else
                        {
                            pmactual.Emit(OpCodes.Ldarg_S,param);
                        }
                    }
                    else
                    {
                        LocalBuilder local = buscarlocal(context.IDENT().GetText());
                        pmactual.Emit(OpCodes.Ldloc,local);
                    }
                }

                if (context.NUMBER() != null)
                {
                    pmactual.Emit(OpCodes.Ldc_I4, int.Parse(context.NUMBER()[indexvalor].GetText()));
                    pmactual.Emit(OpCodes.Ceq);
                    pmactual.Emit(OpCodes.Brfalse_S,else_IF);
                    Visit(context.statement()[indexvalor]);
                    pmactual.Emit(OpCodes.Br_S,final);
                    pmactual.MarkLabel(else_IF);
                }
                else
                {
                    string g = context.CHARCONST()[indexvalor].GetText();
                    char[] cl = g.ToCharArray();
                    char ch = cl[1];
                    pmactual.Emit(OpCodes.Ldc_I4_S,(int)ch);
                    pmactual.Emit(OpCodes.Ceq);
                    pmactual.Emit(OpCodes.Brfalse_S,else_IF);
                    Visit(context.statement()[indexvalor]);
                    pmactual.Emit(OpCodes.Br_S,final);
                    pmactual.MarkLabel(else_IF);
                }
                indexvalor++;
            }
            Visit(context.statement()[context.statement().Length - 1]);
            pmactual.MarkLabel(final);
            return null;
        }
        public void Addvars(Parser1.VardeclASTContext context, MethodBuilder pointMainBldr)
        {
            pointMainBldr.InitLocals = true;
            ILGenerator gen = pointMainBldr.GetILGenerator();
            string rtipo = (string)Visit(context.type());
            if (rtipo.Contains("LIST"))
            {
                if (rtipo=="CHAR LIST")
                {
                    if (context.IDENT().Length>1)
                    {
                        typelist = true;
                        Parser1.ChartypeASTContext c =(Parser1.ChartypeASTContext) Visit(context.type());
                        ArrayList arr=null;
                        if (c.NUMBER()!=null)
                        {
                            arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                        }
                        else
                        {
                            arr = new ArrayList();
                        }

                        foreach (var node in context.IDENT())
                        {
                            LocalBuilder a = gen.DeclareLocal(arr.GetType());
                            a.SetLocalSymInfo(node.GetText());
                            ArrayList localinfo= new ArrayList();
                            localinfo.Add(a);
                            localinfo.Add(node.GetText());
                            localesmet.Add(localinfo);
                        }
                        typelist = false;
                    }
                    else
                    {
                        typelist = true;
                        Parser1.ChartypeASTContext c =(Parser1.ChartypeASTContext) Visit(context.type());
                        ArrayList arr=null;
                        if (c.NUMBER()!=null)
                        {
                            arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                        }
                        else
                        {
                            arr = new ArrayList();
                        }
                        LocalBuilder a = gen.DeclareLocal(arr.GetType());
                        a.SetLocalSymInfo(context.IDENT()[0].GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(context.IDENT()[0].GetText());
                        localesmet.Add(localinfo);
                        typelist = false;
                    }
                    return;
                }if (rtipo=="INT LIST")
                {
                    if (context.IDENT().Length>1)
                    {
                        foreach (var node in context.IDENT())
                        {
                            typelist = true;
                            Parser1.InttypeASTContext c =(Parser1.InttypeASTContext) Visit(context.type());
                            ArrayList arr=null;
                            if (c.NUMBER()!=null)
                            {
                                arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                            }
                            else
                            {
                                arr = new ArrayList();
                            }
                            LocalBuilder a = gen.DeclareLocal(arr.GetType());
                            a.SetLocalSymInfo(node.GetText());
                            ArrayList localinfo= new ArrayList();
                            localinfo.Add(a);
                            localinfo.Add(node.GetText());
                            localesmet.Add(localinfo);
                            typelist = false;
                        }
                    }else
                    {
                        typelist = true;
                        Parser1.InttypeASTContext c =(Parser1.InttypeASTContext) Visit(context.type());
                        ArrayList arr=null;
                        if (c.NUMBER()!=null)
                        {
                            arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                        }
                        else
                        {
                            arr = new ArrayList();
                        }
                        LocalBuilder a = gen.DeclareLocal(arr.GetType());
                        a.SetLocalSymInfo(context.IDENT()[0].GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(context.IDENT()[0].GetText());
                        localesmet.Add(localinfo);
                        typelist = false;
                    }
                    return;
                }
                if (rtipo=="IDENT LIST")
                {
                    if (context.IDENT().Length>1)
                    {
                        foreach (var node in context.IDENT())
                        {
                            typelist = true;
                            Parser1.IdenttypeASTContext c =(Parser1.IdenttypeASTContext) Visit(context.type());
                            ArrayList arr=null;
                            if (c.NUMBER()!=null)
                            {
                                arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                            }
                            else
                            {
                                arr = new ArrayList();
                            }
                            LocalBuilder a = gen.DeclareLocal(arr.GetType());
                            a.SetLocalSymInfo(node.GetText());
                            ArrayList localinfo= new ArrayList();
                            localinfo.Add(a);
                            localinfo.Add(node.GetText());
                            localesmet.Add(localinfo);
                            typelist = false;
                        }
                        
                    }
                    else
                    {
                        typelist = true;
                        Parser1.IdenttypeASTContext c =(Parser1.IdenttypeASTContext) Visit(context.type());
                        ArrayList arr=null;
                        if (c.NUMBER()!=null)
                        {
                            arr = new ArrayList(int.Parse(c.NUMBER().GetText()));
                        }
                        else
                        {
                            arr = new ArrayList();
                        }
                        LocalBuilder a = gen.DeclareLocal(arr.GetType());
                        a.SetLocalSymInfo(context.IDENT()[0].GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(context.IDENT()[0].GetText());
                        localesmet.Add(localinfo);
                        typelist = false;
                    }  
                    return;
                }
            }
            if (rtipo=="CHAR")
            {
                if (context.IDENT().Length>1)
                {
                    foreach (var node in context.IDENT())
                    {
                        LocalBuilder a = gen.DeclareLocal(typeof(char));
                        a.SetLocalSymInfo(node.GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(node.GetText());
                        localesmet.Add(localinfo);
                    }
                }
                else
                {
                    LocalBuilder a = gen.DeclareLocal(typeof(char));
                    a.SetLocalSymInfo(context.IDENT()[0].GetText());
                    ArrayList localinfo= new ArrayList();
                    localinfo.Add(a);
                    localinfo.Add(context.IDENT()[0].GetText());
                    localesmet.Add(localinfo);
                }
            }else if (rtipo=="BOOL")
            {
                if (context.IDENT().Length>1)
                {
                    foreach (var node in context.IDENT())
                    {
                        LocalBuilder a = gen.DeclareLocal(typeof(bool));
                        a.SetLocalSymInfo(node.GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(node.GetText());
                        localesmet.Add(localinfo);
                    }
                }
                else
                {
                    LocalBuilder a = gen.DeclareLocal(typeof(bool));
                    a.SetLocalSymInfo(context.IDENT()[0].GetText());
                    ArrayList localinfo= new ArrayList();
                    localinfo.Add(a);
                    localinfo.Add(context.IDENT()[0].GetText());
                    localesmet.Add(localinfo);
                }
            }else if (rtipo=="STRING")
            {
                if (context.IDENT().Length>1)
                {
                    foreach (var node in context.IDENT())
                    {
                        LocalBuilder a = gen.DeclareLocal(typeof(string));
                        a.SetLocalSymInfo(node.GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(node.GetText());
                        localesmet.Add(localinfo);
                    }
                }
                else
                {
                    LocalBuilder a = gen.DeclareLocal(typeof(string));
                    a.SetLocalSymInfo(context.IDENT()[0].GetText());
                    ArrayList localinfo= new ArrayList();
                    localinfo.Add(a);
                    localinfo.Add(context.IDENT()[0].GetText());
                    localesmet.Add(localinfo);
                }
            }else if (rtipo=="FLOAT")
            {
                if (context.IDENT().Length>1)
                {
                    foreach (var node in context.IDENT())
                    {
                        LocalBuilder a = gen.DeclareLocal(typeof(double));
                        a.SetLocalSymInfo(node.GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(node.GetText());
                        localesmet.Add(localinfo);
                    }
                }
                else
                {
                    LocalBuilder a = gen.DeclareLocal(typeof(double));
                    a.SetLocalSymInfo(context.IDENT()[0].GetText());
                    ArrayList localinfo= new ArrayList();
                    localinfo.Add(a);
                    localinfo.Add(context.IDENT()[0].GetText());
                    localesmet.Add(localinfo);
                }
            }else if (rtipo=="INT")
            {
                if (context.IDENT().Length>1)
                {
                    foreach (var node in context.IDENT())
                    {
                        LocalBuilder a = gen.DeclareLocal(typeof(int));
                        a.SetLocalSymInfo(node.GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(node.GetText());
                        localesmet.Add(localinfo);
                    }
                }
                else
                {
                    LocalBuilder a = gen.DeclareLocal(typeof(int));
                    a.SetLocalSymInfo(context.IDENT()[0].GetText());
                    ArrayList localinfo= new ArrayList();
                    localinfo.Add(a);
                    localinfo.Add(context.IDENT()[0].GetText());
                    localesmet.Add(localinfo);
                }
            }
            else
            {
                if (context.IDENT().Length>1)
                {
                    Type t = null;
                    foreach (var clase in clases)
                    {
                        if (((Type)clase).Name==rtipo)
                        {
                            t = (Type) clase;
                        }
                    }
                    foreach (var node in context.IDENT())
                    {
                        //declaracion con clases!
                        LocalBuilder a = gen.DeclareLocal(t);
                        a.SetLocalSymInfo(node.GetText());
                        ArrayList localinfo= new ArrayList();
                        localinfo.Add(a);
                        localinfo.Add(node.GetText());
                        localesmet.Add(localinfo);
                    }
                }
                else
                {
                    //declaracion con clases!
                    Type t = null;
                    foreach (var clase in clases)
                    {
                        if (((Type)clase).Name==rtipo)
                        {
                            t = (Type) clase;
                        }
                    }
                    LocalBuilder a = gen.DeclareLocal(t);
                    a.SetLocalSymInfo(context.IDENT()[0].GetText());
                    ArrayList localinfo= new ArrayList();
                    localinfo.Add(a);
                    localinfo.Add(context.IDENT()[0].GetText());
                    localesmet.Add(localinfo);

                }
            }
        }

        public FieldBuilder buscarglobal(string nombre)
        {
            foreach (var glob in globales)
            {
                if (((FieldBuilder)glob).Name==nombre)
                {
                    return (FieldBuilder)glob;
                }
            }

            return null;
        }

        public int buscarparam(string nombre)
        {
            int i = 0;
            foreach (var param in localesParams)
            {
                if (((ParameterBuilder)param).Name==nombre)
                {
                    return i;
                }

                i++;
            }

            return -1;
        }

        public LocalBuilder buscarlocal(string nombre)
        {
            foreach (var metloc in localesmet)
            {
                if (((ArrayList)metloc)[1].ToString()==nombre)
                {
                    return ((LocalBuilder) ((ArrayList) metloc)[0]);
                }
            }

            return null;
        }

        public MethodBuilder buscarMet(string nombre)
        {
            foreach (var metodo in metodos)
            {
                if (((MethodBuilder)metodo).Name== nombre)
                {
                    return (MethodBuilder) metodo;
                }
            }

            return null;
        }

        public ConstructorBuilder buscarC(string nombre)
        {
            foreach (var cont in contructores)
            {
                if (((string) ((ArrayList) cont)[0]).Equals(nombre))
                {
                    return ((ConstructorBuilder) ((ArrayList) cont)[1]);
                }
            }
            return null;
        }
        
        public object tipowrit(object e)
        {
            if (e is FieldBuilder)
            {
                if (((FieldBuilder)e).FieldType == typeof(string))
                {
                    var tipo = "";
                    return tipo;
                }else if (((FieldBuilder)e).FieldType == typeof(int))
                {
                    var tipo = 1;
                    return tipo;  
                }else if (((FieldBuilder) e).FieldType == typeof(bool) )
                {
                    var tipo = true;
                    return tipo;
                }else if(((FieldBuilder)e).FieldType == typeof(char))
                {
                    var tipo = ' ';
                    return tipo;
                }
                else
                {
                    var tipo = 1.2;
                    return (float)tipo;
                }
            }else if (e is ParameterInfo)
            {
                if (((ParameterInfo)e).ParameterType == typeof(string))
                {
                    var tipo = "";
                    return tipo;
                }else if (((ParameterInfo)e).ParameterType == typeof(int))
                {
                    var tipo = 1;
                    return tipo;  
                }else if (((ParameterInfo) e).ParameterType == typeof(bool))
                {
                    var tipo = true;
                    return tipo;
                }else if(((ParameterInfo)e).ParameterType == typeof(char))
                {
                    var tipo = ' ';
                    return tipo;
                }
                else
                {
                    var tipo = 1.2;
                    return (float)tipo;
                }
            }else if (e is LocalBuilder)
            {
                if (((LocalBuilder)e).LocalType == typeof(string))
                {
                    var tipo = "";
                    return tipo;
                }else if (((LocalBuilder)e).LocalType == typeof(int))
                {
                    var tipo = 1;
                    return tipo;  
                }else if (((LocalBuilder) e).LocalType == typeof(bool))
                {
                    var tipo = true;
                    return tipo;
                }else if(((LocalBuilder)e).LocalType == typeof(char))
                {
                    var tipo = ' ';
                    return tipo;
                }
                else
                {
                    var tipo = 1.2;
                    return (float)tipo;
                }
            }

            return null;
        }
    }
    
    
}