﻿using System;
using System.Collections.Generic;
using System.Text;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;

namespace ProyectoOscarMarco
{
    public class AContext : Parser1BaseVisitor<object>
    {
        private List<string> _errors ;
        private TablaSimbolos Table;
        private TablaSimbolos TableMethodsP;
        private bool retorno;
        private bool retorexp;
        private bool returndesigasing;
        private bool retornoexprterm;
        private bool retornon;
        private bool retornodesign;
        private bool conditions;
        private bool okmet;
        private bool okcond;
        private bool visitastat;
        private bool tipoasig;
        private bool returned;
        public AContext(){
            this.Table= new TablaSimbolos();
            this.TableMethodsP = new TablaSimbolos();
            _errors = new List<string>();
            TableMethodsP.Table.Add(new TablaSimbolos.chr());
            TableMethodsP.Table.Add(new TablaSimbolos.ord());
            TableMethodsP.Table.Add(new TablaSimbolos.Len());
            TableMethodsP.Table.Add(new TablaSimbolos.Null());
            retorno = false;
            retorexp = false;
            returndesigasing = false;
            retornoexprterm = false;
            retornon = false;
            retornodesign = false;
            conditions = false;
            okmet = false;
            okcond = false;
            visitastat = false;
            returned = false;
        }

        public override object VisitStringtypeAST(Parser1.StringtypeASTContext context)
        {
            return "STRING";
        }

        public override object VisitSwitchcaseSTAST(Parser1.SwitchcaseSTASTContext context)
        {
            return null;
        }

        public override object VisitStringFACAST(Parser1.StringFACASTContext context)
        {
            return null;
        }

        public override object VisitSwitchAST(Parser1.SwitchASTContext context)
        {
            return null;
        }

        public override object VisitProgramAST(Parser1.ProgramASTContext context)
        {
            Visit(context.classdecl()[0]);
            if (context.vardecl().Length == 0 && context.methodecl().Length == 0 && context.constdecl().Length == 0 && context.methodecl().Length > 0)
            {
                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR: Esta declarando un bloque vacio!");
                return null;
            }
            
            if (context.vardecl().Length > 0)
            {
                foreach (var vardecl in context.vardecl())
                {
                    Visit(vardecl);
                }
            }
            if (context.constdecl().Length > 0)
            {
                foreach (var constdecl in context.constdecl())
                {
                    Visit(constdecl);
                }
            }
            if (context.classdecl().Length > 1)
            {
                for (int i = 1; i < context.classdecl().Length; i++)
                {
                    Visit(context.classdecl()[i]);
                }
            }
            if (context.methodecl().Length > 0)
            {
                foreach (var methdecl in context.methodecl())
                {
                    Visit(methdecl);
                }
            }
            
            
            return null;
        }

        public override object VisitConstAST(Parser1.ConstASTContext context)
        {
            object Tipo = null;
            if (Table.buscar(context.IDENT().GetText()) == null)
            {
                Tipo = Visit(context.type());
                if ((string)Tipo== "INT")
                {
                    if(context.NUMBER()!= null)
                        Table.Insertar(new TablaSimbolos.Const(context.IDENT().Symbol,Table.Nivel,context.NUMBER().GetText(),(string)Tipo));
                    else
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPO: TIPO DESIGNADO NO ES ALGO VALIDO LAS CONST SOLO PUEDEN SER TIPO INT O CHAR, USTED ESTA ASIGNANDO UN TIPO DISTINTO A LO PERMITIDO O ESTE ASIGNANDO UN TIPO ERRONEO! ");
                }
                else if ((string) Tipo == "CHAR")
                {
                    if(context.CHARCONST() != null)
                        Table.Insertar(new TablaSimbolos.Const(context.IDENT().Symbol,Table.Nivel,context.CHARCONST().GetText(),(string)Tipo));
                    else
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPO: TIPO DESIGNADO NO ES ALGO VALIDO LAS CONST SOLO PUEDEN SER TIPO INT O CHAR, USTED ESTA ASIGNANDO UN TIPO DISTINTO A LO PERMITIDO O ESTE ASIGNANDO UN TIPO ERRONEO! ");
                }
                else
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPO: TIPO DESIGNADO NO ES ALGO VALIDO LAS CONST SOLO PUEDEN SER TIPO INT O CHAR, USTED ESTA ASIGNANDO: " + Visit(context.type()));
                    
                }
            }
            else
            {
                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: Ya existe declarado el identificador: "+context.IDENT().GetText());
            }

            return null;
        }

        public override object VisitVardeclAST(Parser1.VardeclASTContext context)
        {
            bool valido = true;
            object Tipo = null;
            if (Table.buscar(context.IDENT()[0].GetText())==null)
            {
                Tipo = Visit(context.type());

                if ((string)Tipo != "IDENT ARRAY")
                {
                    Table.Insertar(new TablaSimbolos.Var(context.IDENT()[0].Symbol, Table.Nivel, (string)Tipo));
                    if (retorno)
                    {
                        TablaSimbolos.Var nuevo = new TablaSimbolos.Var(context.IDENT()[0].Symbol, Table.Nivel, (string)Tipo);
                        TablaSimbolos.Clase clse = (TablaSimbolos.Clase)Table.buscar(((Parser1.ClassdeclASTContext)context.Parent).IDENT().GetText());
                        clse.addvars(nuevo);
                    }
                    if (context.IDENT().Length > 1 && valido)
                    {
                        for (int i = 1; i < context.IDENT().Length; i++)
                        {
                            if (Table.buscar(context.IDENT()[i].GetText()) == null)
                            {
                                Table.Insertar(new TablaSimbolos.Var(context.IDENT()[i].Symbol, Table.Nivel,(string) Tipo));
                                if (retorno)
                                {
                                    TablaSimbolos.Var nuevo = new TablaSimbolos.Var(context.IDENT()[i].Symbol, Table.Nivel, (string)Tipo);
                                    TablaSimbolos.Clase clse = (TablaSimbolos.Clase) Table.buscar(((Parser1.ClassdeclASTContext) context.Parent).IDENT().GetText());
                                    clse.addvars(nuevo);
                                }
                            }else
                            {
                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: Ya existe declarado el identificador: "+context.IDENT()[i].GetText());
                            }
                        }
                
                    }
                }
                else
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPO: TIPO DESIGNADO NO ES ALGO VALIDO USTED ESTA NOMBRANDO ALGO DE TIPO: "+Tipo);
                    valido = false;
                }
                

            }
            else
            {
                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: Ya existe declarado el identificador: "+context.IDENT()[0].GetText());
            }

           
            
              
            return null;
        }

        public override object VisitClassdeclAST(Parser1.ClassdeclASTContext context)
        {
            if (Table.Table.Count == 0)
            {
                var c = context.IDENT();
                Table.Insertar(new TablaSimbolos.Clase(context.IDENT().Symbol,Table.Nivel)); 
                
            }else{
                if (Table.buscar(context.IDENT().GetText()) == null)
                {
                    
                    Table.Insertar(new TablaSimbolos.Clase(context.IDENT().Symbol, Table.Nivel));
                    Table.openScope();
                    retorno = true;
                    foreach (var var in context.vardecl())
                    {
                        Visit(var);
                    }

                    retorno = false;
                    Table.closeScope();
                }else
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: Ya existe declarado el identificador "+context.IDENT().GetText());
                }

            }


            return null;
        }
        

        public override object VisitMethodeclAST(Parser1.MethodeclASTContext context)
        {
            if (Table.buscar(context.IDENT().GetText())== null)
            {
                if (context.type() == null)
                {
                    object Tipo = "VOID";
                    Table.Insertar(new TablaSimbolos.method(context.IDENT().Symbol, Table.Nivel, (string)Tipo));
                    TablaSimbolos.method temp = (TablaSimbolos.method)Table.buscar(context.IDENT().GetText());
                    temp.setvars(new ArrayList<string>());
                    Table.openScope();
                    if (context.formpars() != null)
                        Visit(context.formpars());

                    foreach (var var in context.vardecl())
                    {
                        Visit(var);
                    }
                    Visit(context.block());

                    Table.closeScope();
                }
                else
                {
                    object Tipo = null;
                    Tipo = Visit(context.type());
                    Table.Insertar(new TablaSimbolos.method(context.IDENT().Symbol, Table.Nivel, (string)Tipo));
                    TablaSimbolos.method temp = (TablaSimbolos.method)Table.buscar(context.IDENT().GetText());
                    temp.setvars(new ArrayList<string>());
                    Table.openScope();
                    if (context.formpars() != null)
                        Visit(context.formpars());
                    foreach (var var in context.vardecl())
                    {
                        Visit(var);
                    }
                    Visit(context.block());
                    if (!returned)
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR: No existe un return en el metodo que usted esta creando");
                    returned = false;
                    Table.closeScope();
                }



            }else
                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: Ya existe declarado el método " + context.IDENT().GetText());
            
            
            return null;
        }

        public override object VisitFormparsAST(Parser1.FormparsASTContext context)
        {
            TablaSimbolos.method met = (TablaSimbolos.method)Table.buscar(((Parser1.MethodeclASTContext) context.Parent).IDENT().GetText());
            bool valido = true;
            string Tipo = null;
            if (Table.buscar(context.IDENT()[0].GetText())==null)
            {
                 Tipo = (string)Visit(context.type()[0]);
                met.vars.Add(Tipo);
                if ((string) Tipo !="IDENT ARRAY")
                {
                    Table.Insertar(new TablaSimbolos.Var(context.IDENT()[0].Symbol,Table.Nivel,(string)Tipo));
                    if (context.IDENT().Length > 1 && valido)
                    {
                        string tipon;
                        for (int i = 1; i < context.IDENT().Length; i++)
                        {
                            if (Table.buscar(context.IDENT()[i].GetText()) == null)
                            {
                                tipon = (string)Visit(context.type()[i]);
                                met.vars.Add(tipon);
                                Table.Insertar(new TablaSimbolos.Var(context.IDENT()[i].Symbol, Table.Nivel,tipon));
                            }else
                            {
                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: Ya existe declarado el identificador: "+context.IDENT()[i].GetText());
                            }
                        }
                
                    }
                }
                else
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPO: TIPO DESIGNADO NO ES ALGO VALIDO USTED ESTA NOMBRANDO ALGO DE TIPO: "+Tipo);
                    valido = false;
                }
                

            }
            else
            {
                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: Ya existe declarado el identificador: "+context.IDENT()[0].GetText());
            }
            return null;
        }

        public override object VisitChartypeAST(Parser1.ChartypeASTContext context)
        {
            if (context.CORCHETEDER()!=null)
            {
                return "CHAR ARRAY";
            }
            return "CHAR";
        }

        public override object VisitInttypeAST(Parser1.InttypeASTContext context)
        {
            if (context.CORCHETEDER()!=null)
            {
                return "INT ARRAY";
            }
            return "INT";
        }

        public override object VisitFloattypeAST(Parser1.FloattypeASTContext context)
        {
            return "FLOAT";
        }

        public override object VisitBooltypeAST(Parser1.BooltypeASTContext context)
        {
            return "BOOL";
        }

        public override object VisitIdenttypeAST(Parser1.IdenttypeASTContext context)
        {
            if (context.CORCHETEDER()!= null)
            {
                return context.IDENT()[0].GetText() + " ARRAY";
            }
            return context.IDENT()[0].GetText();
        }

        public override object VisitDesignatorSTMAST(Parser1.DesignatorSTMASTContext context)
        {
            if (visitastat)
                return context;
            if (context.PLUS() != null)
            {
                if (Visit(context.designator()) != null)
                {
                    object auxP = Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).IDENT()[0].GetText());
                    if (auxP != null)
                    {
                        if (auxP.GetType() == typeof(TablaSimbolos.Var))
                        {
                            TablaSimbolos.Var auxp2 = (TablaSimbolos.Var)auxP;
                            if (auxp2.type != "INT")
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Las expreciones unarias solo se pueden realizar en variables de tipo INT");
                                return null;
                            }
                            return null;
                        }
                        else
                        {
                            _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Esta accion solo se puede hacer en variables!");
                            return null;
                        }
                    }
                    else
                    {
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: No se puede acceder al parametro: " + ((Parser1.DesignatorASTContext)Visit(context.designator())).IDENT()[0].GetText() + " ya que este no a sido nombrado con anterioridad!");
                        return null;
                    }
                }
                else
                {
                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: No existe el parametro al que usted desea realizarle una expresion unaria!");
                    return null;

                }
            }
            else if (context.RESTA() != null)
            {
                if (Visit(context.designator()) != null)
                {
                    object auxP = Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).IDENT()[0].GetText());
                    if (auxP != null)
                    {
                        if (auxP.GetType() == typeof(TablaSimbolos.Var))
                        {
                            TablaSimbolos.Var auxp2 = (TablaSimbolos.Var)auxP;
                            if (auxp2.type != "INT")
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Las expreciones unarias solo se pueden realizar en variables de tipo INT");
                                return null;
                            }
                            return null;
                        }
                        else
                        {
                            _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Esta accion solo se puede hacer en variables!");
                            return null;
                        }
                    }
                    else
                    {
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: No se puede acceder al parametro: " + ((Parser1.DesignatorASTContext)Visit(context.designator())).IDENT()[0].GetText() + " ya que este no a sido nombrado con anterioridad!");
                        return null;
                    }
                }
                else
                {
                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: No existe el parametro al que usted desea realizarle una expresion unaria!");
                    return null;
                }
            }
            if (retorno)
            {
                return context.designator();
            }
            if (context.PDER() != null || context.PIZQ() != null)
            {
                Parser1.DesignatorASTContext tempd = (Parser1.DesignatorASTContext)Visit(context.designator());



                if (context.actpars() != null && tempd != null)
                {
                    retorexp = true;
                    Parser1.ActparsASTContext tempc = (Parser1.ActparsASTContext)Visit(context.actpars());
                    retorexp = false;
                    if (tempd.IDENT()[0].GetText() == "len")
                    {
                        okmet = true;
                        object templ = Visit(tempc.expr()[0]);
                        okmet = false;
                        if (templ.GetType() == typeof(Parser1.DesignatorASTContext))
                            templ = (TablaSimbolos.Var)Table.buscar(((Parser1.DesignatorASTContext)templ).GetText());
                        if (templ.GetType() == typeof(TablaSimbolos.Var))
                        {
                            TablaSimbolos.Var varchange = (TablaSimbolos.Var)templ;
                            if (varchange.type.Contains("ARRAY"))
                            {
                                TablaSimbolos.Len len = (TablaSimbolos.Len)buscarTabM("LEN");
                                len.SizeArr(varchange);
                                return null;

                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "TIPOS INVALIDOS: la funcion len es para usar en tipos ARRAY");
                                return null;
                            }
                        }
                    }
                    else if (tempd.IDENT()[0].GetText() == "chr")
                    {
                        object templ = Visit(tempc.expr()[0]);
                        if (templ.GetType() == typeof(Parser1.DesignatorASTContext))
                            templ = (TablaSimbolos.Var)Table.buscar(((Parser1.DesignatorASTContext)templ).GetText());
                        if (templ.GetType() == typeof(TablaSimbolos.Var))
                        {
                            TablaSimbolos.Var varchange = (TablaSimbolos.Var)templ;
                            if (varchange.type.Contains("INT"))
                            {
                                TablaSimbolos.chr chr = (TablaSimbolos.chr)buscarTabM("CHR");
                                chr.convertir(varchange);
                                return null;
                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "TIPOS INVALIDOS: la funcion chr es para usar en tipos INT");
                                return null;
                            }
                        }

                    }
                    else if (tempd.IDENT()[0].GetText() == "ord")
                    {
                        object templ = Visit(tempc.expr()[0]);
                        if (templ.GetType() == typeof(Parser1.DesignatorASTContext))
                            templ = (TablaSimbolos.Var)Table.buscar(((Parser1.DesignatorASTContext)templ).GetText());
                        if (templ.GetType() == typeof(TablaSimbolos.Var))
                        {
                            TablaSimbolos.Var varchange = (TablaSimbolos.Var)templ;
                            if (varchange.type.Contains("CHAR"))
                            {
                                TablaSimbolos.ord ord = (TablaSimbolos.ord)buscarTabM("ORD");
                                ord.convert(varchange);
                                return null;
                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "TIPOS INVALIDOS: la funcion ord es para usar en tipos CHAR");
                                return null;
                            }
                        }

                    }
                    else
                    {
                        TablaSimbolos.method met = (TablaSimbolos.method)Table.buscar(tempd.IDENT()[0].GetText());
                        if (met != null)
                        {
                            if (met.vars != null)
                            {
                                if (context.actpars() != null)
                                {
                                    return Visit(context.actpars());
                                }

                                return met.type;
                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR: La cantidad de parametros no es la requerida para hacer la llamada al metodo: " + tempd.IDENT()[0].GetText());
                                if (met.vars.Count == 0)
                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "Se requieren la siguiente cantidad: NINGUN PARAMETRO");
                                else
                                {
                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "Se requieren la siguiente cantidad: " + met.vars.Count);
                                }
                            }
                        }
                        else
                        {
                            _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: El método " + tempd.IDENT()[0].GetText() + " no ha sido declarado!!!");
                            return null;
                        }
                    }
                }
                else
                {

                    if (tempd != null)
                    {
                        TablaSimbolos.method met = (TablaSimbolos.method)Table.buscar(tempd.IDENT()[0].GetText());
                        if (met.vars.Count == 0)
                        {
                            return met.type;
                        }
                        else
                        {
                            _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR: La cantidad de parametros no es la requerida para hacer la llamada al metodo: " + tempd.IDENT()[0].GetText());
                            if (met.vars.Count == 0)
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "Se requieren la siguiente cantidad: NINGUN PARAMETRO");
                            else
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "Se requieren la siguiente cantidad: " + met.vars.Count);
                            }
                            return null;
                        }
                    }
                    else
                    {
                        retorno = true;
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: El método " + Visit(context.designator()) + " no ha sido declarado!!!");
                        retorno = false;
                        return null;
                    }
                }
            }
            Parser1.DesignatorASTContext aux = (Parser1.DesignatorASTContext)Visit(context.designator());
            if (aux == null)
            {
                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: La variable que esta pidiendo no existe: " + context.designator().GetText());
                return null;
            }
            object temp = Table.buscar(aux.IDENT()[0].GetText());

            object temp2 = Visit(context.expr());
            if (temp2.GetType() == typeof(Parser1.DesignatorASTContext))
                temp2 = Table.buscar(((Parser1.DesignatorASTContext)temp2).IDENT()[0].GetText());
            if (temp != null && temp2 == null)
            {
                if (temp.GetType() == typeof(TablaSimbolos.Var))
                {
                    TablaSimbolos.Var tem = (TablaSimbolos.Var)temp;
                    if (tem.type.Contains("ARRAY") || tem.type.Contains("IDENT"))
                    {

                        foreach (var local in TableMethodsP.Table)
                        {
                            if (local.GetType() == typeof(TablaSimbolos.Null))
                            {
                                TablaSimbolos.Null tab = (TablaSimbolos.Null)local;
                                tab.asig(temp);
                            }
                        }
                    }
                    else
                    {
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Solo variables del tipo ARRAY o CLASS pueden tomar un valor de NULL o ERROR DE ALCANCES: La variable o constante que esta igualando no ha sido nombrada con anterioridad");
                    }

                }
                else
                {
                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Solo variables pueden ser nombradas como tipo NULL o ERROR DE ALCANCES: La variable o constante que esta igualando no ha sido nombrada con anterioridad");
                }
            }
            else if (temp != null && temp2 != null && (temp2.GetType() == typeof(TablaSimbolos.Var) || temp2.GetType() == typeof(TablaSimbolos.Const) || temp2.GetType() == typeof(TablaSimbolos.Clase)))
            {

                if (temp.GetType() == temp2.GetType())
                {
                    if (temp.GetType() == typeof(TablaSimbolos.Var))
                    {
                        TablaSimbolos.Var ntemp = (TablaSimbolos.Var)temp;
                        TablaSimbolos.Var ntemp2 = (TablaSimbolos.Var)temp2;

                        if (ntemp.type.Contains("ARRAY"))
                        {
                            _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: ESTA IGUALANDO TIPOS ARRAY");
                        }
                        else
                        {
                            ntemp.valor = ntemp2.valor;
                        }
                    }
                    else if (temp.GetType() == typeof(TablaSimbolos.Const))
                    {
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: ESTA IGUALANDO TIPOS CONST Y ESTAS SON INMUTABLES");
                    }
                    else if (temp.GetType() == typeof(TablaSimbolos.Clase))
                    {
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: ESTA IGUALANDO TIPOS CLASES");
                    }
                    else if (temp.GetType() == typeof(TablaSimbolos.method))
                    {
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: ESTA IGUALANDO TIPOS METODOS");
                    }
                }
                else
                {
                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: ESTA IGUALANDO TIPOS DISTINTOS");
                }


            }
            else
            {

                if (Visit(context.expr()) != null)
                {
                    if (((temp2.GetType() == typeof(Parser1.NumFACASTContext)) || (temp2.GetType() == typeof(Parser1.CharFACASTContext))|| (temp2.GetType() == typeof(Parser1.BooleanFACASTContext))||(temp2.GetType() == typeof(Parser1.FloattypeASTContext))|| (temp2.GetType() == typeof(Parser1.StringFACASTContext))) && temp != null)
                    {

                        returndesigasing = true;
                        if (Table.buscar((string)(Visit(context.designator()))) != null)
                        {
                            object busc = Table.buscar((string)(Visit(context.designator())));
                            if (busc.GetType() == typeof(TablaSimbolos.Var))
                            {
                                conditions = true;
                                if (((string)(Visit(context.expr())) == ((TablaSimbolos.Var)busc).type || ((TablaSimbolos.Var)busc).type.Contains(((string)(Visit(context.expr()))))))
                                {
                                    conditions = false;
                                    returndesigasing = false;
                                    return null;
                                }
                                else
                                {
                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: El tipo que usted esta queriendo asignar es: " + ((string)(Visit(context.expr())) + " pero la variable a la cual le esta queriendo asignar ese valor es de tipo: " + ((TablaSimbolos.Var)busc).type));
                                    conditions = false;
                                    returndesigasing = false;
                                    return null;
                                }
                            }
                            returndesigasing = false;
                        }
                        else
                        {
                            returndesigasing = false;
                            retornon = true;
                            Visit(context.expr());
                            retornon = false;
                        }

                        return null;
                    }
                   
                    else if (Visit(context.expr()).GetType() == typeof(Parser1.NewFACASTContext))
                    {
                        retorno = true;
                        if (Visit(context.expr()) == "NEW IDENT")
                        {
                            tipoasig = true;
                            retorno = false;
                            returndesigasing = true;
                            if (Table.buscar(((string)Visit(context.designator()))).GetType() == null || Table.buscar(((string)Visit(context.designator()))).GetType() != typeof(TablaSimbolos.Var))
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Esta asignando valores a un tipo invalido(const o otro tipo) solo se admiten variables");
                                tipoasig = false;
                                returndesigasing = false;
                                return null;
                            }
                            returndesigasing = false;
                            TablaSimbolos.Var v;
                            if (((Parser1.DesignatorASTContext)Visit(context.designator())).GetText().Contains("["))
                            {
                                returndesigasing = true;
                                v = (TablaSimbolos.Var)Table.buscar((string)Visit(context.designator()));
                                returndesigasing = false;
                            }
                            else
                                v = (TablaSimbolos.Var)Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).GetText());
                            if (v.type != Visit(context.expr()) && !v.type.Contains(((string)Visit(context.expr()))))
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: El tipo que usted esta queriendo asignar es: " + Visit(context.expr()) + " pero la variable a la cual le esta queriendo asignar ese valor es de tipo: " + v.type);
                                tipoasig = false;
                                return null;
                            }
                            else
                            {
                                tipoasig = false;
                                return null;
                            }
                        }
                        retorno = false;
                        object instas = Table.buscar(((Parser1.DesignatorASTContext)(Visit(context.expr()))).IDENT()[0].GetText());
                        if (instas.GetType() != typeof(TablaSimbolos.Var))
                        {
                            if (instas.GetType() != typeof(TablaSimbolos.method))
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Esta asignando valores a un tipo invalido(const o otro tipo) solo se admiten variables");
                        }
                    }
                    okmet = false;
                    conditions = false;
                    object bus = Table.buscar(((Parser1.DesignatorASTContext)Visit(context.expr())).GetText());
                    if (bus != null)
                    {
                        if (bus.GetType() == typeof(TablaSimbolos.method))
                        {
                            if ((TablaSimbolos.method)Table.buscar(((Parser1.DesignatorASTContext)Visit(context.expr())).GetText()) != null)
                            {

                                TablaSimbolos.method met = (TablaSimbolos.method)Table.buscar(((Parser1.DesignatorASTContext)Visit(context.expr())).GetText());
                                if (met.type == "VOID")
                                {

                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: No puede darle valor a una variable sobre un retorno de tipo VOID de una función!");
                                }
                                else if (Table.buscar(context.designator().GetText()).GetType() == null)
                                {
                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: La variable a la cual usted le quiere asignar el valor no a sido nombrada");
                                }
                                else if (Table.buscar(context.designator().GetText()).GetType() == typeof(TablaSimbolos.Var))
                                {
                                    TablaSimbolos.Var var = (TablaSimbolos.Var)Table.buscar(context.designator().GetText());
                                    if (var.type == met.type)
                                    {
                                        returndesigasing = true;
                                        Visit(context.expr());
                                        returndesigasing = false;
                                        return null;
                                    }
                                    else
                                    {
                                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: El tipo que usted esta queriendo asignar es: " + met.type + " pero la variable a la cual le esta queriendo asignar ese valor es de tipo: " + var.type);
                                    }

                                }
                                else
                                {
                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: No puede darle valor del retorno de una función a algo que no sea una variable del mismo tipo de retorno!");
                                }

                            }
                        }

                        else if (((Parser1.DesignatorASTContext)(Visit(context.expr()))).PUNTO() != null)
                        {
                            retornodesign = true;
                            string desg = (string)Visit(context.designator());
                            retornodesign = false;
                            if (Table.buscar(desg).GetType() == typeof(TablaSimbolos.Const))
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Las constantes son valores inmutables, usted esta queriendo asignar un valor a la constante:" + ((TablaSimbolos.Const)Table.buscar(desg)).tok.Text);
                                return null;
                            }
                            TablaSimbolos.Var v = (TablaSimbolos.Var)Table.buscar(desg);
                            TablaSimbolos.Var Var = (TablaSimbolos.Var)Table.buscar(((Parser1.DesignatorASTContext)(Visit(context.expr()))).IDENT()[0].GetText());
                            TablaSimbolos.Clase cls = (TablaSimbolos.Clase)Table.buscar(Var.type);
                            if (cls != null)
                            {
                                Parser1.DesignatorASTContext cont = ((Parser1.DesignatorASTContext)(Visit(context.expr())));
                                for (int i = 1; i < cont.IDENT().Length; i++)
                                {
                                    if (cont.IDENT().Length == 2)
                                    {
                                        for (int k = 0; k < cls.vars.Count; k++)
                                        {
                                            if (cont.IDENT()[i].GetText() == cls.vars[k].tok.Text)
                                            {
                                                if (cls.vars[k].type != v.type)
                                                {
                                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Los tipos que esta queriendo nombrar no son los correctos: " + v.type + " y " + cls.vars[k].type);
                                                    return null;
                                                }

                                            }
                                        }
                                    }
                                    else if (cont.IDENT().Length >= 3)
                                    {
                                        if (cls.buscvars(cont.IDENT()[i].GetText()))
                                        {

                                            TablaSimbolos.Var vaux = cls.returnv(cont.IDENT()[i].GetText());
                                            if (vaux != null && cont.PUNTO().Length == i)
                                            {
                                                if (((TablaSimbolos.Var)cls.returnv(cont.IDENT()[i].GetText())).type !=
                                                v.type)
                                                {
                                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Los tipos son distintos a la variable que necesita un valor de tipo: " + v.type + " y esta tratando de asignar un tipo: " + ((TablaSimbolos.Var)cls.returnv(cont.IDENT()[i].GetText())).type);
                                                    return null;
                                                }
                                                else
                                                {
                                                    return null;
                                                }

                                            }
                                            else
                                            {
                                                cls = (TablaSimbolos.Clase)Table.buscar(vaux.type);

                                            }

                                        }
                                        else
                                        {
                                            if (cls.buscvars(cont.IDENT()[i].GetText()))
                                            {
                                                cls = (TablaSimbolos.Clase)Table.buscar(cont.IDENT()[i].GetText());
                                            }
                                            else
                                            {
                                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: Variable: " + cont.IDENT()[i].GetText() + " no ha sido declarada en la clase: " + cls.tok.Text);
                                                return null;
                                            }

                                        }
                                    }

                                }
                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: La clase llamada: " + cls.tok.Text + " no esta previamente nombrada!");
                            }
                            return null;
                        }
                        else if (bus.GetType() == typeof(TablaSimbolos.Var))
                        {
                            if ((TablaSimbolos.Var)Table.buscar(((Parser1.DesignatorASTContext)Visit(context.expr())).GetText()) != null)
                            {

                                TablaSimbolos.Var var1 = (TablaSimbolos.Var)Table.buscar(((Parser1.DesignatorASTContext)Visit(context.expr())).GetText());
                                if (var1.type == "VOID")
                                {

                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: No puede darle valor a una variable sobre un retorno de tipo VOID de una función!");
                                }
                                else if (Table.buscar(context.designator().GetText()).GetType() == null)
                                {
                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: La variable a la cual usted le quiere asignar el valor no a sido nombrada");
                                }
                                else if (Table.buscar(context.designator().GetText()).GetType() == typeof(TablaSimbolos.Var))
                                {
                                    TablaSimbolos.Var var2 = (TablaSimbolos.Var)Table.buscar(context.designator().GetText());
                                    if (var1.type == var2.type)
                                    {
                                        returndesigasing = true;
                                        Visit(context.expr());
                                        returndesigasing = false;
                                        return null;
                                    }
                                    else
                                    {
                                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: El tipo que usted esta queriendo asignar es: " + var1.type + " pero la variable a la cual le esta queriendo asignar ese valor es de tipo: " + var2.type);
                                    }

                                }
                                else
                                {
                                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: No puede darle valor del retorno de una función a algo que no sea una variable del mismo tipo de retorno!");
                                }

                            }

                        }
                        else
                        {
                            _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: La expresión o función no existe");
                        }

                    }
                    else
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: ASEGURESE QUE LAS VARIABLES SON NOMBRADAS CON ANTERIORIDAD");
                }
                return null;
            }
            return null;
        }

        public override object VisitIfSTMAST(Parser1.IfSTMASTContext context)
        {
            if (visitastat)
                return context;
            Visit(context.condition());
            Visit(context.statement()[0]);
            if (context.statement().Length > 1)
            {
                Visit(context.statement()[1]);
            }

            return null;
        }

        public override object VisitForSTMAST(Parser1.ForSTMASTContext context)
        {
            if (visitastat)
                return context;
            Visit(context.expr());
            Visit(context.condition());
            if (context.statement().Length>1)
            {
                Visit(context.statement()[0]);
                Visit(context.statement()[1]);
            }
            else
            {
                Visit(context.statement()[0]);
            }

            return null;
        }

        public override object VisitWhileSTMAST(Parser1.WhileSTMASTContext context)
        {
            if (visitastat)
                return context;
            Visit(context.condition());
            Visit(context.statement());
            return null;
        }

        public override object VisitBreakSTMAST(Parser1.BreakSTMASTContext context)
        {
            if (visitastat)
                return context;
            return null;
        }

        public override object VisitReturnSTMAST(Parser1.ReturnSTMASTContext context)
        {
            returned = true;
            if (visitastat)
                return context;

            object Tipo = null;
            if (Visit(context.expr()) == null) {
                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCE: El valor a retornar es inexistente");
                return null;
            }
            if (Visit(context.expr()).GetType() == typeof(Parser1.DesignatorASTContext)) {
                Parser1.DesignatorASTContext desg = (Parser1.DesignatorASTContext)Visit(context.expr());

                if (desg != null)
                {
                    object ob = Table.buscar(desg.GetText());
                    if (ob != null)
                    {
                        if (Table.buscar(desg.GetText()).GetType() == typeof(TablaSimbolos.Const))
                        {
                            TablaSimbolos.Const aux = (TablaSimbolos.Const)Table.buscar(desg.GetText());
                            Tipo = aux.type;
                        }
                        else
                        {
                            TablaSimbolos.Var aux = (TablaSimbolos.Var)Table.buscar(desg.GetText());
                            Tipo = aux.type;
                        }
                    }
                    else {
                        if (desg.IDENT()[0].GetText() == "null") {
                            Tipo = "NULL";
                        }
                        if (desg.IDENT()[0].GetText() == "true" || desg.IDENT()[0].GetText() == "false") {
                            Tipo = "BOOL";
                        }
                    }
                }
                
            } else{
                if (Visit(context.expr()).GetType() == typeof(Parser1.NumFACASTContext))
                {
                    Tipo = "INT";
                }
                else if (Visit(context.expr()).GetType() == typeof(Parser1.CharFACASTContext))
                {
                    Tipo = "CHAR";
                }
                else if (Visit(context.expr()).GetType() == typeof(Parser1.BooleanFACASTContext))
                {
                    Tipo = "BOOL";
                }
                else if (Visit(context.expr()).GetType() == typeof(Parser1.StringFACASTContext))
                {
                    Tipo = "STRING";
                }
                else if (Visit(context.expr()).GetType() == typeof(Parser1.DesignatorASTContext)) {
                    if (((Parser1.DesignatorASTContext)Visit(context.expr())).IDENT()[0].GetText() == "true" || ((Parser1.DesignatorASTContext)Visit(context.expr())).IDENT()[0].GetText() == "false") {
                        Tipo = "BOOL";
                    }
                }

                else
                {
                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Esta enviando un tipo no valido para un retorno");
                    return null;
                }

            }
            RuleContext tempc = (RuleContext) Visit(context.expr());
            while (!(tempc.GetType() == typeof(Parser1.MethodeclASTContext)))
            {
                tempc = tempc.Parent;
                if (tempc.GetType() == typeof(Parser1.MethodeclASTContext))
                {
                    break;
                }
            }

            TablaSimbolos.method met = (TablaSimbolos.method)Table.buscar(((Parser1.MethodeclASTContext) tempc).IDENT().GetText());
            if (met!=null)
            {
                if ((string)Tipo == met.type)
                {
                    return null;
                }
                else if (met.type == "VOID") {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: La funcion que usted esta retornando no tiene necesidad de return ya que es de tipo VOID");
                }
                else
                {
                    if(Tipo != "NULL")
                        _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Tipo de retorno no concuerda a el retorno requerido por la funcion: " + ((Parser1.MethodeclASTContext)tempc).IDENT().GetText() + ", el cual necesita retornar un valor: " + met.type);
                }
            }
            return null;
        }

        public override object VisitReadSTMAST(Parser1.ReadSTMASTContext context)
        {
            if (visitastat)
                return context;
            return null;
        }

        public override object VisitWriteSTMAST(Parser1.WriteSTMASTContext context)
        {
            if (visitastat)
                return context;
            return null;
        }

        public override object VisitBlockSTMAST(Parser1.BlockSTMASTContext context)
        {
            if (visitastat)
                return context;
            Visit(context.block());
            Table.openScope();
            return null;
        }

        public override object VisitPycomaSTMAST(Parser1.PycomaSTMASTContext context)
        {
            return null;
        }

        public override object VisitBlockAST(Parser1.BlockASTContext context)
        {
            if ((context.vardecl().Length == 0|| context.vardecl().Length< 1) && (context.methodecl().Length== 0 ||context.methodecl().Length <1) && (context.constdecl().Length == 0|| context.constdecl().Length <1) && (context.statement().Length ==0 || context.statement().Length<1))
            {
                _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR: Esta declarando un bloque vacio!");
                return null;
            }
            if (context.vardecl().Length >0)
            {
                foreach (var vardecl in context.vardecl())
                {
                    Visit(vardecl);
                    //_errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR: NO SE PUEDEN DECLARAR VARIABLES DENTRO DE BLOQUES!!!");
                }   
            }

            if (context.methodecl().Length > 0)
            {
                foreach (var methdecl in context.methodecl())
                {
                    Visit(methdecl);
                }
            }

            if (context.constdecl().Length>0)
            {
                foreach (var constdecl in context.constdecl())
                {
                    Visit(constdecl);
                }
            }

            if (context.statement().Length > 0)
            {
                if (context.statement().Length == 0) {
                    visitastat = true;
                    object  visita = Visit(context.statement()[0]);
                    if (visita.GetType() == typeof(Parser1.BlockASTContext) || visita.GetType() == typeof(Parser1.BlockSTMASTContext))
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR: Esta declarando un bloque vacio!");
                    visitastat = false;
                }
                foreach (var stat in context.statement())
                {
                    Visit(stat);
                }
            }
            if (context.Parent.Parent != null)
            {
                if (context.Parent.Parent.GetType() == typeof(Parser1.IfSTMASTContext))
                {
                    Console.WriteLine(context.statement().Length);
                    if (context.statement().Length == 0)
                    {
                            _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR: Esta declarando un bloque vacio!");
                    }
                }
            }

            return null;
        }

        public override object VisitActparsAST(Parser1.ActparsASTContext context)
        {
            if (retorexp)
            {
                return context;
            }
            if (returndesigasing)
            {
                retorno = true;
                returndesigasing = false;
                 okmet= true;
                Parser1.DesignatorASTContext tempvisit = (Parser1.DesignatorASTContext)Visit((Parser1.DesignatorFACASTContext)context.Parent);
                okmet = false;
                TablaSimbolos.method mt = (TablaSimbolos.method)Table.buscar(tempvisit.IDENT()[0].GetText());
                retorno = false;

                if (mt == null)
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: El método " + tempvisit.IDENT()[0].GetText() + " no ha sido declarado!!!");
                }
                else if (mt.vars == null && context.expr().Length > 0)
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR: La cantidad de parametros no es la requerida para hacer la llamada al metodo: " + tempvisit.IDENT()[0].GetText());
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"Este metodo no cuenta con parametros definidos");
                }
                else if (mt.vars.Count == context.expr().Length)
                {
                    for (int i = 0; i < mt.vars.Count; i++)
                    {
                        retorno = true;
                        if (mt.vars[i] != (string)Visit(context.expr(i)))
                        {
                            if ((string)Visit(context.expr(i)) == null)
                            {
                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"Tipos distintos entre los parametros de funciones, chequear tipos incompatibles entre " + mt.type + " y NULL(VARIABLE O CONST NO HA SIDO NOMBRADA!)");
                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+ " Tipos distintos entre los parametros de funciones,chequear tipos incompatibles entre " + mt.vars[i] + " y " + (string)Visit(context.expr(i)));
                            }
                            String p = "{";
                            bool band = true;
                            foreach (string a in mt.vars)
                            {
                                if (band)
                                {
                                    p += "" + a;
                                    band = false;
                                }
                                else
                                {
                                    p += ",";
                                    p += "" + a;
                                }
                            }
                            p += "}";
                            _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"El metodo " + tempvisit.IDENT()[0].GetText() + " necesita:" + p);
                            retorno = false;
                        }
                        retorno = false;
                    }
                }
                else
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR: La cantidad de parametros no es la requerida para hacer la llamada al metodo: " + tempvisit.IDENT()[0].GetText());
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"Se requieren la siguiente cantidad: " + mt.vars.Count + " y  la cantidad que usted esta enviando es de:" + context.expr().Length);
                }
                return context;

            }
            else
            {
                retorno = true;
                Parser1.DesignatorASTContext tempvisit = (Parser1.DesignatorASTContext)Visit((Parser1.DesignatorSTMASTContext)context.Parent);
                retorno = false;
                TablaSimbolos.method mt = (TablaSimbolos.method)Table.buscar(tempvisit.IDENT()[0].GetText());
                if (mt == null)
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: El método " + tempvisit.IDENT()[0].GetText() + " no ha sido declarado!!!");
                }
                else if (mt.vars == null && context.expr().Length > 0)
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR: La cantidad de parametros no es la requerida para hacer la llamada al metodo: " + tempvisit.IDENT()[0].GetText());
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"Este metodo no cuenta con parametros definidos");
                }
                else if (mt.vars.Count == context.expr().Length)
                {
                    for (int i = 0; i < mt.vars.Count; i++)
                    {
                        retorno = true;
                        string var = "";
                        if (Visit(context.expr(i)).GetType() == typeof(Parser1.NumFACASTContext)|| Visit(context.expr(i)) == "INT")
                            var = "INT";
                        if (Visit(context.expr(i)).GetType() == typeof(Parser1.BooleanFACASTContext)|| Visit(context.expr(i)) == "BOOL")
                            var = "BOOL";
                        if (Visit(context.expr(i)).GetType() == typeof(Parser1.StringFACASTContext)|| Visit(context.expr(i)) == "STRING")
                            var = "STRING";
                        if (Visit(context.expr(i)).GetType() == typeof(Parser1.CharFACASTContext)|| Visit(context.expr(i)) == "CHAR")
                            var = "CHAR";
                        if (mt.vars[i] != var)
                        {
                            if ((string)Visit(context.expr(i)) == null)
                            {
                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"Tipos distintos entre los parametros de funciones, chequear tipos incompatibles entre " + mt.type + " y NULL(VARIABLE O CONST NO HA SIDO NOMBRADA!)");
                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"Tipos distintos entre los parametros de funciones, chequear tipos incompatibles entre " + mt.vars[i] + " y " + var);
                            }
                            String p = "{";
                            bool band = true;
                            foreach (string a in mt.vars)
                            {
                                if (band)
                                {
                                    p += "" + a;
                                    band = false;
                                }
                                else
                                {
                                    p += ",";
                                    p += "" + a;
                                }
                            }
                            p += "}";
                            _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"El metodo " + tempvisit.IDENT()[0].GetText() + " necesita:" + p);
                        }
                    }
                    retorno = false;
                }
                else
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR: La cantidad de parametros no es la requerida para hacer la llamada al metodo: " + tempvisit.IDENT()[0].GetText());
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"Se requieren la siguiente cantidad: " + mt.vars.Count + " y  la cantidad que usted esta enviando es de:" + context.expr().Length);
                }
                return context;
            }
            return context;
        }
            
        

        public override object VisitConditiontermAST(Parser1.ConditiontermASTContext context)
        {
            object visit = Visit(context.condterm()[0]);
            if (visit!=null) {
                if (visit != "BOOL")
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERORR DE TIPOS: La expresion contiene algo que no es de tipo BOOL!!");
                    return null;

                }
            }
            for (int i = 1; i < context.condterm().Length; i++) {
                object visitfor = Visit(context.condterm()[i]);
                if (visitfor != null)
                {
                    if (visitfor != "BOOL")
                    {
                        _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERORR DE TIPOS: La expresion contiene algo que no es de tipo BOOL!!");
                        return null;

                    }

                }

            }
            
            
            return "BOOL";
        }


        public override object VisitCondtermAST(Parser1.CondtermASTContext context)
        {
            object visit = Visit(context.condfact()[0]);
            if (visit != null)
            {
                if (visit != "BOOL")
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERORR DE TIPOS: La expresion contiene algo que no es de tipo BOOL!!");
                    return null;

                }
            }
            for (int i = 1; i < context.condfact().Length; i++)
            {
                object visitfor = Visit(context.condfact()[i]);
                if (visitfor != null)
                {
                    if (visitfor != "BOOL")
                    {
                        _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERORR DE TIPOS: La expresion contiene algo que no es de tipo BOOL!!");
                        return null;

                    }

                }

            }

            return "BOOL";
        }
        

        public override object VisitCondfactAST(Parser1.CondfactASTContext context)
        {
            if (((Parser1.RelopContext)Visit(context.relop())).GetText() == ">" || ((Parser1.RelopContext)Visit(context.relop())).GetText() == ">=" || ((Parser1.RelopContext)Visit(context.relop())).GetText() == "<" || 
                ((Parser1.RelopContext)Visit(context.relop())).GetText() == "<="||
                ((Parser1.RelopContext)Visit(context.relop())).GetText() == ">")
            {
                conditions = true;
                if (Visit(context.expr()[0]) == null || Visit(context.expr()[1]) == null) {
                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: No existe definido la variable o constante que esta colocando en la condicion!");
                    return null;
                }
                if (Visit(context.expr()[0]) == "INT")
                {
                    if (Visit(context.expr()[1]) == "INT")
                    {
                        conditions = false;
                        return "BOOL";
                    }
                    else
                    {
                        conditions = false;
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Esos operadores solo se pueden utilizar con tipos enteros!");
                    }
                    conditions = false;
                }
                else {
                    conditions = false;
                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE TIPOS: Esos operadores solo se pueden utilizar con tipos enteros!");
                }
                conditions = false;
            }
            else if (((Parser1.RelopContext)Visit(context.relop())).GetText() == "!=" || ((Parser1.RelopContext)Visit(context.relop())).GetText() == "==")
            {
                conditions = true;

                if (Visit(context.expr()[0]) != null)
                {
                    if (Visit(context.expr()[1]) != null)
                    {

                        if (Visit(context.expr()[0]) == "INT" || Visit(context.expr()[0]) == "BOOL" ||
                            Visit(context.expr()[0]) == "CHAR")
                        {
                            if (Visit(context.expr()[1]) == "INT" || Visit(context.expr()[1]) == "BOOL" ||
                                Visit(context.expr()[1]) == "CHAR")
                            {
                                conditions = false;
                                return "BOOL";
                            }
                            conditions = false;
                        }
                        conditions = false;
                    }
                    else {
                        conditions = false;
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: No existe definido la variable o constante que esta colocando en la condicion!");
                    }
                }
                else {
                    conditions = false;
                    _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: No existe definido la variable o constante que esta colocando en la condicion!");
                }

                return "BOOL";
            }
            else {
               _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: SE NECESITA {>,<,>=,<=,==,!=}");
            }
            return null;
        }

        public override object VisitExprAST(Parser1.ExprASTContext context)
        {
            if (retornon) {
                for (int i = 0; i < context.term().Length; i++) {
                    Visit(context.term()[i]);
                }

                if (context.addop() != null) { 
                retornoexprterm = true;
                for (int k = 0; k < context.term().Length; k++) {
                    if (Visit(context.term()[k]).GetType() == typeof(Parser1.NumFACASTContext)) {
                            if (context.addop().Length > 0)
                            {
                                Visit(context.addop()[k]);
                            }
                            else {
                                retornoexprterm = false;
                                return "INT";
                            }

                    }
                    else
                    {
                            _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Usted esta operando un tipo invalido para la expreción");
                            retornoexprterm = false;
                            return null;
                    }
                }
                }
                retornoexprterm = false;
            }
            return Visit(context.term()[0]);
        }

        public override object VisitTermAST(Parser1.TermASTContext context)
        {
            if (retornoexprterm) {
                return Visit(context.factor()[0]);
            }
            if (retornon) {
                if (Visit(context.factor()[0]).GetType() == typeof(Parser1.NumFACASTContext) || Visit(context.factor()[0]).GetType() == typeof(Parser1.DesignatorASTContext))
                {
                    if (Visit(context.factor()[0]).GetType() == typeof(Parser1.DesignatorASTContext))
                    {
                        object aux = Table.buscar(((Parser1.DesignatorASTContext)Visit(context.factor()[0])).IDENT()[0].GetText());
                        if (aux != null)
                        {
                            if (aux.GetType() == typeof(TablaSimbolos.Var))
                            {
                                TablaSimbolos.Var naux = (TablaSimbolos.Var)aux;
                                if (naux.type != "INT")
                                {
                                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Tipo no valido para la expresion!");
                                    return null;
                                }
                            }
                            else if (aux.GetType() == typeof(TablaSimbolos.Const))
                            {
                                TablaSimbolos.Const naux = (TablaSimbolos.Const)aux;
                                if (naux.type != "INT")
                                {
                                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Tipo no valido para la expresion!");
                                    return null;
                                }
                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Usted esta enviando a una expresion algo que no es una variable o una constante!");
                            }
                        }
                        else
                        {
                            _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: La variable o constante " + ((Parser1.DesignatorASTContext)Visit(context.factor()[0])).IDENT()[0].GetText() + " no fue previamente nombrada!");
                            return null;
                        }
                    }
                    for (int i = 1; i < context.factor().Length; i++)
                    {
                        if (Visit(context.factor()[i]).GetType() == typeof(Parser1.NumFACASTContext) || Visit(context.factor()[0]).GetType() == typeof(Parser1.DesignatorASTContext))
                        {
                            if (context.mulop().Length > 0)
                            {
                                if (Visit(context.factor()[0]).GetType() == typeof(Parser1.DesignatorASTContext))
                                {
                                    object aux = Table.buscar(((Parser1.DesignatorASTContext)Visit(context.factor()[0])).IDENT()[0].GetText());
                                    if (aux != null)
                                    {
                                        if (aux.GetType() == typeof(TablaSimbolos.Var))
                                        {
                                            TablaSimbolos.Var naux = (TablaSimbolos.Var)aux;
                                            if (naux.type != "INT")
                                            {
                                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Tipo no valido para la expresion!");
                                                return null;
                                            }
                                        }
                                        else if (aux.GetType() == typeof(TablaSimbolos.Const))
                                        {
                                            TablaSimbolos.Const naux = (TablaSimbolos.Const)aux;
                                            if (naux.type != "INT")
                                            {
                                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Tipo no valido para la expresion!");
                                                return null;
                                            }
                                        }
                                        else
                                        {
                                            _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Usted esta enviando a una expresion algo que no es una variable o una constante!");
                                        }
                                    }
                                    else
                                    {
                                        _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE ALCANCES: La variable o constante " + ((Parser1.DesignatorASTContext)Visit(context.factor()[0])).IDENT()[0].GetText() + " no fue previamente nombrada!");
                                        return null;
                                    }
                                    Visit(context.mulop()[i - 1]);

                                }
                                else
                                {
                                    return null;
                                }
                            }
                            else
                            {
                                _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Usted esta operando un tipo invalido para la expreción");
                                return null;
                            }
                        }
                    }
                }
                else
                {
                    _errors.Add("ERROR EN LINEA "+"{"+context.Start.Line+":"+context.Start.Column+"}. "+"ERROR DE TIPOS: Usted esta operando un tipo invalido para la expreción");
                    return null;
                }
            }else if (context.factor().Length > 0)
                return Visit(context.factor()[0]);
            return null;
        }

        public override object VisitDesignatorFACAST(Parser1.DesignatorFACASTContext context)
        {
            if (returndesigasing) {
                return Visit(context.actpars()[0]);
            }
            if (conditions) {
                if (Visit(context.designator()) != null)
                    if (okmet)
                        return Visit(context.designator());
                    else if (((Parser1.DesignatorASTContext)Visit(context.designator())).GetText() == "true" || ((Parser1.DesignatorASTContext)Visit(context.designator())).GetText() == "false")
                        return (Parser1.DesignatorASTContext)Visit(context.designator());
                    else {
                        if (((Parser1.DesignatorASTContext)Visit(context.designator())).GetText() == "len")
                        {
                            return "INT";
                        }
                        else if (((Parser1.DesignatorASTContext)Visit(context.designator())).GetText() == "false" || ((Parser1.DesignatorASTContext)Visit(context.designator())).GetText() == "true")
                        {
                            return "BOOL";
                        }
                        else if (((Parser1.DesignatorASTContext)Visit(context.designator())).GetText().Contains("["))
                        {
                            returndesigasing = true;
                            object retarr = (((TablaSimbolos.Var)Table.buscar((string)Visit(context.designator()))).type);
                            returndesigasing = false;
                            return retarr;
                        }
                        else if (((Parser1.DesignatorASTContext)Visit(context.designator())).GetText() == "null")
                        {
                            return "NULL";
                        }
                        else if (Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).GetText()) != null)
                        {
                            if (Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).GetText()).GetType() == typeof(TablaSimbolos.method))
                            {
                                return (((TablaSimbolos.method)Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).GetText())).type);
                            }

                        }
                        else if (Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).GetText()) != null)
                        {
                            if (Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).GetText()).GetType() == typeof(TablaSimbolos.Const))
                            {

                                return (((TablaSimbolos.Const)Table.buscar(((Parser1.DesignatorASTContext)Visit(context.designator())).GetText())).type);
                            }
                        }
                        returndesigasing = true;
                        object r;
                        if (Table.buscar((string)Visit(context.designator())).GetType() == typeof(TablaSimbolos.Const))
                             r = (((TablaSimbolos.Const)Table.buscar((string)Visit(context.designator()))).type);
                        else
                            r = (((TablaSimbolos.Var)Table.buscar((string)Visit(context.designator()))).type);
                        returndesigasing = false;
                        return r;
                    }
                else
                {
                    return null;
                }
            }
            return Visit(context.designator());
        }

        public override object VisitNumFACAST(Parser1.NumFACASTContext context)
        {
            if (conditions)
            {
                return "INT";
            }
            if (retorno) {
                return "INT";
            }
            return context;
        }

        public override object VisitCharFACAST(Parser1.CharFACASTContext context)
        {
            if (conditions)
                return "CHAR";
            if (retorno)
            {
                return "CHAR";
            }
            return context;
        }

        public override object VisitBooleanFACAST(Parser1.BooleanFACASTContext context)
        {
            if (conditions)
                return "BOOL";
            if (retorno)
            {
                return "BOOL";
            }
            return context;
        }

        public override object VisitNewFACAST(Parser1.NewFACASTContext context)
        {
            if (conditions)
                return "NEW IDENT";
            if (retorno)
            {
                return "NEW IDENT";
            }
            if (tipoasig) {
                if (context.type() != null)
                {
                    if (Visit(context.type()) == "ARRAY") {
                        return context.IDENT().GetText();
                    }
                    return Visit(context.type());
                }
                else if (context.IDENT() != null)
                {
                    return context.IDENT().GetText();
                }
            }
            return context;
        }

        public override object VisitExprFACAST(Parser1.ExprFACASTContext context)
        {
            
            return context;
        }

        public override object VisitDesignatorAST(Parser1.DesignatorASTContext context)
        {
            if (retornodesign) {
                return context.IDENT()[0].GetText();
            }
            object desig = null;
            if (context.IDENT()[0].GetText() == "len" || context.IDENT()[0].GetText() == "ord" || context.IDENT()[0].GetText() == "chr")
            {
                return context;
            }
            else {
                if (Table.buscar(context.IDENT()[0].GetText()) != null)
                {
                    if (retorno)
                    {
                        desig = Table.buscar(context.IDENT()[0].GetText());
                        if (desig != null)
                        {
                            if (desig.GetType() == typeof(TablaSimbolos.Var)) {
                                if (okmet)
                                    return context;
                                return ((TablaSimbolos.Var)desig).type;

                            }
                                
                            if (desig.GetType() == typeof(TablaSimbolos.Const))
                            {
                                if (okmet)
                                    return context;

                                return ((TablaSimbolos.Const)desig).type;
                            }

                            if (desig.GetType() == typeof(TablaSimbolos.method)) {
                                 if (okmet)
                                    return context;
                                return ((TablaSimbolos.method)desig).type;
                            }
                                
                        }
                        else {
                            return context.IDENT()[0].GetText();
                        }
                        
                    }

                }
                else
                {
                    if (retorno)
                    {

                        return context.IDENT()[0].GetText();
                    }
                    else if (context.IDENT()[0].GetText() == "true" || context.IDENT()[0].GetText() == "false")
                        return context;
                    else if (context.IDENT()[0].GetText() == "null") {
                        return context;
                    }
                    else if (okcond)
                    {
                        return null;
                    }
                    else
                    {
                        _errors.Add("ERROR EN LINEA " + "{" + context.Start.Line + ":" + context.Start.Column + "}. " + "ERROR DE ALCANCES: La variable o constante no ha sido nombrada con anterioridad: " + context.IDENT()[0].GetText());
                        return null;
                    }
                    
                }


            }
            if (returndesigasing) {
                return context.IDENT()[0].GetText();
            }
            return context;
        }

        public override object VisitRelop(Parser1.RelopContext context)
        {
            return context;
        }

        public override object VisitAddop(Parser1.AddopContext context)
        {
            return context;
        }

        public override object VisitMulop(Parser1.MulopContext context)
        {
            return context;
        }

        public object buscarTabM(string b)
        {
            foreach (var tab in TableMethodsP.Table)
            {
                if (tab.GetType() == typeof(TablaSimbolos.Len)&& b=="LEN")
                {
                    return tab;
                }if (tab.GetType() == typeof(TablaSimbolos.chr)&& b=="CHR")
                {
                    return tab;
                }
                if (tab.GetType() == typeof(TablaSimbolos.ord) && b == "ORD")
                {
                    return tab;
                }
            }
            return null;
        }

        public string getList() {
            StringBuilder err=new StringBuilder();
            if (_errors.Count ==0) {
                return "\nNO HAY ERRORES CONTEXTUALES!";
            }

            err.Append("\nERRORES CONTEXTUALES:\n");
            foreach (string e in _errors) {
                err.Append(e+"\n");
                
            }

            return err.ToString();

        }
    }
}