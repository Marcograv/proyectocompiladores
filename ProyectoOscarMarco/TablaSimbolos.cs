﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;

namespace ProyectoOscarMarco
{
    public class TablaSimbolos
    {
        public ArrayList<Object> Table;
        public int Nivel;
        
        
        
        public TablaSimbolos() {
            this.Table = new ArrayList<object>();
            this.Nivel=-1;
        }
        public void Insertar(object item)
        {
            Table.Add(item);
        }
        public object buscar(String nombre)
        {
            //debe buscarse en otro orden... de atrás para adelante
            object temp=null;

            foreach (object id in Table)
            {
                if (id.GetType()==typeof(Clase))
                {
                    var idn = (Clase) id;
                    if (idn.tok.Text.Equals(nombre))
                    {
                        temp = (id);
                        break;
                    }
                        
                }

                else if (id.GetType()==typeof(Var))
                {
                    var idn = (Var) id;
                    if (idn.tok.Text.Equals(nombre))
                    {
                        temp = (id);
                        break;
                    }
                }else if (id.GetType()==typeof(Const))
                {
                    var idn = (Const) id;
                    if (idn.tok.Text.Equals(nombre))
                    {
                        temp = (id);
                        break;
                    }
                }else if (id.GetType()==typeof(method))
                {
                    var idn = (method) id;
                    if (idn.tok.Text.Equals(nombre))
                    {
                        temp = (id);
                        break;
                    }
                }
                
            }

            return temp;
        }
        public void openScope(){
            Nivel++;
        }
        public void closeScope(){

            for (int i = 0; i < Table.Count; i++)
            {
                if (Table[i].GetType()==typeof(Clase))
                {
                    if (((Clase)Table[i]).nivel == Nivel)
                    {
                        Table.Remove(Table[i]);
                        i--;
                    }
                }

                else if (Table[i].GetType()==typeof(Var))
                {
                    if (((Var)Table[i]).nivel == Nivel)
                    {
                        Table.Remove(Table[i]);
                        i--;
                    }
                }else if (Table[i].GetType()==typeof(Const))
                {
                    if (((Const)Table[i]).nivel == Nivel)
                    {
                        Table.Remove(Table[i]);
                        i--;
                    }
                }else if (Table[i].GetType()==typeof(method))
                {
                    if (((method)Table[i]).nivel == Nivel)
                    {

                        Table.Remove(Table[i]);
                        i--;
                    }
                }
            }

            Nivel--;
        }
       
        
        //CLASES
        public class Const{
                public IToken tok;
                public string type;
                public int nivel;
                public object valor;
        
                public Const(IToken t,int nivelact, object val, string typ){
                    tok = t;
                    type = typ;
                    nivel=nivelact;
                    valor = val;
                }
            }
        
           public class Var
            {
                public IToken tok;
                public string type;
                public int nivel;
                public object valor;
                public Var(IToken t,int nivelact, string typ){
                    tok = t;
                    type = typ ;
                    nivel=nivelact;
                }
                
                public void setValue(object v){
                    valor = v;
                }
                
            }
        
           public class Clase
            {
                public IToken tok;
                string type;
                public int nivel;
                public ArrayList<Var> vars;
        
                public Clase(IToken t,int nivelact){
                    tok = t;
                    type = "Class";
                    nivel=nivelact;
                    vars= new ArrayList<Var>();
                }

                public void addvars(Var v)
                {
                    vars.Add(v);
                }
            public bool buscvars(string v) {
                foreach (var local in vars)
                {
                    if (local.tok.Text.Equals(v))
                    {
                        return true;
                    }
                }

                return false;
            }
            public Var returnv(string v) {
                foreach (var local in vars)
                {
                    if (local.tok.Text.Equals(v))
                    {
                        return local;
                    }
                }

                return null;
            }

            }
        
            public class method
            {
                public IToken tok;
                public string type;
                public int nivel;
                public ArrayList<String> vars;
                public method(IToken t, int nivelact, string ty)
                {
                    tok = t;
                    nivel = nivelact;
                    type = ty;
        
                }

                public void setvars(ArrayList<String> arr)
                {
                    this.vars = arr;
                }
            }
        public class Len
        {
            public Len()
            {

            }

            public int SizeArr(Var obj)
            {
                return 1;
            }
        }
        public class chr
        {
            public chr()
            {
            }

            public void convertir(Var obj)
            {
                if (obj.type == "CHAR" || obj.type == "INT")
                {
                    obj.type = "CHAR";
                }
                else
                {
                    Console.WriteLine("ERROR DE TIPOS: intenta convertir un tipo: " + obj.type + "a tipo CHAR");
                }
            }
        }

        public class ord
        {

            public ord()
            {
            }

            public void convert(Var obj)
            {

                if (obj.type == "CHAR")
                {
                    if (!isAlpha((string)obj.valor))
                    {
                        obj.type = "INT";
                    }
                    else
                    {
                        Console.WriteLine("ERROR DE TIPOS: intenta convertir un tipo: " + obj.type + "a tipo INT y este contiene letras");
                    }

                }
                else
                {
                    Console.WriteLine("ERROR DE TIPOS: intenta convertir un tipo: " + obj.type + "a tipo INT");
                }

            }
            public bool isAlpha(string name)
            {
                return Regex.IsMatch(name, ("[a-zA-Z]"));
            }
        }

        public class Null {

            public Null()
            {

            }
            public void asig(object ob){
                if (ob.GetType() == typeof(TablaSimbolos.Var))
                {
                    TablaSimbolos.Var obn = (TablaSimbolos.Var)ob;
                    obn.type = "NULL";
                }           

            }


        }



    }
    

    
}