﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;


namespace ProyectoOscarMarco
{
    static class Program
    {

        public static LexerP lexer = null;
        public static Parser1 parser = null;
        public static ITokenStream tokens = null;
        public static ErrorListener errores = null;
        public static Form FormMain = null;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FormMain = new Form1();
            
            Application.Run(FormMain);

        }

        public static String GetErroresMinics(String texto,Form1 form1)
        {
            ICharStream stream = CharStreams.fromstring(texto);
            lexer = new LexerP(stream);
            tokens = new CommonTokenStream(lexer);
            parser = new Parser1(tokens);
            errores = new ErrorListener();
            parser.RemoveErrorListeners();
            parser.AddErrorListener(errores);
            IParseTree tree =parser.program();
            form1.Lista = errores.listalineas;
            if (errores.HasErrors() == false) { 
                
                //AContext contx= new AContext();
                //contx.Visit(tree);
                CodeGen contx = new CodeGen();
                contx.Codigo(tree);
                //CodeGenR contx2= new CodeGenR();
                //contx2.Codigo(tree);
                string lista_errcontx = contx.getList();
                Console.WriteLine(errores.ToString() + lista_errcontx);
                return errores.ToString() + lista_errcontx;

            }
            return errores.ToString();

        }
        public static String GetErroresReflect(String texto,Form1 form1)
                 {
                     ICharStream stream = CharStreams.fromstring(texto);
                     lexer = new LexerP(stream);
                     tokens = new CommonTokenStream(lexer);
                     parser = new Parser1(tokens);
                     errores = new ErrorListener();
                     parser.RemoveErrorListeners();
                     parser.AddErrorListener(errores);
                     IParseTree tree =parser.program();
                     form1.Lista = errores.listalineas;
                     if (errores.HasErrors() == false) { 
                         
                         //AContext contx= new AContext();
                         //contx.Visit(tree);
                         //CodeGen contx = new CodeGen();
                         //contx.Codigo(tree);
                         CodeGenR contx2= new CodeGenR();
                         contx2.Codigo(tree);
                         //string lista_errcontx = contx.getList();
                         //Console.WriteLine(errores.ToString() + lista_errcontx);
                         return errores.ToString();
         
                     }
                     return errores.ToString();
         
                 }


    }

}
