parser grammar Parser1;

options {
    tokenVocab = LexerP;
}

program : classdecl (vardecl|constdecl|classdecl)*?BRAKETIZQ (methodecl)*? BRAKETDER                                                        #programAST;
constdecl: CONST type IDENT ASIGNA (NUMBER|CHARCONST|BOOLEAN|STRING) PYCOMA                                                                        #constAST;
vardecl  : type IDENT (COMA IDENT)*? PYCOMA                                                                                                 #vardeclAST;
classdecl : CLASS IDENT (BRAKETIZQ(vardecl)*? BRAKETDER) *?                                                                                 #classdeclAST;
methodecl: (type|VOID)IDENT PIZQ(formpars)?PDER (vardecl)*? block                                                                           #methodeclAST;              
formpars  : type IDENT (COMA type IDENT)*?                                                                                                  #formparsAST;                                         
type  :  CHAR    (CORCHETEIZQ (IDENT|NUMBER)? CORCHETEDER)?                                                                                 #chartypeAST
        |INT    (CORCHETEIZQ (IDENT|NUMBER)? CORCHETEDER)?                                                                                  #inttypeAST
        |FLOAT                                                                                                                              #floattypeAST
        |BOOL                                                                                                                               #booltypeAST
        |IDENT  (CORCHETEIZQ (IDENT|NUMBER)? CORCHETEDER)?                                                                                  #identtypeAST
        |STRINGV                                                                                                                            #stringtypeAST;                   
statement : designator (ASIGNA expr|PIZQ (actpars)? PDER|PLUS|RESTA) PYCOMA                                                                 #designatorSTMAST
            |IF PIZQ condition PDER statement (ELSE statement)?                                                                             #ifSTMAST
            |FOR PIZQ expr PYCOMA (condition) PYCOMA (statement)? PDER statement                                                            #forSTMAST
            |WHILE PIZQ condition PDER statement                                                                                            #whileSTMAST
            |BREAK PYCOMA                                                                                                                   #breakSTMAST
            |RETURN (expr)? PYCOMA                                                                                                          #returnSTMAST
            |READ PIZQ designator PDER PYCOMA                                                                                               #readSTMAST
            |WRITE PIZQ expr (COMA NUMBER)? PDER PYCOMA                                                                                     #writeSTMAST
            |block                                                                                                                          #blockSTMAST
            |PYCOMA                                                                                                                         #pycomaSTMAST
            |switchcase                                                                                                                     #switchcaseSTAST;
block : BRAKETIZQ (vardecl|constdecl|statement|methodecl)* BRAKETDER                                                                        #blockAST;                     
actpars : expr? (COMA expr)*?                                                                                                                #actparsAST;
condition : condterm (OR condterm)*                                                                                                         #conditiontermAST;
condterm  : condfact (AND condfact)*                                                                                                        #condtermAST;
condfact  : expr relop expr                                                                                                                 #condfactAST;
expr: (RES)? term (addop term)*?                                                                                                            #exprAST;
term: factor (mulop factor)*?                                                                                                               #termAST;
factor: designator (PIZQ(actpars)PDER)*                                                                                                     #designatorFACAST
        |NUMBER                                                                                                                             #numFACAST
        |CHARCONST                                                                                                                          #charFACAST
        |BOOLEAN                                                                                                                            #booleanFACAST
        |NEW (type|IDENT)                                                                                                                   #newFACAST
        |STRING                                                                                                                             #stringFACAST
        |FLOATNUMBER                                                                                                                        #floatFACAST
        |PIZQ expr PDER                                                                                                                     #exprFACAST;
designator : IDENT (PUNTO IDENT |CORCHETEIZQ expr CORCHETEDER)*                                                                             #designatorAST;
relop : IGUAL|DIF|MENOR|MENORIGUAL|MAYOR|MAYORIGUAL                                                                                         #relopAST;
addop : SUM|RES                                                                                                                             #addopAST;
mulop: MULT|DIV|DIVMOD                                                                                                                      #mulopAST;
switchcase: SWITCH (PIZQ IDENT PDER) BRAKETIZQ (CASE (NUMBER|CHARCONST) DOSPUNTOS (statement))* (DEFAULT DOSPUNTOS (statement)) BRAKETDER   #switchAST;
