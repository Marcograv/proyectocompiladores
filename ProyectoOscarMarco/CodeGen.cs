﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using Antlr4.Runtime.Tree;
//REALIZADO POR MARCO GRANADOS!
namespace ProyectoOscarMarco
{
    public class CodeGen: Parser1BaseVisitor<object>
    {
        private ArrayList code;
        private int Nivel;
        private int Line;
        private ArrayList locals;
        private ArrayList globals;

        public CodeGen()
        {
            this.code = new ArrayList();
            this.locals = new ArrayList();
            this.globals = new ArrayList();
            this.Nivel = 0;
            this.Line = 0;
        }

        public void Codigo(IParseTree ctx)
        {
            this.Visit(ctx);
            foreach (var elemento in code)
            {
                
                Console.WriteLine(elemento);
            }
            code.Add(Line+" END");

            string archivo =Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PruebaMiniCS.txt");
            string cadena = "";
            foreach (var cod in code)
            {
                cadena += cod + "\n";
            }
            using (StreamWriter sw = File.CreateText(archivo))
            {
                sw.WriteLine(cadena);
            }
        }
        public override object VisitProgramAST(Parser1.ProgramASTContext context)
        {
            //GENERAR CODIGO PARA VARIABLES
            
            foreach (var varde in context.vardecl())
            {
                Visit(varde);
            }
            //GENERAR CODIGO PARA METODOS
            Nivel++;
            foreach (var met in context.methodecl())
            {
                Visit(met);
            }
            return null;
        }

        public override object VisitConstAST(Parser1.ConstASTContext context)
        {
            return null;
        }

        public override object VisitVardeclAST(Parser1.VardeclASTContext context)
        {
            
            string tipo = (string) Visit(context.type());
            if (tipo.Equals("INT"))
            {
                if (Nivel == 0)
                {
                    code.Add(Line+" PUSH_GLOBAL_I "+ context.IDENT()[0].GetText());
                    Line++;
                    globals.Add(context.IDENT()[0].GetText());
                }
                else
                {
                    code.Add(Line+" PUSH_LOCAL_I "+ context.IDENT()[0].GetText());
                    Line++;
                    locals.Add(context.IDENT()[0].GetText());
                }
                
            }else if (tipo.Equals("CHAR"))
            {
                if (Nivel == 0)
                {
                    code.Add(Line+" PUSH_GLOBAL_C "+ context.IDENT()[0].GetText());
                    Line++;
                    globals.Add(context.IDENT()[0].GetText());
                }
                else
                {
                    code.Add(Line+" PUSH_LOCAL_C "+ context.IDENT()[0].GetText());
                    Line++;
                    locals.Add(context.IDENT()[0].GetText());
                }
            }else if (tipo.Equals("STRING"))
            {
                if (Nivel == 0)
                {
                    code.Add(Line+" PUSH_GLOBAL_C "+ context.IDENT()[0].GetText());
                    Line++;
                    globals.Add(context.IDENT()[0].GetText());
                }
                else
                {
                    code.Add(Line+" PUSH_LOCAL_C "+ context.IDENT()[0].GetText());
                    Line++;
                    locals.Add(context.IDENT()[0].GetText());
                }
            }else if (tipo.Equals("FLOAT"))
            {
                if (Nivel == 0)
                {
                    code.Add(Line+" PUSH_GLOBAL_I "+ context.IDENT()[0].GetText());
                    Line++;
                    globals.Add(context.IDENT()[0].GetText());
                }
                else
                {
                    code.Add(Line+" PUSH_LOCAL_I "+ context.IDENT()[0].GetText());
                    Line++;
                    locals.Add(context.IDENT()[0].GetText());
                }
            }else if (tipo.Equals("BOOLEAN"))
            {
                if (Nivel == 0)
                {
                    code.Add(Line+" PUSH_GLOBAL_C "+ context.IDENT()[0].GetText());
                    Line++;
                    globals.Add(context.IDENT()[0].GetText());
                }
                else
                {
                    code.Add(Line+" PUSH_LOCAL_C "+ context.IDENT()[0].GetText());
                    Line++;
                    locals.Add(context.IDENT()[0].GetText());
                }
            }
            else
            {
                if (Nivel == 0)
                {
                    code.Add(Line+" PUSH_GLOBAL_C "+ context.IDENT()[0].GetText());
                    Line++;
                    globals.Add(context.IDENT()[0].GetText());
                }
                else
                {
                    code.Add(Line+" PUSH_LOCAL_C "+ context.IDENT()[0].GetText());
                    Line++;
                    locals.Add(context.IDENT()[0].GetText());
                }
            }

            return null;
        }

        public override object VisitClassdeclAST(Parser1.ClassdeclASTContext context)
        {
            return base.VisitClassdeclAST(context);
        }

        public override object VisitMethodeclAST(Parser1.MethodeclASTContext context)
        {
            code.Add(Line +" DEF "+context.IDENT().GetText());
            Line++;
            if (context.formpars()!=null)
            {
                Visit(context.formpars());
            }
            
            foreach (var vr in context.vardecl())
            {
                Visit(vr);
            }

            Visit(context.block());
            locals.Clear();
            return null;
        }

        public override object VisitFormparsAST(Parser1.FormparsASTContext context)
        {
            for (int i = 0; i < context.type().Length; i++)
            {
                string tipo = (string)Visit(context.type()[i]);
                if (tipo.Equals("INT"))
                {
                    code.Add(Line+ " PUSH_LOCAL_I "+ context.IDENT()[i].GetText());
                    Line++;
                    locals.Add(context.IDENT()[i].GetText());
                }else if (tipo.Equals("CHAR"))
                {
                    code.Add(Line+ " PUSH_LOCAL_C "+ context.IDENT()[i].GetText());
                    Line++;
                    locals.Add(context.IDENT()[0].GetText());
                }else if (tipo.Equals("STRING"))
                {
                    code.Add(Line+" PUSH_LOCAL_C "+ context.IDENT()[i].GetText());
                    Line++;
                    locals.Add(context.IDENT()[i].GetText());
                
                }else if (tipo.Equals("FLOAT"))
                { 
                    code.Add(Line+" PUSH_LOCAL_I "+ context.IDENT()[i].GetText());
                    Line++;
                    locals.Add(context.IDENT()[i].GetText());
                
                }else if (tipo.Equals("BOOLEAN"))
                {
                    code.Add(Line+" PUSH_LOCAL_C "+ context.IDENT()[i].GetText());
                    Line++;
                    locals.Add(context.IDENT()[i].GetText());
                }else
                {
                    code.Add(Line+" PUSH_LOCAL_C "+ context.IDENT()[i].GetText());
                    Line++;
                    locals.Add(context.IDENT()[i].GetText());
                }
                
            }
            return null;
        }

        public override object VisitChartypeAST(Parser1.ChartypeASTContext context)
        {
            return "CHAR";
        }

        public override object VisitInttypeAST(Parser1.InttypeASTContext context)
        {
            return "INT";
        }

        public override object VisitFloattypeAST(Parser1.FloattypeASTContext context)
        {
            return "FLOAT";
        }

        public override object VisitBooltypeAST(Parser1.BooltypeASTContext context)
        {
            return "BOOLEAN";
        }

        public override object VisitIdenttypeAST(Parser1.IdenttypeASTContext context)
        {
            return context.GetText();
        }

        public override object VisitStringtypeAST(Parser1.StringtypeASTContext context)
        {
            return "STRING";
        }    

        public override object VisitDesignatorSTMAST(Parser1.DesignatorSTMASTContext context)
        {
            if (context.PLUS()!=null)
            {
                if (buscarLocals((string)Visit(context.designator())))
                {
                    code.Add(Line + " LOAD_FAST " + (string) Visit(context.designator()));
                    Line++;
                }
                else
                {
                    code.Add(Line + " LOAD_GLOBAL " + (string) Visit(context.designator()));
                    Line++;
                }

                code.Add(Line + " LOAD_CONST 1");
                Line++;
                code.Add(Line + " BINARY_ADD");
                Line++;
                if (buscarLocals((string)Visit(context.designator())))
                {
                    code.Add(Line + " STORE_FAST " + (string) Visit(context.designator()));
                    Line++;
                }
                else
                {
                    code.Add(Line + " STORE_GLOBAL " + (string) Visit(context.designator()));
                    Line++;
                }
            }else if (context.RESTA()!=null)
            {
                if (buscarLocals((string)Visit(context.designator())))
                {
                    code.Add(Line + " LOAD_FAST " + (string) Visit(context.designator()));
                    Line++;
                }
                else
                {
                    code.Add(Line + " LOAD_GLOBAL " + (string) Visit(context.designator()));
                    Line++;
                }

                code.Add(Line + " LOAD_CONST 1");
                Line++;
                code.Add(Line + " BINARY_SUBSTRACT");
                Line++;
                if (buscarLocals((string)Visit(context.designator())))
                {
                    code.Add(Line + " STORE_FAST " + (string) Visit(context.designator()));
                    Line++;
                }
                else
                {
                    code.Add(Line + " STORE_GLOBAL " + (string) Visit(context.designator()));
                    Line++;
                }
            }
            else if (context.ASIGNA()!=null)
            {
                string nombre = (string)Visit(context.designator());
                var exp = Visit(context.expr());
                if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                {
                    if (((string)code[code.Count-1]).Contains("BINARY"))
                    {
                        if (buscarLocals(nombre))
                        {
                            code.Add(Line + " STORE_FAST "+ nombre);
                            Line++; 
                        }
                        else
                        {
                            code.Add((Line + " STORE_GLOBAL " + nombre));
                            Line++;
                        }
                    }
                    else
                    {
                       code.Add(Line + " LOAD_CONST "+((Parser1.NumFACASTContext)exp).GetText());
                       Line++;
                       if (buscarLocals(nombre))
                       {
                           code.Add(Line + " STORE_FAST "+ nombre);
                           Line++; 
                       }
                       else
                       {
                           code.Add((Line + " STORE_GLOBAL " + nombre));
                           Line++;
                       }
                       
                    }
                    
                }

                else if (exp.GetType()==typeof(Parser1.DesignatorFACASTContext))
                {

                    if (((string)code[code.Count-1]).Contains("BINARY"))
                    {
                        if (buscarLocals(nombre))
                        {
                            code.Add(Line + " STORE_FAST "+ nombre);
                            Line++; 
                        }
                        else
                        {
                            code.Add((Line + " STORE_GLOBAL " + nombre));
                            Line++;
                        }
                    }
                    else
                    {
                        if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)exp).designator())))
                        {
                            code.Add(Line + " LOAD_FAST "+Visit(((Parser1.DesignatorFACASTContext)exp).designator()));
                            Line++;
                        }
                        else
                        {
                            if (!((string)(code[code.Count-1])).Contains("CALL_FUNCTION"))
                            {
                                code.Add(Line + " LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)exp).designator()));
                                Line++;
                            }
                        }
                        if (buscarLocals(nombre))
                        {
                            code.Add(Line + " STORE_FAST "+ nombre);
                            Line++; 
                        }
                        else
                        {
                            code.Add((Line + " STORE_GLOBAL " + nombre));
                            Line++;
                        }
                    }
                    
                    
                }else if (exp.GetType() == typeof(Parser1.CharFACASTContext))
                {
                    if (((string) code[code.Count - 1]).Contains("BINARY"))
                    {
                        if (buscarLocals(nombre))
                        {
                            code.Add(Line + " STORE_FAST "+ nombre);
                            Line++; 
                        }
                        else
                        {
                            code.Add((Line + " STORE_GLOBAL " + nombre));
                            Line++;
                        }
                    }
                    else
                    {
                        code.Add(Line + " LOAD_CONST " + ((Parser1.CharFACASTContext) exp).GetText());
                        Line++;
                        if (buscarLocals(nombre))
                        {
                            code.Add(Line + " STORE_FAST "+ nombre);
                            Line++; 
                        }
                        else
                        {
                            code.Add((Line + " STORE_GLOBAL " + nombre));
                            Line++;
                        }
                    }
                }



            }
            else
            {
               var nom = (string) Visit(context.designator());
               if (context.PDER()!=null)
               {
                   if (context.actpars()!=null)
                   {
                       var acp = ((Parser1.ActparsASTContext)Visit(context.actpars())).expr().Length;
                       code.Add(Line + " LOAD_GLOBAL " + nom);
                       Line++;
                       code.Add(Line + " CALL_FUNCTION " + acp);
                       Line++;
                   }
                   else
                   {
                       code.Add(Line + " LOAD_GLOBAL " + nom);
                       Line++;
                       code.Add(Line + " CALL_FUNCTION " + 0);
                       Line++;
                   }

               }
            }

            return null;
        }

        public override object VisitIfSTMAST(Parser1.IfSTMASTContext context)
        {
            Visit(context.condition());
            if (context.ELSE()==null)
            {
                code.Add(Line+ " JUMP_IF_FALSE ");
                var lineant = Line;
                Line++;
                var indice = code.Count-1;
                Visit(context.statement()[0]);
                code.Add(Line + " JUMP_ABSOLUTE ");
                var lineantab = Line;
                Line++;
                var indiceab = code.Count-1;
                code[indice] = lineant + " JUMP_IF_FALSE " + Line;
                code[indiceab] = lineantab + " JUMP_ABSOLUTE " + Line;
            }
            if (context.ELSE()!=null)
            {
                code.Add(Line+" JUMP_IF_FALSE");
                var lineant = Line;
                Line++;
                var indice = code.Count-1;
                Visit(context.statement()[0]);
                code.Add(Line + " JUMP_ABSOLUTE ");
                var lineantab = Line;
                Line++;
                var indiceab = code.Count-1;
                code[indice] = lineant + " JUMP_IF_FALSE " + Line;
                Visit(context.statement()[1]);
                code[indiceab] = lineantab + " JUMP_ABSOLUTE " + Line;
            }
            
            return null;
        }

        public override object VisitForSTMAST(Parser1.ForSTMASTContext context)
        {
            Visit(context.expr());
            var condinicial = Line;
            Visit(context.condition());
            code.Add(Line+" JUMP_IF_FALSE ");
            var lineant = Line;
            Line++;
            var indice = code.Count-1;
            var linebreak = Line;
            Visit(context.statement()[1]);
            Visit(context.statement()[0]);
            code.Add(Line + " JUMP_ABSOLUTE " + condinicial);
            Line++;
            code[indice] = lineant + " JUMP_IF_FALSE " + Line;
            
            for (int i = linebreak; i < code.Count; i++)
            {
                if (((string)code[i]).Contains("BREAK!!"))
                {
                    code[i] = i + " JUMP_ABSOLUTE " + Line;
                }
            }
            
            return null;
        }

        public override object VisitWhileSTMAST(Parser1.WhileSTMASTContext context)
        {
            var condinicial = Line;
            Visit(context.condition());
            code.Add(Line+" JUMP_IF_FALSE");
            var lineant = Line;
            Line++;
            var indice = code.Count-1;
            var linebreak = Line;
            Visit(context.statement());
            
            code.Add(Line + " JUMP_ABSOLUTE " + condinicial);
            Line++;
            code[indice] = lineant + " JUMP_IF_FALSE " + Line;
            for (int i = linebreak; i < code.Count; i++)
            {
                if (((string)code[i]).Contains("BREAK!!"))
                {
                    code[i] = i + " JUMP_ABSOLUTE " + Line;
                }
            }
            return null;
        }

        public override object VisitBreakSTMAST(Parser1.BreakSTMASTContext context)
        {
            code.Add(Line + " BREAK!!");
            Line++;
            return null;
        }

        public override object VisitReturnSTMAST(Parser1.ReturnSTMASTContext context)
        {
            if (context.expr()==null)
            {
                code.Add(Line+" RETURN");
                Line++;
                return null;
            }
            var exp = Visit(context.expr());
            if (exp.GetType()==typeof(Parser1.DesignatorFACASTContext))
            {
                if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)exp).designator())))
                {
                    code.Add(Line+" LOAD_FAST "+Visit(((Parser1.DesignatorFACASTContext)exp).designator()));
                    Line++;
                }
                else
                {
                    code.Add(Line+" LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)exp).designator()));
                    Line++;
                }

                code.Add(Line+" RETURN_VALUE");
                Line++;
                
            }else if (exp.GetType()==typeof(Parser1.NumFACASTContext))
            {
                code.Add(Line+" LOAD_CONST "+((Parser1.NumFACASTContext)exp).GetText());
                Line++;
                code.Add(Line+" RETURN_VALUE");
                Line++;
                
            }else if (exp.GetType()==typeof(Parser1.CharFACASTContext))
            {
                code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)exp).GetText());
                Line++;
                code.Add(Line+" RETURN_VALUE");
                Line++;    
            }
            return null;
        }

        public override object VisitReadSTMAST(Parser1.ReadSTMASTContext context)
        {
            code.Add(Line + " LOAD_GLOBAL read");
            Line++;
            code.Add(Line + " CALL_FUNCTION 1");
            Line++;
            var nom = (string)Visit(context.designator());
            if (buscarLocals(nom))
            {
                code.Add(Line + " STORE_FAST " + nom);
                Line++;
            }
            else
            {
                code.Add(Line+" STORE_GLOBAL "+nom);
                Line++;
            }

            return null;
        }

        public override object VisitWriteSTMAST(Parser1.WriteSTMASTContext context)
        {
            var exp = Visit(context.expr());
            if (exp.GetType()==typeof(Parser1.DesignatorFACASTContext))
            {
                if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)exp).designator())))
                {
                   /* if (((string)code[code.Count-2]).Contains("LOAD_GLOBAL"))
                    {
                        
                    }
                    else
                    {*/
                        code.Add(Line + " LOAD_FAST " + Visit(((Parser1.DesignatorFACASTContext) exp).designator()));
                        Line++;
                    //}
                }
                else
                {
                    /*if (((string)code[code.Count-2]).Contains("LOAD_GLOBAL"))
                    {
                        
                    }
                    else
                    {*/
                        code.Add(Line+" LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)exp).designator()));
                        Line++;
                    //}

                    
                }

                code.Add(Line+" LOAD_GLOBAL write");
                Line++;
                code.Add(Line + " CALL_FUNCTION 1");
                Line++;
            }else if (exp.GetType()==typeof(Parser1.NumFACASTContext))
            {
                code.Add(Line+" LOAD_CONST "+((Parser1.NumFACASTContext)exp).GetText());
                Line++;
                code.Add(Line+" LOAD_GLOBAL write");
                Line++;
                code.Add(Line + " CALL_FUNCTION 1");
                Line++;
            }else if (exp.GetType()==typeof(Parser1.CharFACASTContext))
            {
                code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)exp).GetText());
                Line++;
                code.Add(Line+" LOAD_GLOBAL write");
                Line++;
                code.Add(Line + " CALL_FUNCTION 1");
                Line++;
            }else if (exp.GetType()==typeof(Parser1.FloatFACASTContext))
            {
                code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)exp).GetText());
                Line++;
                code.Add(Line+" LOAD_GLOBAL write");
                Line++;
                code.Add(Line + " CALL_FUNCTION 1");
                Line++;
            }else if (exp.GetType()==typeof(Parser1.StringFACASTContext))
            {
                code.Add(Line+" LOAD_CONST "+((Parser1.StringFACASTContext)exp).GetText());
                Line++;
                code.Add(Line+" LOAD_GLOBAL write");
                Line++;
                code.Add(Line + " CALL_FUNCTION 1");
                Line++;
            }else if (exp.GetType()==typeof(Parser1.BooleanFACASTContext))
            {
                code.Add(Line+" LOAD_CONST "+((Parser1.BooleanFACASTContext)exp).GetText());
                Line++;
                code.Add(Line+" LOAD_GLOBAL write");
                Line++;
                code.Add(Line + " CALL_FUNCTION 1");
                Line++;
            }
            return null;
        }

        public override object VisitBlockSTMAST(Parser1.BlockSTMASTContext context)
        {
            Visit(context.block());
            return null;
        }

        public override object VisitPycomaSTMAST(Parser1.PycomaSTMASTContext context)
        {
            return base.VisitPycomaSTMAST(context);
        }

        public override object VisitSwitchcaseSTAST(Parser1.SwitchcaseSTASTContext context)
        {
            //FALTA!!
            //JUMP_TRUE EN STATEMENT Y SI EN ESTE ENTRA JUMP_ABSOLUTE AFUERA DEL SWITCH
            //EN EL CASO DE QUE NINGUNO SE ACCIONE ENTONCES ENTRARA AL DEFAULT (NO HACE FALTA COMPROBAR NADA!!!) Y ESTE DARA UN JUMP_ABSOLUTE AFUERA DEL SWITCH!(O SE PUEDE OBVIAR)
            Visit(context.switchcase());
            return null;
        }

        public override object VisitBlockAST(Parser1.BlockASTContext context)
        {
            foreach (var vardeclContext in context.vardecl())
            {
                Visit(vardeclContext);
            }

            foreach (var stat in context.statement())
            {
                Visit(stat);
            }
            return null;
        }

        public override object VisitActparsAST(Parser1.ActparsASTContext context)
        {
            foreach (var exprContext in context.expr())
            {
                var exp= Visit(exprContext);
                if (exp!=null)
                {
                    if (exp.GetType()==typeof(Parser1.DesignatorFACASTContext))
                    {
                        if (buscarLocals(((Parser1.DesignatorFACASTContext)exp).GetText()))
                        {
                            code.Add(Line + " LOAD_FAST " + ((Parser1.DesignatorFACASTContext) exp).GetText());
                            Line++;
                        }
                        else
                        {
                            code.Add(Line + " LOAD_GLOBAL " + ((Parser1.DesignatorFACASTContext) exp).GetText());
                            Line++; 
                        }
                    }else if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                    {
                        code.Add(Line + " LOAD_CONST " + ((Parser1.NumFACASTContext) exp).GetText());
                        Line++;
                    }else if (exp.GetType()==typeof(Parser1.CharFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)exp).GetText());
                        Line++;
                    }else if (exp.GetType()==typeof(Parser1.StringFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.StringFACASTContext)exp).GetText());
                        Line++;
                    }else if (exp.GetType()==typeof(Parser1.FloatFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)exp).GetText());
                        Line++;
                    }
                }
                
            }
            return context;
        }
        
        public override object VisitConditiontermAST(Parser1.ConditiontermASTContext context)
        {
            if (context.condterm().Length==1)
            {
                Visit(context.condterm()[0]);
            }
            else
            {
                var total = 0;
                for (int i = 0; i < context.condterm().Length; i++)
                {
                    Visit(context.condterm()[i]);
                    var a = (i+1) % 2;
                    if (a==0&&i!=0&&!((string)code[code.Count-1]).Contains("BINARY"))
                    {
                        code.Add(Line + " BINARY_OR");
                        Line++;
                    }
                    total = a;
                }

                if (total!=0 && !((string)code[code.Count-1]).Contains("BINARY"))
                {
                    code.Add(Line + " BINARY_OR");
                    Line++;
                }
            }

            
            return null;
        }

        public override object VisitCondtermAST(Parser1.CondtermASTContext context)
        {
            if (context.condfact().Length==1)
            {
                Visit(context.condfact()[0]);
            }
            else
            {
                var total = 0;
                for (int i = 0; i < context.condfact().Length; i++)
                {
                    Visit(context.condfact()[i]);
                    var a = (i+1) % 2;
                    if (a==0&&i!=0)
                    {
                        code.Add(Line + " BINARY_AND");
                        Line++;
                    }
                    total = a;
                }
                if (total!=0 && !((string)code[code.Count-1]).Contains("BINARY"))
                {
                    code.Add(Line + " BINARY_AND");
                    Line++;
                }
            }

            
            return null;
        }

        public override object VisitCondfactAST(Parser1.CondfactASTContext context)
        {
            for (int i = 0; i < context.expr().Length; i++)
            {
                var exp = Visit(context.expr()[i]);
                if (exp.GetType()==typeof(Parser1.DesignatorFACASTContext))
                {
                    if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)exp).designator())))
                    {
                        code.Add(Line+" LOAD_FAST "+Visit(((Parser1.DesignatorFACASTContext)exp).designator()));
                        Line++;
                    }
                    else
                    {
                        code.Add(Line+" LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)exp).designator()));
                        Line++;
                    }
                }else if (exp.GetType()==typeof(Parser1.NumFACASTContext))
                {
                    code.Add(Line+" LOAD_CONST "+((Parser1.NumFACASTContext)exp).GetText());
                    Line++;
                }else if (exp.GetType()==typeof(Parser1.CharFACASTContext))
                {
                    code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)exp).GetText());
                    Line++;
                }else if (exp.GetType()==typeof(Parser1.FloatFACASTContext))
                {
                    code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)exp).GetText());
                    Line++;
                }else if (exp.GetType()==typeof(Parser1.StringFACASTContext))
                {
                    code.Add(Line+" LOAD_CONST "+((Parser1.StringFACASTContext)exp).GetText());
                    Line++;
                }else if (exp.GetType()==typeof(Parser1.BooleanFACASTContext))
                {
                    code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)exp).GetText());
                    Line++;
                }
            }

            code.Add(Line+" COMPARE_OP "+Visit(context.relop()));
            Line++;
            
            return context;
        }

        public override object VisitExprAST(Parser1.ExprASTContext context)
        {
            //REVISAR EXPR (HACER PRUEBAS)
            if (context.addop().Length>=1)
            {
                var t1 = Visit(context.term()[0]);
                var t2 = Visit(context.term()[1]);
                if (t1.GetType()==typeof(Parser1.DesignatorFACASTContext))
                {
                    if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)t1).designator())))
                    {
                        code.Add(Line+" LOAD_FAST "+Visit(((Parser1.DesignatorFACASTContext)t1).designator()));
                        Line++;
                    }
                    else
                    {
                        code.Add(Line+" LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)t1).designator()));
                        Line++;
                    }
                    if (t2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                    {
                        if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)t2).designator())))
                        {
                            code.Add(Line+" LOAD_FAST "+Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            Line++;
                        }
                        else
                        {
                            code.Add(Line+" LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            Line++;
                        }

                    }else if (t2.GetType()==typeof(Parser1.NumFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.NumFACASTContext)t2).GetText());
                        Line++;
                    }else if (t1.GetType()==typeof(Parser1.CharFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)t2).GetText());
                        Line++;
                    }else if (t1.GetType()==typeof(Parser1.FloatFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)t2).GetText());
                        Line++;
                    }
                }else if (t1.GetType()==typeof(Parser1.NumFACASTContext))
                {
                    code.Add(Line+" LOAD_CONST "+((Parser1.NumFACASTContext)t1).GetText());
                    Line++;
                    if (t2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                    {
                        if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)t2).designator())))
                        {
                            code.Add(Line+" LOAD_FAST "+Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            Line++;
                        }
                        else
                        {
                            code.Add(Line+" LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            Line++;
                        }
                    }else if (t2.GetType()==typeof(Parser1.NumFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.NumFACASTContext)t2).GetText());
                        Line++;
                    }else if (t1.GetType()==typeof(Parser1.CharFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)t2).GetText());
                        Line++;
                    }
                    else if (t1.GetType()==typeof(Parser1.FloatFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)t2).GetText());
                        Line++;
                    }
                }else if (t1.GetType()==typeof(Parser1.CharFACASTContext))
                {
                    code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)t1).GetText());
                    Line++;
                    if (t2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                    {
                        if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)t2).designator())))
                        {
                            code.Add(Line+" LOAD_FAST "+Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            Line++;
                        }
                        else
                        {
                            code.Add(Line+" LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            Line++;
                        }
                    }else if (t2.GetType()==typeof(Parser1.NumFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.NumFACASTContext)t2).GetText());
                        Line++;
                    }else if (t1.GetType()==typeof(Parser1.CharFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)t2).GetText());
                        Line++;
                    }
                    else if (t1.GetType()==typeof(Parser1.FloatFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)t2).GetText());
                        Line++;
                    }
                }else if (t1.GetType()==typeof(Parser1.FloatFACASTContext))
                {
                    code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)t1).GetText());
                    Line++;
                    if (t2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                    {
                        if (buscarLocals((string)Visit(((Parser1.DesignatorFACASTContext)t2).designator())))
                        {
                            code.Add(Line+" LOAD_FAST "+Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            Line++;
                        }
                        else
                        {
                            code.Add(Line+" LOAD_GLOBAL "+Visit(((Parser1.DesignatorFACASTContext)t2).designator()));
                            Line++;
                        }
                    }else if (t2.GetType()==typeof(Parser1.NumFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.NumFACASTContext)t2).GetText());
                        Line++;
                    }else if (t1.GetType()==typeof(Parser1.CharFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.CharFACASTContext)t2).GetText());
                        Line++;
                    }else if (t1.GetType()==typeof(Parser1.FloatFACASTContext))
                    {
                        code.Add(Line+" LOAD_CONST "+((Parser1.FloatFACASTContext)t2).GetText());
                        Line++;
                    }
                }
                

                Parser1.AddopContext op = (Parser1.AddopContext)Visit(context.addop()[0]);
                if (op.SUM()!=null)
                {
                    code.Add(Line + " BINARY_ADD");
                    Line++;
                }
                else
                {
                    code.Add(Line + " BINARY_SUBSTRACT");
                    Line++;
                }

                return Visit(context.term()[0]);
            }
            return Visit(context.term()[0]);
        }

        public override object VisitTermAST(Parser1.TermASTContext context)
        {
            if (context.factor().Length >1)
            {
                var F1=Visit(context.factor()[0]);
                var F2 =Visit(context.factor()[1]);
                var op = (Parser1.MulopContext)Visit(context.mulop()[0]);
                if (op.DIV()!=null)
                {
                    if (F1.GetType()==typeof(Parser1.DesignatorFACASTContext))
                    {
                        if (buscarLocals(((Parser1.DesignatorFACASTContext) F1).GetText()))
                        {
                            code.Add(Line + " LOAD_FAST " +
                                     ((Parser1.DesignatorFACASTContext) F1).GetText());
                            Line++;
                        }
                        else
                        {
                            code.Add(Line + " LOAD_GLOBAL " +
                                     ((Parser1.DesignatorFACASTContext) F1).GetText());
                            Line++;
                        }
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }else if (F2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }else if (F2.GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                    }
                    if (F1.GetType() == typeof(Parser1.NumFACASTContext))
                    {
                        code.Add(Line + " LOAD_CONST " +
                                 ((Parser1.NumFACASTContext) F1).GetText());
                        Line++;
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }else if (F2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }else if (F2.GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                        
                    }

                    if (F1.GetType() == typeof(Parser1.FloatFACASTContext))
                    {
                        code.Add(Line + " LOAD_CONST " +
                                 ((Parser1.FloatFACASTContext) F1).GetText());
                        Line++;
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }
                        else if (F2.GetType() == typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }
                        else if (F2.GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                    }



                    code.Add(Line + " BINARY_DIVIDE");
                    Line++;
                }else if (op.DIVMOD() != null)
                {
                    if (F1.GetType()==typeof(Parser1.DesignatorFACASTContext))
                    {
                        if (buscarLocals(((Parser1.DesignatorFACASTContext) F1).GetText()))
                        {
                            code.Add(Line + " LOAD_FAST " +
                                     ((Parser1.DesignatorFACASTContext) F1).GetText());
                            Line++;
                        }
                        else
                        {
                            code.Add(Line + " LOAD_GLOBAL " +
                                     ((Parser1.DesignatorFACASTContext) F1).GetText());
                            Line++;
                        }
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }else if (F2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }else if (F2.GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                    }
                    if (F1.GetType() == typeof(Parser1.NumFACASTContext))
                    {
                        code.Add(Line + " LOAD_CONST " +
                                 ((Parser1.NumFACASTContext) F1).GetText());
                        Line++;
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }else if (F2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }else if (F2.GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                        
                    }

                    if (F1.GetType() == typeof(Parser1.FloatFACASTContext))
                    {
                        code.Add(Line + " LOAD_CONST " +
                                 ((Parser1.FloatFACASTContext) F1).GetText());
                        Line++;
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }
                        else if (F2.GetType() == typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }
                        else if (F2.GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                    }
                    code.Add(Line + " BINARY_MODULO");
                    Line++;
                    
                }else if (op.MULT() != null)
                {
                    if (F1.GetType()==typeof(Parser1.DesignatorFACASTContext))
                    {
                        if (buscarLocals(((Parser1.DesignatorFACASTContext) F1).GetText()))
                        {
                            code.Add(Line + " LOAD_FAST " +
                                     ((Parser1.DesignatorFACASTContext) F1).GetText());
                            Line++;
                        }
                        else
                        {
                            code.Add(Line + " LOAD_GLOBAL " +
                                     ((Parser1.DesignatorFACASTContext) F1).GetText());
                            Line++;
                        }
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }else if (F2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (((string)code[code.Count-2]).Contains("CALL"))
                            {
                                
                            }
                            else if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }else if (F2.GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                    }
                    if (F1.GetType() == typeof(Parser1.NumFACASTContext))
                    {
                        code.Add(Line + " LOAD_CONST " +
                                 ((Parser1.NumFACASTContext) F1).GetText());
                        Line++;
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }else if (F2.GetType()==typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }else if (Visit(context.factor()[1]).GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                        
                    }

                    if (F1.GetType() == typeof(Parser1.FloatFACASTContext))
                    {
                        code.Add(Line + " LOAD_CONST " +
                                 ((Parser1.FloatFACASTContext) F1).GetText());
                        Line++;
                        if (F2.GetType() == typeof(Parser1.NumFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.NumFACASTContext) F2).GetText());
                            Line++;
                        }
                        else if (F2.GetType() == typeof(Parser1.DesignatorFACASTContext))
                        {
                            if (buscarLocals(((Parser1.DesignatorFACASTContext) F2).GetText()))
                            {
                                code.Add(Line + " LOAD_FAST " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }
                            else
                            {
                                code.Add(Line + " LOAD_GLOBAL " +
                                         ((Parser1.DesignatorFACASTContext) F2).GetText());
                                Line++;
                            }

                        }
                        else if (F2.GetType() == typeof(Parser1.FloatFACASTContext))
                        {
                            code.Add(Line + " LOAD_CONST " +
                                     ((Parser1.FloatFACASTContext) F2).GetText());
                            Line++;
                        }
                    }
                    code.Add(Line + " BINARY_MULTIPLY");
                    Line++;
                }
            }
            return Visit(context.factor()[0]);
        }

        public override object VisitDesignatorFACAST(Parser1.DesignatorFACASTContext context)
        {
            if (context.PDER().Length>=1)
            {
                Visit(context.actpars()[0]);
                if (!((string)(code[code.Count-1])).Contains("LOAD_GLOBAL"+Visit(context.designator())))
                {
                    if (!((string)(code[code.Count-1])).Contains("CALL_FUNCTION"))
                    {
                        code.Add(Line + " LOAD_GLOBAL " + Visit(context.designator()));
                        Line++;
                    }
                    
                }
                var acp = context.actpars().Length;
                code.Add(Line + " CALL_FUNCTION " + acp);
                Line++;
            }
            return context;
        }

        public override object VisitNumFACAST(Parser1.NumFACASTContext context)
        {
            return context;
        }

        public override object VisitCharFACAST(Parser1.CharFACASTContext context)
        {
            return context;
        }

        public override object VisitBooleanFACAST(Parser1.BooleanFACASTContext context)
        {
            return context;
        }

        public override object VisitNewFACAST(Parser1.NewFACASTContext context)
        {
            return context;
        }

        public override object VisitStringFACAST(Parser1.StringFACASTContext context)
        {
            return context;
        }

        public override object VisitFloatFACAST(Parser1.FloatFACASTContext context)
        {
            return context;
        }

        public override object VisitExprFACAST(Parser1.ExprFACASTContext context)
        {
            Visit(context.expr());
            return context;
        }

        public override object VisitDesignatorAST(Parser1.DesignatorASTContext context)
        {
            return context.IDENT()[0].GetText();
        }

        public override object VisitRelop(Parser1.RelopContext context)
        {
            return context.GetText();
        }

        public override object VisitAddop(Parser1.AddopContext context)
        {
            return context;
        }

        public override object VisitMulop(Parser1.MulopContext context)
        {
            return context;
        }

        public override object VisitSwitchAST(Parser1.SwitchASTContext context)
        {
            //FALTA!!
            var lineinicio = Line;
            string cargavar = "";
            if (buscarLocals(context.IDENT().GetText()))
            {
                //code.Add(Line + " LOAD_FAST " + context.IDENT().GetText());
                //Line++;
                cargavar = " LOAD_FAST " + context.IDENT().GetText();
            }
            else
            {
                //code.Add(Line + " LOAD_GLOBAL " + context.IDENT().GetText());
                //Line++;
                cargavar = " LOAD_GLOBAL " + context.IDENT().GetText();
            }

            for (int i = 0; i < context.CASE().Length; i++)
            {
                var linejump = 0;
                if (context.NUMBER().Length>0)
                {
                    code.Add(Line + cargavar);
                    Line++;
                    code.Add(Line + " LOAD_CONST " + context.NUMBER()[i].GetText());
                    Line++;
                    code.Add(Line + " COMPARE_OP ==");
                    Line++;
                    linejump = Line;
                    code.Add(Line + " JUMP_IF_FALSE");
                    Line++;
                    Visit(context.statement()[i]);
                    code.Add(Line+" JUMP_ABSOLUTE ");
                    Line++;
                }
                else
                {
                    code.Add(Line + cargavar);
                    Line++;
                    code.Add(Line + " LOAD_CONST " + context.CHARCONST()[i].GetText());
                    Line++;
                    code.Add(Line + " COMPARE_OP ==");
                    Line++;
                    linejump = Line;
                    code.Add(Line + " JUMP_IF_FALSE");
                    Line++;
                    Visit(context.statement()[i]);
                    code.Add(Line+" JUMP_ABSOLUTE ");
                    Line++;
                }

                code[linejump] = linejump + " JUMP_IF_FALSE " + Line;
            }

            Visit(context.statement()[(context.statement().Length) - 1]);
            code.Add(Line + " JUMP_ABSOLUTE ");
            Line++;
            for (int i = lineinicio; i < code.Count; i++)
            {
                if (((string) code[i]).Contains("JUMP_ABSOLUTE"))
                    code[i] = ""+code[i]+Line;
            }

            return null;
        }
        public string getList() {
            StringBuilder err=new StringBuilder();
            if (code.Count ==0) {
                return "\nNO HAY CODIGO GENERADO!";
            }

            err.Append("\nCODIGO!:\n");
            foreach (string e in code) {
                err.Append(e+"\n");
                
            }

            return err.ToString();

        }

        public bool buscarLocals(string name)
        {
            foreach (var local in locals)
            {
                if (local.Equals(name))
                {
                    return true;
                }
            }
            return false;
        }
    }
    
}