// Generated from C:/Users/marco/source/repos/ProyectoOscarMarco/ProyectoOscarMarco\Parser1.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link Parser1}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface Parser1Visitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link Parser1#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(Parser1.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#constdecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstdecl(Parser1.ConstdeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#vardecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVardecl(Parser1.VardeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#classdecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassdecl(Parser1.ClassdeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#methodecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodecl(Parser1.MethodeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#formpars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormpars(Parser1.FormparsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(Parser1.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(Parser1.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(Parser1.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#actpars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActpars(Parser1.ActparsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(Parser1.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#condterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondterm(Parser1.CondtermContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#condfact}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondfact(Parser1.CondfactContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(Parser1.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(Parser1.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactor(Parser1.FactorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#designator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDesignator(Parser1.DesignatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#relop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelop(Parser1.RelopContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#addop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddop(Parser1.AddopContext ctx);
	/**
	 * Visit a parse tree produced by {@link Parser1#mulop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMulop(Parser1.MulopContext ctx);
}