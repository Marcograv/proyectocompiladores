// Generated from C:/Users/marco/source/repos/ProyectoOscarMarco/ProyectoOscarMarco\Parser1.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class Parser1 extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BREAK=1, CLASS=2, CONST=3, ELSE=4, IF=5, NEW=6, READ=7, RETURN=8, VOID=9, 
		WHILE=10, FOR=11, WRITE=12, VAR=13, BOOL=14, INT=15, FLOAT=16, CHAR=17, 
		IDENT=18, NUMBER=19, FLOATNUMBER=20, CHARCONST=21, BOOLEAN=22, SUM=23, 
		RES=24, MULT=25, DIV=26, DIVMOD=27, IGUAL=28, DIF=29, MENOR=30, MENORIGUAL=31, 
		MAYOR=32, MAYORIGUAL=33, AND=34, OR=35, ASIGNA=36, PLUS=37, RESTA=38, 
		PYCOMA=39, COMA=40, PUNTO=41, PIZQ=42, PDER=43, CORCHETEIZQ=44, CORCHETEDER=45, 
		BRAKETIZQ=46, BRAKETDER=47, COMMENT=48, LINE_COMMENT=49, WS=50;
	public static final int
		RULE_program = 0, RULE_constdecl = 1, RULE_vardecl = 2, RULE_classdecl = 3, 
		RULE_methodecl = 4, RULE_formpars = 5, RULE_type = 6, RULE_statement = 7, 
		RULE_block = 8, RULE_actpars = 9, RULE_condition = 10, RULE_condterm = 11, 
		RULE_condfact = 12, RULE_expr = 13, RULE_term = 14, RULE_factor = 15, 
		RULE_designator = 16, RULE_relop = 17, RULE_addop = 18, RULE_mulop = 19;
	private static String[] makeRuleNames() {
		return new String[] {
			"program", "constdecl", "vardecl", "classdecl", "methodecl", "formpars", 
			"type", "statement", "block", "actpars", "condition", "condterm", "condfact", 
			"expr", "term", "factor", "designator", "relop", "addop", "mulop"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'break'", "'class'", "'const'", "'else'", "'if'", "'new'", "'read'", 
			"'return'", "'void'", "'while'", "'for'", "'write'", "'var'", "'boolean'", 
			"'int'", "'float'", "'char'", null, null, null, null, null, "'+'", "'-'", 
			"'*'", "'/'", "'%'", "'=='", "'!='", "'>'", "'>='", "'<'", "'<='", "'&&'", 
			"'||'", "'='", "'++'", "'--'", "';'", "','", "'.'", "'('", "')'", "'['", 
			"']'", "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "BREAK", "CLASS", "CONST", "ELSE", "IF", "NEW", "READ", "RETURN", 
			"VOID", "WHILE", "FOR", "WRITE", "VAR", "BOOL", "INT", "FLOAT", "CHAR", 
			"IDENT", "NUMBER", "FLOATNUMBER", "CHARCONST", "BOOLEAN", "SUM", "RES", 
			"MULT", "DIV", "DIVMOD", "IGUAL", "DIF", "MENOR", "MENORIGUAL", "MAYOR", 
			"MAYORIGUAL", "AND", "OR", "ASIGNA", "PLUS", "RESTA", "PYCOMA", "COMA", 
			"PUNTO", "PIZQ", "PDER", "CORCHETEIZQ", "CORCHETEDER", "BRAKETIZQ", "BRAKETDER", 
			"COMMENT", "LINE_COMMENT", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Parser1.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public Parser1(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public List<ClassdeclContext> classdecl() {
			return getRuleContexts(ClassdeclContext.class);
		}
		public ClassdeclContext classdecl(int i) {
			return getRuleContext(ClassdeclContext.class,i);
		}
		public TerminalNode BRAKETIZQ() { return getToken(Parser1.BRAKETIZQ, 0); }
		public TerminalNode BRAKETDER() { return getToken(Parser1.BRAKETDER, 0); }
		public List<VardeclContext> vardecl() {
			return getRuleContexts(VardeclContext.class);
		}
		public VardeclContext vardecl(int i) {
			return getRuleContext(VardeclContext.class,i);
		}
		public List<ConstdeclContext> constdecl() {
			return getRuleContexts(ConstdeclContext.class);
		}
		public ConstdeclContext constdecl(int i) {
			return getRuleContext(ConstdeclContext.class,i);
		}
		public List<MethodeclContext> methodecl() {
			return getRuleContexts(MethodeclContext.class);
		}
		public MethodeclContext methodecl(int i) {
			return getRuleContext(MethodeclContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			classdecl();
			setState(46);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					setState(44);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case BOOL:
					case INT:
					case FLOAT:
					case CHAR:
					case IDENT:
						{
						setState(41);
						vardecl();
						}
						break;
					case CONST:
						{
						setState(42);
						constdecl();
						}
						break;
					case CLASS:
						{
						setState(43);
						classdecl();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(48);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			setState(49);
			match(BRAKETIZQ);
			setState(53);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					{
					setState(50);
					methodecl();
					}
					} 
				}
				setState(55);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(56);
			match(BRAKETDER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstdeclContext extends ParserRuleContext {
		public TerminalNode CONST() { return getToken(Parser1.CONST, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENT() { return getToken(Parser1.IDENT, 0); }
		public TerminalNode ASIGNA() { return getToken(Parser1.ASIGNA, 0); }
		public TerminalNode PYCOMA() { return getToken(Parser1.PYCOMA, 0); }
		public TerminalNode NUMBER() { return getToken(Parser1.NUMBER, 0); }
		public TerminalNode CHARCONST() { return getToken(Parser1.CHARCONST, 0); }
		public ConstdeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constdecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterConstdecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitConstdecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitConstdecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstdeclContext constdecl() throws RecognitionException {
		ConstdeclContext _localctx = new ConstdeclContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_constdecl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			match(CONST);
			setState(59);
			type();
			setState(60);
			match(IDENT);
			setState(61);
			match(ASIGNA);
			setState(62);
			_la = _input.LA(1);
			if ( !(_la==NUMBER || _la==CHARCONST) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(63);
			match(PYCOMA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VardeclContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<TerminalNode> IDENT() { return getTokens(Parser1.IDENT); }
		public TerminalNode IDENT(int i) {
			return getToken(Parser1.IDENT, i);
		}
		public TerminalNode PYCOMA() { return getToken(Parser1.PYCOMA, 0); }
		public List<TerminalNode> COMA() { return getTokens(Parser1.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(Parser1.COMA, i);
		}
		public VardeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_vardecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterVardecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitVardecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitVardecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VardeclContext vardecl() throws RecognitionException {
		VardeclContext _localctx = new VardeclContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_vardecl);
		try {
			int _alt;
			setState(80);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(65);
				type();
				setState(66);
				match(IDENT);
				setState(71);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1+1 ) {
						{
						{
						setState(67);
						match(COMA);
						setState(68);
						match(IDENT);
						}
						} 
					}
					setState(73);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				}
				setState(74);
				match(PYCOMA);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(76);
				type();
				setState(77);
				match(IDENT);
				setState(78);
				match(PYCOMA);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassdeclContext extends ParserRuleContext {
		public TerminalNode CLASS() { return getToken(Parser1.CLASS, 0); }
		public TerminalNode IDENT() { return getToken(Parser1.IDENT, 0); }
		public List<TerminalNode> BRAKETIZQ() { return getTokens(Parser1.BRAKETIZQ); }
		public TerminalNode BRAKETIZQ(int i) {
			return getToken(Parser1.BRAKETIZQ, i);
		}
		public List<TerminalNode> BRAKETDER() { return getTokens(Parser1.BRAKETDER); }
		public TerminalNode BRAKETDER(int i) {
			return getToken(Parser1.BRAKETDER, i);
		}
		public List<VardeclContext> vardecl() {
			return getRuleContexts(VardeclContext.class);
		}
		public VardeclContext vardecl(int i) {
			return getRuleContext(VardeclContext.class,i);
		}
		public ClassdeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classdecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterClassdecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitClassdecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitClassdecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassdeclContext classdecl() throws RecognitionException {
		ClassdeclContext _localctx = new ClassdeclContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_classdecl);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(82);
			match(CLASS);
			setState(83);
			match(IDENT);
			setState(90);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					{
					setState(84);
					match(BRAKETIZQ);
					{
					setState(85);
					vardecl();
					}
					setState(86);
					match(BRAKETDER);
					}
					} 
				}
				setState(92);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodeclContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(Parser1.IDENT, 0); }
		public TerminalNode PIZQ() { return getToken(Parser1.PIZQ, 0); }
		public TerminalNode PDER() { return getToken(Parser1.PDER, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode VOID() { return getToken(Parser1.VOID, 0); }
		public FormparsContext formpars() {
			return getRuleContext(FormparsContext.class,0);
		}
		public List<VardeclContext> vardecl() {
			return getRuleContexts(VardeclContext.class);
		}
		public VardeclContext vardecl(int i) {
			return getRuleContext(VardeclContext.class,i);
		}
		public MethodeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterMethodecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitMethodecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitMethodecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodeclContext methodecl() throws RecognitionException {
		MethodeclContext _localctx = new MethodeclContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_methodecl);
		int _la;
		try {
			int _alt;
			setState(124);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(95);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case BOOL:
				case INT:
				case FLOAT:
				case CHAR:
				case IDENT:
					{
					setState(93);
					type();
					}
					break;
				case VOID:
					{
					setState(94);
					match(VOID);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(97);
				match(IDENT);
				setState(98);
				match(PIZQ);
				setState(100);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOL) | (1L << INT) | (1L << FLOAT) | (1L << CHAR) | (1L << IDENT))) != 0)) {
					{
					setState(99);
					formpars();
					}
				}

				setState(102);
				match(PDER);
				setState(106);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1+1 ) {
						{
						{
						setState(103);
						vardecl();
						}
						} 
					}
					setState(108);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				}
				setState(109);
				block();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(112);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case BOOL:
				case INT:
				case FLOAT:
				case CHAR:
				case IDENT:
					{
					setState(110);
					type();
					}
					break;
				case VOID:
					{
					setState(111);
					match(VOID);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(114);
				match(IDENT);
				setState(115);
				match(PIZQ);
				setState(116);
				match(PDER);
				setState(120);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOL) | (1L << INT) | (1L << FLOAT) | (1L << CHAR) | (1L << IDENT))) != 0)) {
					{
					{
					setState(117);
					vardecl();
					}
					}
					setState(122);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(123);
				block();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormparsContext extends ParserRuleContext {
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> IDENT() { return getTokens(Parser1.IDENT); }
		public TerminalNode IDENT(int i) {
			return getToken(Parser1.IDENT, i);
		}
		public List<TerminalNode> COMA() { return getTokens(Parser1.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(Parser1.COMA, i);
		}
		public FormparsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formpars; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterFormpars(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitFormpars(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitFormpars(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FormparsContext formpars() throws RecognitionException {
		FormparsContext _localctx = new FormparsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_formpars);
		try {
			int _alt;
			setState(140);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(126);
				type();
				setState(127);
				match(IDENT);
				setState(134);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1+1 ) {
						{
						{
						setState(128);
						match(COMA);
						setState(129);
						type();
						setState(130);
						match(IDENT);
						}
						} 
					}
					setState(136);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(137);
				type();
				setState(138);
				match(IDENT);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode CHAR() { return getToken(Parser1.CHAR, 0); }
		public List<TerminalNode> CORCHETEIZQ() { return getTokens(Parser1.CORCHETEIZQ); }
		public TerminalNode CORCHETEIZQ(int i) {
			return getToken(Parser1.CORCHETEIZQ, i);
		}
		public TerminalNode CORCHETEDER() { return getToken(Parser1.CORCHETEDER, 0); }
		public List<TerminalNode> IDENT() { return getTokens(Parser1.IDENT); }
		public TerminalNode IDENT(int i) {
			return getToken(Parser1.IDENT, i);
		}
		public TerminalNode NUMBER() { return getToken(Parser1.NUMBER, 0); }
		public TerminalNode INT() { return getToken(Parser1.INT, 0); }
		public TerminalNode FLOAT() { return getToken(Parser1.FLOAT, 0); }
		public TerminalNode BOOL() { return getToken(Parser1.BOOL, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_type);
		int _la;
		try {
			setState(178);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(142);
				match(CHAR);
				setState(148);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CORCHETEIZQ) {
					{
					setState(143);
					match(CORCHETEIZQ);
					setState(145);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==IDENT || _la==NUMBER) {
						{
						setState(144);
						_la = _input.LA(1);
						if ( !(_la==IDENT || _la==NUMBER) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
					}

					setState(147);
					match(CORCHETEDER);
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(150);
				match(INT);
				setState(156);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CORCHETEIZQ) {
					{
					setState(151);
					match(CORCHETEIZQ);
					setState(153);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==IDENT || _la==NUMBER) {
						{
						setState(152);
						_la = _input.LA(1);
						if ( !(_la==IDENT || _la==NUMBER) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
					}

					setState(155);
					match(CORCHETEDER);
					}
				}

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(158);
				match(FLOAT);
				setState(164);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CORCHETEIZQ) {
					{
					setState(159);
					match(CORCHETEIZQ);
					setState(161);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==IDENT || _la==NUMBER) {
						{
						setState(160);
						_la = _input.LA(1);
						if ( !(_la==IDENT || _la==NUMBER) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
					}

					setState(163);
					match(CORCHETEIZQ);
					}
				}

				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(166);
				match(INT);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(167);
				match(FLOAT);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(168);
				match(BOOL);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(169);
				match(CHAR);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(170);
				match(IDENT);
				setState(176);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CORCHETEIZQ) {
					{
					setState(171);
					match(CORCHETEIZQ);
					setState(173);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==IDENT || _la==NUMBER) {
						{
						setState(172);
						_la = _input.LA(1);
						if ( !(_la==IDENT || _la==NUMBER) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
					}

					setState(175);
					match(CORCHETEDER);
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public DesignatorContext designator() {
			return getRuleContext(DesignatorContext.class,0);
		}
		public List<TerminalNode> PYCOMA() { return getTokens(Parser1.PYCOMA); }
		public TerminalNode PYCOMA(int i) {
			return getToken(Parser1.PYCOMA, i);
		}
		public TerminalNode ASIGNA() { return getToken(Parser1.ASIGNA, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode PIZQ() { return getToken(Parser1.PIZQ, 0); }
		public TerminalNode PDER() { return getToken(Parser1.PDER, 0); }
		public TerminalNode PLUS() { return getToken(Parser1.PLUS, 0); }
		public TerminalNode RESTA() { return getToken(Parser1.RESTA, 0); }
		public ActparsContext actpars() {
			return getRuleContext(ActparsContext.class,0);
		}
		public TerminalNode IF() { return getToken(Parser1.IF, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(Parser1.ELSE, 0); }
		public TerminalNode FOR() { return getToken(Parser1.FOR, 0); }
		public TerminalNode WHILE() { return getToken(Parser1.WHILE, 0); }
		public TerminalNode BREAK() { return getToken(Parser1.BREAK, 0); }
		public TerminalNode RETURN() { return getToken(Parser1.RETURN, 0); }
		public TerminalNode READ() { return getToken(Parser1.READ, 0); }
		public TerminalNode WRITE() { return getToken(Parser1.WRITE, 0); }
		public TerminalNode COMA() { return getToken(Parser1.COMA, 0); }
		public TerminalNode NUMBER() { return getToken(Parser1.NUMBER, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_statement);
		int _la;
		try {
			setState(246);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENT:
				enterOuterAlt(_localctx, 1);
				{
				setState(180);
				designator();
				setState(190);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ASIGNA:
					{
					setState(181);
					match(ASIGNA);
					setState(182);
					expr();
					}
					break;
				case PIZQ:
					{
					setState(183);
					match(PIZQ);
					setState(185);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NEW) | (1L << IDENT) | (1L << NUMBER) | (1L << CHARCONST) | (1L << BOOLEAN) | (1L << RES) | (1L << PIZQ))) != 0)) {
						{
						setState(184);
						actpars();
						}
					}

					setState(187);
					match(PDER);
					}
					break;
				case PLUS:
					{
					setState(188);
					match(PLUS);
					}
					break;
				case RESTA:
					{
					setState(189);
					match(RESTA);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(192);
				match(PYCOMA);
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 2);
				{
				setState(194);
				match(IF);
				setState(195);
				match(PIZQ);
				setState(196);
				condition();
				setState(197);
				match(PDER);
				setState(198);
				statement();
				setState(201);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
				case 1:
					{
					setState(199);
					match(ELSE);
					setState(200);
					statement();
					}
					break;
				}
				}
				break;
			case FOR:
				enterOuterAlt(_localctx, 3);
				{
				setState(203);
				match(FOR);
				setState(204);
				match(PIZQ);
				setState(205);
				expr();
				setState(206);
				match(PYCOMA);
				{
				setState(207);
				condition();
				}
				setState(208);
				match(PYCOMA);
				setState(210);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BREAK) | (1L << IF) | (1L << READ) | (1L << RETURN) | (1L << WHILE) | (1L << FOR) | (1L << WRITE) | (1L << IDENT) | (1L << PYCOMA) | (1L << BRAKETIZQ))) != 0)) {
					{
					setState(209);
					statement();
					}
				}

				setState(212);
				match(PDER);
				setState(213);
				statement();
				}
				break;
			case WHILE:
				enterOuterAlt(_localctx, 4);
				{
				setState(215);
				match(WHILE);
				setState(216);
				match(PIZQ);
				setState(217);
				condition();
				setState(218);
				match(PDER);
				setState(219);
				statement();
				}
				break;
			case BREAK:
				enterOuterAlt(_localctx, 5);
				{
				setState(221);
				match(BREAK);
				setState(222);
				match(PYCOMA);
				}
				break;
			case RETURN:
				enterOuterAlt(_localctx, 6);
				{
				setState(223);
				match(RETURN);
				setState(225);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NEW) | (1L << IDENT) | (1L << NUMBER) | (1L << CHARCONST) | (1L << BOOLEAN) | (1L << RES) | (1L << PIZQ))) != 0)) {
					{
					setState(224);
					expr();
					}
				}

				setState(227);
				match(PYCOMA);
				}
				break;
			case READ:
				enterOuterAlt(_localctx, 7);
				{
				setState(228);
				match(READ);
				setState(229);
				match(PIZQ);
				setState(230);
				designator();
				setState(231);
				match(PDER);
				setState(232);
				match(PYCOMA);
				}
				break;
			case WRITE:
				enterOuterAlt(_localctx, 8);
				{
				setState(234);
				match(WRITE);
				setState(235);
				match(PIZQ);
				setState(236);
				expr();
				setState(239);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COMA) {
					{
					setState(237);
					match(COMA);
					setState(238);
					match(NUMBER);
					}
				}

				setState(241);
				match(PDER);
				setState(242);
				match(PYCOMA);
				}
				break;
			case BRAKETIZQ:
				enterOuterAlt(_localctx, 9);
				{
				setState(244);
				block();
				}
				break;
			case PYCOMA:
				enterOuterAlt(_localctx, 10);
				{
				setState(245);
				match(PYCOMA);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode BRAKETIZQ() { return getToken(Parser1.BRAKETIZQ, 0); }
		public TerminalNode BRAKETDER() { return getToken(Parser1.BRAKETDER, 0); }
		public List<VardeclContext> vardecl() {
			return getRuleContexts(VardeclContext.class);
		}
		public VardeclContext vardecl(int i) {
			return getRuleContext(VardeclContext.class,i);
		}
		public List<ConstdeclContext> constdecl() {
			return getRuleContexts(ConstdeclContext.class);
		}
		public ConstdeclContext constdecl(int i) {
			return getRuleContext(ConstdeclContext.class,i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<MethodeclContext> methodecl() {
			return getRuleContexts(MethodeclContext.class);
		}
		public MethodeclContext methodecl(int i) {
			return getRuleContext(MethodeclContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248);
			match(BRAKETIZQ);
			setState(255);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BREAK) | (1L << CONST) | (1L << IF) | (1L << READ) | (1L << RETURN) | (1L << VOID) | (1L << WHILE) | (1L << FOR) | (1L << WRITE) | (1L << BOOL) | (1L << INT) | (1L << FLOAT) | (1L << CHAR) | (1L << IDENT) | (1L << PYCOMA) | (1L << BRAKETIZQ))) != 0)) {
				{
				setState(253);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
				case 1:
					{
					setState(249);
					vardecl();
					}
					break;
				case 2:
					{
					setState(250);
					constdecl();
					}
					break;
				case 3:
					{
					setState(251);
					statement();
					}
					break;
				case 4:
					{
					setState(252);
					methodecl();
					}
					break;
				}
				}
				setState(257);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(258);
			match(BRAKETDER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActparsContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMA() { return getTokens(Parser1.COMA); }
		public TerminalNode COMA(int i) {
			return getToken(Parser1.COMA, i);
		}
		public ActparsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actpars; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterActpars(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitActpars(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitActpars(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActparsContext actpars() throws RecognitionException {
		ActparsContext _localctx = new ActparsContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_actpars);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(260);
			expr();
			setState(265);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					{
					setState(261);
					match(COMA);
					setState(262);
					expr();
					}
					} 
				}
				setState(267);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public List<CondtermContext> condterm() {
			return getRuleContexts(CondtermContext.class);
		}
		public CondtermContext condterm(int i) {
			return getRuleContext(CondtermContext.class,i);
		}
		public List<TerminalNode> OR() { return getTokens(Parser1.OR); }
		public TerminalNode OR(int i) {
			return getToken(Parser1.OR, i);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_condition);
		int _la;
		try {
			setState(277);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(268);
				condterm();
				setState(273);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==OR) {
					{
					{
					setState(269);
					match(OR);
					setState(270);
					condterm();
					}
					}
					setState(275);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(276);
				condterm();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CondtermContext extends ParserRuleContext {
		public List<CondfactContext> condfact() {
			return getRuleContexts(CondfactContext.class);
		}
		public CondfactContext condfact(int i) {
			return getRuleContext(CondfactContext.class,i);
		}
		public List<TerminalNode> AND() { return getTokens(Parser1.AND); }
		public TerminalNode AND(int i) {
			return getToken(Parser1.AND, i);
		}
		public CondtermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condterm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterCondterm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitCondterm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitCondterm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CondtermContext condterm() throws RecognitionException {
		CondtermContext _localctx = new CondtermContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_condterm);
		int _la;
		try {
			setState(288);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(279);
				condfact();
				setState(284);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==AND) {
					{
					{
					setState(280);
					match(AND);
					setState(281);
					condfact();
					}
					}
					setState(286);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(287);
				condfact();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CondfactContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public RelopContext relop() {
			return getRuleContext(RelopContext.class,0);
		}
		public CondfactContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condfact; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterCondfact(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitCondfact(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitCondfact(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CondfactContext condfact() throws RecognitionException {
		CondfactContext _localctx = new CondfactContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_condfact);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(290);
			expr();
			setState(291);
			relop();
			setState(292);
			expr();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public TerminalNode RES() { return getToken(Parser1.RES, 0); }
		public List<AddopContext> addop() {
			return getRuleContexts(AddopContext.class);
		}
		public AddopContext addop(int i) {
			return getRuleContext(AddopContext.class,i);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_expr);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(295);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RES) {
				{
				setState(294);
				match(RES);
				}
			}

			setState(297);
			term();
			setState(303);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					{
					setState(298);
					addop();
					setState(299);
					term();
					}
					} 
				}
				setState(305);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public List<FactorContext> factor() {
			return getRuleContexts(FactorContext.class);
		}
		public FactorContext factor(int i) {
			return getRuleContext(FactorContext.class,i);
		}
		public List<MulopContext> mulop() {
			return getRuleContexts(MulopContext.class);
		}
		public MulopContext mulop(int i) {
			return getRuleContext(MulopContext.class,i);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_term);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(306);
			factor();
			setState(312);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					{
					setState(307);
					mulop();
					setState(308);
					factor();
					}
					} 
				}
				setState(314);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public DesignatorContext designator() {
			return getRuleContext(DesignatorContext.class,0);
		}
		public List<TerminalNode> PIZQ() { return getTokens(Parser1.PIZQ); }
		public TerminalNode PIZQ(int i) {
			return getToken(Parser1.PIZQ, i);
		}
		public List<TerminalNode> PDER() { return getTokens(Parser1.PDER); }
		public TerminalNode PDER(int i) {
			return getToken(Parser1.PDER, i);
		}
		public List<ActparsContext> actpars() {
			return getRuleContexts(ActparsContext.class);
		}
		public ActparsContext actpars(int i) {
			return getRuleContext(ActparsContext.class,i);
		}
		public TerminalNode NUMBER() { return getToken(Parser1.NUMBER, 0); }
		public TerminalNode CHARCONST() { return getToken(Parser1.CHARCONST, 0); }
		public TerminalNode BOOLEAN() { return getToken(Parser1.BOOLEAN, 0); }
		public TerminalNode NEW() { return getToken(Parser1.NEW, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IDENT() { return getToken(Parser1.IDENT, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterFactor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitFactor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitFactor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_factor);
		try {
			int _alt;
			setState(337);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENT:
				enterOuterAlt(_localctx, 1);
				{
				setState(315);
				designator();
				setState(322);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
				while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1+1 ) {
						{
						{
						setState(316);
						match(PIZQ);
						{
						setState(317);
						actpars();
						}
						setState(318);
						match(PDER);
						}
						} 
					}
					setState(324);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
				}
				}
				break;
			case NUMBER:
				enterOuterAlt(_localctx, 2);
				{
				setState(325);
				match(NUMBER);
				}
				break;
			case CHARCONST:
				enterOuterAlt(_localctx, 3);
				{
				setState(326);
				match(CHARCONST);
				}
				break;
			case BOOLEAN:
				enterOuterAlt(_localctx, 4);
				{
				setState(327);
				match(BOOLEAN);
				}
				break;
			case NEW:
				enterOuterAlt(_localctx, 5);
				{
				setState(328);
				match(NEW);
				setState(331);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
				case 1:
					{
					setState(329);
					type();
					}
					break;
				case 2:
					{
					setState(330);
					match(IDENT);
					}
					break;
				}
				}
				break;
			case PIZQ:
				enterOuterAlt(_localctx, 6);
				{
				setState(333);
				match(PIZQ);
				setState(334);
				expr();
				setState(335);
				match(PDER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DesignatorContext extends ParserRuleContext {
		public List<TerminalNode> IDENT() { return getTokens(Parser1.IDENT); }
		public TerminalNode IDENT(int i) {
			return getToken(Parser1.IDENT, i);
		}
		public List<TerminalNode> PUNTO() { return getTokens(Parser1.PUNTO); }
		public TerminalNode PUNTO(int i) {
			return getToken(Parser1.PUNTO, i);
		}
		public List<TerminalNode> CORCHETEIZQ() { return getTokens(Parser1.CORCHETEIZQ); }
		public TerminalNode CORCHETEIZQ(int i) {
			return getToken(Parser1.CORCHETEIZQ, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> CORCHETEDER() { return getTokens(Parser1.CORCHETEDER); }
		public TerminalNode CORCHETEDER(int i) {
			return getToken(Parser1.CORCHETEDER, i);
		}
		public DesignatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_designator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterDesignator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitDesignator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitDesignator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DesignatorContext designator() throws RecognitionException {
		DesignatorContext _localctx = new DesignatorContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_designator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(339);
			match(IDENT);
			setState(348);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PUNTO || _la==CORCHETEIZQ) {
				{
				setState(346);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case PUNTO:
					{
					setState(340);
					match(PUNTO);
					setState(341);
					match(IDENT);
					}
					break;
				case CORCHETEIZQ:
					{
					setState(342);
					match(CORCHETEIZQ);
					setState(343);
					expr();
					setState(344);
					match(CORCHETEDER);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(350);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelopContext extends ParserRuleContext {
		public TerminalNode IGUAL() { return getToken(Parser1.IGUAL, 0); }
		public TerminalNode DIF() { return getToken(Parser1.DIF, 0); }
		public TerminalNode MENOR() { return getToken(Parser1.MENOR, 0); }
		public TerminalNode MENORIGUAL() { return getToken(Parser1.MENORIGUAL, 0); }
		public TerminalNode MAYOR() { return getToken(Parser1.MAYOR, 0); }
		public TerminalNode MAYORIGUAL() { return getToken(Parser1.MAYORIGUAL, 0); }
		public RelopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterRelop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitRelop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitRelop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelopContext relop() throws RecognitionException {
		RelopContext _localctx = new RelopContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_relop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(351);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IGUAL) | (1L << DIF) | (1L << MENOR) | (1L << MENORIGUAL) | (1L << MAYOR) | (1L << MAYORIGUAL))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddopContext extends ParserRuleContext {
		public TerminalNode SUM() { return getToken(Parser1.SUM, 0); }
		public TerminalNode RES() { return getToken(Parser1.RES, 0); }
		public AddopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_addop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterAddop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitAddop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitAddop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AddopContext addop() throws RecognitionException {
		AddopContext _localctx = new AddopContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_addop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(353);
			_la = _input.LA(1);
			if ( !(_la==SUM || _la==RES) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MulopContext extends ParserRuleContext {
		public TerminalNode MULT() { return getToken(Parser1.MULT, 0); }
		public TerminalNode DIV() { return getToken(Parser1.DIV, 0); }
		public TerminalNode DIVMOD() { return getToken(Parser1.DIVMOD, 0); }
		public MulopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mulop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).enterMulop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof Parser1Listener ) ((Parser1Listener)listener).exitMulop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof Parser1Visitor ) return ((Parser1Visitor<? extends T>)visitor).visitMulop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MulopContext mulop() throws RecognitionException {
		MulopContext _localctx = new MulopContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_mulop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(355);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MULT) | (1L << DIV) | (1L << DIVMOD))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\64\u0168\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\3\2\7\2/\n\2\f\2\16\2\62\13"+
		"\2\3\2\3\2\7\2\66\n\2\f\2\16\29\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\4\3\4\3\4\3\4\7\4H\n\4\f\4\16\4K\13\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4"+
		"S\n\4\3\5\3\5\3\5\3\5\3\5\3\5\7\5[\n\5\f\5\16\5^\13\5\3\6\3\6\5\6b\n\6"+
		"\3\6\3\6\3\6\5\6g\n\6\3\6\3\6\7\6k\n\6\f\6\16\6n\13\6\3\6\3\6\3\6\5\6"+
		"s\n\6\3\6\3\6\3\6\3\6\7\6y\n\6\f\6\16\6|\13\6\3\6\5\6\177\n\6\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\7\7\u0087\n\7\f\7\16\7\u008a\13\7\3\7\3\7\3\7\5\7\u008f"+
		"\n\7\3\b\3\b\3\b\5\b\u0094\n\b\3\b\5\b\u0097\n\b\3\b\3\b\3\b\5\b\u009c"+
		"\n\b\3\b\5\b\u009f\n\b\3\b\3\b\3\b\5\b\u00a4\n\b\3\b\5\b\u00a7\n\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00b0\n\b\3\b\5\b\u00b3\n\b\5\b\u00b5\n\b"+
		"\3\t\3\t\3\t\3\t\3\t\5\t\u00bc\n\t\3\t\3\t\3\t\5\t\u00c1\n\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00cc\n\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5"+
		"\t\u00d5\n\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00e4"+
		"\n\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00f2\n\t\3\t"+
		"\3\t\3\t\3\t\3\t\5\t\u00f9\n\t\3\n\3\n\3\n\3\n\3\n\7\n\u0100\n\n\f\n\16"+
		"\n\u0103\13\n\3\n\3\n\3\13\3\13\3\13\7\13\u010a\n\13\f\13\16\13\u010d"+
		"\13\13\3\f\3\f\3\f\7\f\u0112\n\f\f\f\16\f\u0115\13\f\3\f\5\f\u0118\n\f"+
		"\3\r\3\r\3\r\7\r\u011d\n\r\f\r\16\r\u0120\13\r\3\r\5\r\u0123\n\r\3\16"+
		"\3\16\3\16\3\16\3\17\5\17\u012a\n\17\3\17\3\17\3\17\3\17\7\17\u0130\n"+
		"\17\f\17\16\17\u0133\13\17\3\20\3\20\3\20\3\20\7\20\u0139\n\20\f\20\16"+
		"\20\u013c\13\20\3\21\3\21\3\21\3\21\3\21\7\21\u0143\n\21\f\21\16\21\u0146"+
		"\13\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u014e\n\21\3\21\3\21\3\21\3"+
		"\21\5\21\u0154\n\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u015d\n\22"+
		"\f\22\16\22\u0160\13\22\3\23\3\23\3\24\3\24\3\25\3\25\3\25\f\60\67I\\"+
		"l\u0088\u010b\u0131\u013a\u0144\2\26\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\34\36 \"$&(\2\7\4\2\25\25\27\27\3\2\24\25\3\2\36#\3\2\31\32\3\2\33\35"+
		"\2\u0197\2*\3\2\2\2\4<\3\2\2\2\6R\3\2\2\2\bT\3\2\2\2\n~\3\2\2\2\f\u008e"+
		"\3\2\2\2\16\u00b4\3\2\2\2\20\u00f8\3\2\2\2\22\u00fa\3\2\2\2\24\u0106\3"+
		"\2\2\2\26\u0117\3\2\2\2\30\u0122\3\2\2\2\32\u0124\3\2\2\2\34\u0129\3\2"+
		"\2\2\36\u0134\3\2\2\2 \u0153\3\2\2\2\"\u0155\3\2\2\2$\u0161\3\2\2\2&\u0163"+
		"\3\2\2\2(\u0165\3\2\2\2*\60\5\b\5\2+/\5\6\4\2,/\5\4\3\2-/\5\b\5\2.+\3"+
		"\2\2\2.,\3\2\2\2.-\3\2\2\2/\62\3\2\2\2\60\61\3\2\2\2\60.\3\2\2\2\61\63"+
		"\3\2\2\2\62\60\3\2\2\2\63\67\7\60\2\2\64\66\5\n\6\2\65\64\3\2\2\2\669"+
		"\3\2\2\2\678\3\2\2\2\67\65\3\2\2\28:\3\2\2\29\67\3\2\2\2:;\7\61\2\2;\3"+
		"\3\2\2\2<=\7\5\2\2=>\5\16\b\2>?\7\24\2\2?@\7&\2\2@A\t\2\2\2AB\7)\2\2B"+
		"\5\3\2\2\2CD\5\16\b\2DI\7\24\2\2EF\7*\2\2FH\7\24\2\2GE\3\2\2\2HK\3\2\2"+
		"\2IJ\3\2\2\2IG\3\2\2\2JL\3\2\2\2KI\3\2\2\2LM\7)\2\2MS\3\2\2\2NO\5\16\b"+
		"\2OP\7\24\2\2PQ\7)\2\2QS\3\2\2\2RC\3\2\2\2RN\3\2\2\2S\7\3\2\2\2TU\7\4"+
		"\2\2U\\\7\24\2\2VW\7\60\2\2WX\5\6\4\2XY\7\61\2\2Y[\3\2\2\2ZV\3\2\2\2["+
		"^\3\2\2\2\\]\3\2\2\2\\Z\3\2\2\2]\t\3\2\2\2^\\\3\2\2\2_b\5\16\b\2`b\7\13"+
		"\2\2a_\3\2\2\2a`\3\2\2\2bc\3\2\2\2cd\7\24\2\2df\7,\2\2eg\5\f\7\2fe\3\2"+
		"\2\2fg\3\2\2\2gh\3\2\2\2hl\7-\2\2ik\5\6\4\2ji\3\2\2\2kn\3\2\2\2lm\3\2"+
		"\2\2lj\3\2\2\2mo\3\2\2\2nl\3\2\2\2o\177\5\22\n\2ps\5\16\b\2qs\7\13\2\2"+
		"rp\3\2\2\2rq\3\2\2\2st\3\2\2\2tu\7\24\2\2uv\7,\2\2vz\7-\2\2wy\5\6\4\2"+
		"xw\3\2\2\2y|\3\2\2\2zx\3\2\2\2z{\3\2\2\2{}\3\2\2\2|z\3\2\2\2}\177\5\22"+
		"\n\2~a\3\2\2\2~r\3\2\2\2\177\13\3\2\2\2\u0080\u0081\5\16\b\2\u0081\u0088"+
		"\7\24\2\2\u0082\u0083\7*\2\2\u0083\u0084\5\16\b\2\u0084\u0085\7\24\2\2"+
		"\u0085\u0087\3\2\2\2\u0086\u0082\3\2\2\2\u0087\u008a\3\2\2\2\u0088\u0089"+
		"\3\2\2\2\u0088\u0086\3\2\2\2\u0089\u008f\3\2\2\2\u008a\u0088\3\2\2\2\u008b"+
		"\u008c\5\16\b\2\u008c\u008d\7\24\2\2\u008d\u008f\3\2\2\2\u008e\u0080\3"+
		"\2\2\2\u008e\u008b\3\2\2\2\u008f\r\3\2\2\2\u0090\u0096\7\23\2\2\u0091"+
		"\u0093\7.\2\2\u0092\u0094\t\3\2\2\u0093\u0092\3\2\2\2\u0093\u0094\3\2"+
		"\2\2\u0094\u0095\3\2\2\2\u0095\u0097\7/\2\2\u0096\u0091\3\2\2\2\u0096"+
		"\u0097\3\2\2\2\u0097\u00b5\3\2\2\2\u0098\u009e\7\21\2\2\u0099\u009b\7"+
		".\2\2\u009a\u009c\t\3\2\2\u009b\u009a\3\2\2\2\u009b\u009c\3\2\2\2\u009c"+
		"\u009d\3\2\2\2\u009d\u009f\7/\2\2\u009e\u0099\3\2\2\2\u009e\u009f\3\2"+
		"\2\2\u009f\u00b5\3\2\2\2\u00a0\u00a6\7\22\2\2\u00a1\u00a3\7.\2\2\u00a2"+
		"\u00a4\t\3\2\2\u00a3\u00a2\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a5\3\2"+
		"\2\2\u00a5\u00a7\7.\2\2\u00a6\u00a1\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7"+
		"\u00b5\3\2\2\2\u00a8\u00b5\7\21\2\2\u00a9\u00b5\7\22\2\2\u00aa\u00b5\7"+
		"\20\2\2\u00ab\u00b5\7\23\2\2\u00ac\u00b2\7\24\2\2\u00ad\u00af\7.\2\2\u00ae"+
		"\u00b0\t\3\2\2\u00af\u00ae\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0\u00b1\3\2"+
		"\2\2\u00b1\u00b3\7/\2\2\u00b2\u00ad\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3"+
		"\u00b5\3\2\2\2\u00b4\u0090\3\2\2\2\u00b4\u0098\3\2\2\2\u00b4\u00a0\3\2"+
		"\2\2\u00b4\u00a8\3\2\2\2\u00b4\u00a9\3\2\2\2\u00b4\u00aa\3\2\2\2\u00b4"+
		"\u00ab\3\2\2\2\u00b4\u00ac\3\2\2\2\u00b5\17\3\2\2\2\u00b6\u00c0\5\"\22"+
		"\2\u00b7\u00b8\7&\2\2\u00b8\u00c1\5\34\17\2\u00b9\u00bb\7,\2\2\u00ba\u00bc"+
		"\5\24\13\2\u00bb\u00ba\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\u00bd\3\2\2\2"+
		"\u00bd\u00c1\7-\2\2\u00be\u00c1\7\'\2\2\u00bf\u00c1\7(\2\2\u00c0\u00b7"+
		"\3\2\2\2\u00c0\u00b9\3\2\2\2\u00c0\u00be\3\2\2\2\u00c0\u00bf\3\2\2\2\u00c1"+
		"\u00c2\3\2\2\2\u00c2\u00c3\7)\2\2\u00c3\u00f9\3\2\2\2\u00c4\u00c5\7\7"+
		"\2\2\u00c5\u00c6\7,\2\2\u00c6\u00c7\5\26\f\2\u00c7\u00c8\7-\2\2\u00c8"+
		"\u00cb\5\20\t\2\u00c9\u00ca\7\6\2\2\u00ca\u00cc\5\20\t\2\u00cb\u00c9\3"+
		"\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00f9\3\2\2\2\u00cd\u00ce\7\r\2\2\u00ce"+
		"\u00cf\7,\2\2\u00cf\u00d0\5\34\17\2\u00d0\u00d1\7)\2\2\u00d1\u00d2\5\26"+
		"\f\2\u00d2\u00d4\7)\2\2\u00d3\u00d5\5\20\t\2\u00d4\u00d3\3\2\2\2\u00d4"+
		"\u00d5\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7\7-\2\2\u00d7\u00d8\5\20"+
		"\t\2\u00d8\u00f9\3\2\2\2\u00d9\u00da\7\f\2\2\u00da\u00db\7,\2\2\u00db"+
		"\u00dc\5\26\f\2\u00dc\u00dd\7-\2\2\u00dd\u00de\5\20\t\2\u00de\u00f9\3"+
		"\2\2\2\u00df\u00e0\7\3\2\2\u00e0\u00f9\7)\2\2\u00e1\u00e3\7\n\2\2\u00e2"+
		"\u00e4\5\34\17\2\u00e3\u00e2\3\2\2\2\u00e3\u00e4\3\2\2\2\u00e4\u00e5\3"+
		"\2\2\2\u00e5\u00f9\7)\2\2\u00e6\u00e7\7\t\2\2\u00e7\u00e8\7,\2\2\u00e8"+
		"\u00e9\5\"\22\2\u00e9\u00ea\7-\2\2\u00ea\u00eb\7)\2\2\u00eb\u00f9\3\2"+
		"\2\2\u00ec\u00ed\7\16\2\2\u00ed\u00ee\7,\2\2\u00ee\u00f1\5\34\17\2\u00ef"+
		"\u00f0\7*\2\2\u00f0\u00f2\7\25\2\2\u00f1\u00ef\3\2\2\2\u00f1\u00f2\3\2"+
		"\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f4\7-\2\2\u00f4\u00f5\7)\2\2\u00f5\u00f9"+
		"\3\2\2\2\u00f6\u00f9\5\22\n\2\u00f7\u00f9\7)\2\2\u00f8\u00b6\3\2\2\2\u00f8"+
		"\u00c4\3\2\2\2\u00f8\u00cd\3\2\2\2\u00f8\u00d9\3\2\2\2\u00f8\u00df\3\2"+
		"\2\2\u00f8\u00e1\3\2\2\2\u00f8\u00e6\3\2\2\2\u00f8\u00ec\3\2\2\2\u00f8"+
		"\u00f6\3\2\2\2\u00f8\u00f7\3\2\2\2\u00f9\21\3\2\2\2\u00fa\u0101\7\60\2"+
		"\2\u00fb\u0100\5\6\4\2\u00fc\u0100\5\4\3\2\u00fd\u0100\5\20\t\2\u00fe"+
		"\u0100\5\n\6\2\u00ff\u00fb\3\2\2\2\u00ff\u00fc\3\2\2\2\u00ff\u00fd\3\2"+
		"\2\2\u00ff\u00fe\3\2\2\2\u0100\u0103\3\2\2\2\u0101\u00ff\3\2\2\2\u0101"+
		"\u0102\3\2\2\2\u0102\u0104\3\2\2\2\u0103\u0101\3\2\2\2\u0104\u0105\7\61"+
		"\2\2\u0105\23\3\2\2\2\u0106\u010b\5\34\17\2\u0107\u0108\7*\2\2\u0108\u010a"+
		"\5\34\17\2\u0109\u0107\3\2\2\2\u010a\u010d\3\2\2\2\u010b\u010c\3\2\2\2"+
		"\u010b\u0109\3\2\2\2\u010c\25\3\2\2\2\u010d\u010b\3\2\2\2\u010e\u0113"+
		"\5\30\r\2\u010f\u0110\7%\2\2\u0110\u0112\5\30\r\2\u0111\u010f\3\2\2\2"+
		"\u0112\u0115\3\2\2\2\u0113\u0111\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0118"+
		"\3\2\2\2\u0115\u0113\3\2\2\2\u0116\u0118\5\30\r\2\u0117\u010e\3\2\2\2"+
		"\u0117\u0116\3\2\2\2\u0118\27\3\2\2\2\u0119\u011e\5\32\16\2\u011a\u011b"+
		"\7$\2\2\u011b\u011d\5\32\16\2\u011c\u011a\3\2\2\2\u011d\u0120\3\2\2\2"+
		"\u011e\u011c\3\2\2\2\u011e\u011f\3\2\2\2\u011f\u0123\3\2\2\2\u0120\u011e"+
		"\3\2\2\2\u0121\u0123\5\32\16\2\u0122\u0119\3\2\2\2\u0122\u0121\3\2\2\2"+
		"\u0123\31\3\2\2\2\u0124\u0125\5\34\17\2\u0125\u0126\5$\23\2\u0126\u0127"+
		"\5\34\17\2\u0127\33\3\2\2\2\u0128\u012a\7\32\2\2\u0129\u0128\3\2\2\2\u0129"+
		"\u012a\3\2\2\2\u012a\u012b\3\2\2\2\u012b\u0131\5\36\20\2\u012c\u012d\5"+
		"&\24\2\u012d\u012e\5\36\20\2\u012e\u0130\3\2\2\2\u012f\u012c\3\2\2\2\u0130"+
		"\u0133\3\2\2\2\u0131\u0132\3\2\2\2\u0131\u012f\3\2\2\2\u0132\35\3\2\2"+
		"\2\u0133\u0131\3\2\2\2\u0134\u013a\5 \21\2\u0135\u0136\5(\25\2\u0136\u0137"+
		"\5 \21\2\u0137\u0139\3\2\2\2\u0138\u0135\3\2\2\2\u0139\u013c\3\2\2\2\u013a"+
		"\u013b\3\2\2\2\u013a\u0138\3\2\2\2\u013b\37\3\2\2\2\u013c\u013a\3\2\2"+
		"\2\u013d\u0144\5\"\22\2\u013e\u013f\7,\2\2\u013f\u0140\5\24\13\2\u0140"+
		"\u0141\7-\2\2\u0141\u0143\3\2\2\2\u0142\u013e\3\2\2\2\u0143\u0146\3\2"+
		"\2\2\u0144\u0145\3\2\2\2\u0144\u0142\3\2\2\2\u0145\u0154\3\2\2\2\u0146"+
		"\u0144\3\2\2\2\u0147\u0154\7\25\2\2\u0148\u0154\7\27\2\2\u0149\u0154\7"+
		"\30\2\2\u014a\u014d\7\b\2\2\u014b\u014e\5\16\b\2\u014c\u014e\7\24\2\2"+
		"\u014d\u014b\3\2\2\2\u014d\u014c\3\2\2\2\u014e\u0154\3\2\2\2\u014f\u0150"+
		"\7,\2\2\u0150\u0151\5\34\17\2\u0151\u0152\7-\2\2\u0152\u0154\3\2\2\2\u0153"+
		"\u013d\3\2\2\2\u0153\u0147\3\2\2\2\u0153\u0148\3\2\2\2\u0153\u0149\3\2"+
		"\2\2\u0153\u014a\3\2\2\2\u0153\u014f\3\2\2\2\u0154!\3\2\2\2\u0155\u015e"+
		"\7\24\2\2\u0156\u0157\7+\2\2\u0157\u015d\7\24\2\2\u0158\u0159\7.\2\2\u0159"+
		"\u015a\5\34\17\2\u015a\u015b\7/\2\2\u015b\u015d\3\2\2\2\u015c\u0156\3"+
		"\2\2\2\u015c\u0158\3\2\2\2\u015d\u0160\3\2\2\2\u015e\u015c\3\2\2\2\u015e"+
		"\u015f\3\2\2\2\u015f#\3\2\2\2\u0160\u015e\3\2\2\2\u0161\u0162\t\4\2\2"+
		"\u0162%\3\2\2\2\u0163\u0164\t\5\2\2\u0164\'\3\2\2\2\u0165\u0166\t\6\2"+
		"\2\u0166)\3\2\2\2/.\60\67IR\\aflrz~\u0088\u008e\u0093\u0096\u009b\u009e"+
		"\u00a3\u00a6\u00af\u00b2\u00b4\u00bb\u00c0\u00cb\u00d4\u00e3\u00f1\u00f8"+
		"\u00ff\u0101\u010b\u0113\u0117\u011e\u0122\u0129\u0131\u013a\u0144\u014d"+
		"\u0153\u015c\u015e";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}