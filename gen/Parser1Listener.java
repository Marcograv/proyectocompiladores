// Generated from C:/Users/marco/source/repos/ProyectoOscarMarco/ProyectoOscarMarco\Parser1.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link Parser1}.
 */
public interface Parser1Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link Parser1#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(Parser1.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(Parser1.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#constdecl}.
	 * @param ctx the parse tree
	 */
	void enterConstdecl(Parser1.ConstdeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#constdecl}.
	 * @param ctx the parse tree
	 */
	void exitConstdecl(Parser1.ConstdeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#vardecl}.
	 * @param ctx the parse tree
	 */
	void enterVardecl(Parser1.VardeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#vardecl}.
	 * @param ctx the parse tree
	 */
	void exitVardecl(Parser1.VardeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#classdecl}.
	 * @param ctx the parse tree
	 */
	void enterClassdecl(Parser1.ClassdeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#classdecl}.
	 * @param ctx the parse tree
	 */
	void exitClassdecl(Parser1.ClassdeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#methodecl}.
	 * @param ctx the parse tree
	 */
	void enterMethodecl(Parser1.MethodeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#methodecl}.
	 * @param ctx the parse tree
	 */
	void exitMethodecl(Parser1.MethodeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#formpars}.
	 * @param ctx the parse tree
	 */
	void enterFormpars(Parser1.FormparsContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#formpars}.
	 * @param ctx the parse tree
	 */
	void exitFormpars(Parser1.FormparsContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#type}.
	 * @param ctx the parse tree
	 */
	void enterType(Parser1.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#type}.
	 * @param ctx the parse tree
	 */
	void exitType(Parser1.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(Parser1.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(Parser1.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(Parser1.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(Parser1.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#actpars}.
	 * @param ctx the parse tree
	 */
	void enterActpars(Parser1.ActparsContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#actpars}.
	 * @param ctx the parse tree
	 */
	void exitActpars(Parser1.ActparsContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(Parser1.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(Parser1.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#condterm}.
	 * @param ctx the parse tree
	 */
	void enterCondterm(Parser1.CondtermContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#condterm}.
	 * @param ctx the parse tree
	 */
	void exitCondterm(Parser1.CondtermContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#condfact}.
	 * @param ctx the parse tree
	 */
	void enterCondfact(Parser1.CondfactContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#condfact}.
	 * @param ctx the parse tree
	 */
	void exitCondfact(Parser1.CondfactContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(Parser1.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(Parser1.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(Parser1.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(Parser1.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactor(Parser1.FactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactor(Parser1.FactorContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#designator}.
	 * @param ctx the parse tree
	 */
	void enterDesignator(Parser1.DesignatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#designator}.
	 * @param ctx the parse tree
	 */
	void exitDesignator(Parser1.DesignatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#relop}.
	 * @param ctx the parse tree
	 */
	void enterRelop(Parser1.RelopContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#relop}.
	 * @param ctx the parse tree
	 */
	void exitRelop(Parser1.RelopContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#addop}.
	 * @param ctx the parse tree
	 */
	void enterAddop(Parser1.AddopContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#addop}.
	 * @param ctx the parse tree
	 */
	void exitAddop(Parser1.AddopContext ctx);
	/**
	 * Enter a parse tree produced by {@link Parser1#mulop}.
	 * @param ctx the parse tree
	 */
	void enterMulop(Parser1.MulopContext ctx);
	/**
	 * Exit a parse tree produced by {@link Parser1#mulop}.
	 * @param ctx the parse tree
	 */
	void exitMulop(Parser1.MulopContext ctx);
}